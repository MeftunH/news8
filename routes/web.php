<?php

use App\Http\Controllers\AboutUsController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\Backend\AdsController;
use App\Http\Controllers\Backend\LanguageController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\SectionController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\FrontendController;

use App\Http\Controllers\SegmentController;
use App\Http\Controllers\SubscribeController;

use App\Http\Controllers\UserController;
use App\Http\Controllers\WebsiteSettingController;
use App\Models\Language;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\SubCategoryController;
use \App\Http\Controllers\Backend\PostController;
use \App\Http\Controllers\Backend\SettingController;
use \App\Http\Controllers\Backend\GalleryController;
use \App\Http\Controllers\Frontend\ExtraController;
use \Mcamara\LaravelLocalization\Facades\LaravelLocalization;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum','verified'])->group(function ()
{
    Route::middleware(['admin'])->group(function () {
        Route::get('/dashboard',[AdminController::class, 'Dashboard'])->name('dashboard');
        Route::view('/profile/edit','profile.edit')->name('profile');
        Route::view('/profile/password','profile.password')->name('password');
    });
});

//Logout
Route::get('/admin/logout', [AdminController::class, 'Logout'])->name('admin.logout');
//Route::get('/language/change/{lang}',[AdminController::class,'GetLang'])->name('admin.lang');


//Admin Areas
Route::get('/add/area', [AreaController::class, 'AddArea'])->name('add.area');
Route::post('/store/area', [AreaController::class, 'StoreArea'])->name('store.area');
Route::get('/areas', [AreaController::class, 'Index'])->name('index.area');
Route::get('/edit/area/{id}', [AreaController::class, 'EditArea'])->name('edit.area');
Route::post('/update/area/{id}', [AreaController::class, 'UpdateArea'])->name('update.area');
Route::get('/delete/area/{id}', [AreaController::class, 'DeleteArea'])->name('delete.area');


//About US
Route::get('/edit/aboutUs/{id}', [AboutUsController::class, 'EditAboutUs'])->name('edit.aboutUs');
Route::post('/update/aboutUs/{id}', [AboutUsController::class, 'UpdateAboutUs'])->name('update.aboutUs');

//Admin Category
Route::get('/categories', [CategoryController::class, 'Index'])->name('categories');
Route::get('/add/category', [CategoryController::class, 'AddCategory'])->name('add.category');
Route::get('/edit/category/{id}', [CategoryController::class, 'EditCategory'])->name('edit.category');
Route::get('/delete/category/{id}', [CategoryController::class, 'DeleteCategory'])->name('delete.category');
Route::post('/store/category', [CategoryController::class, 'StoreCategory'])->name('store.category');
Route::post('/update/category/{id}', [CategoryController::class, 'UpdateCategory'])->name('update.category');

//Category Json
Route::get('/get/subcategory/{category_id}', [PostController::class, 'GetSubCategory']);

//Sections
Route::get('/slider', [SectionController::class, 'Slider'])->name('section.slider');
Route::get('create/slider', [SectionController::class, 'SliderCreate'])->name('section.slider.add');
Route::post('/slider/save', [SectionController::class, 'SliderSave'])->name('section.slider.save');

Route::middleware([\App\Http\Middleware\SectionMiddleware::class])->group(function () {

    Route::get('/most-important-news',[SectionController::class, 'MostImportantNews'])->name('section.mostImportantNews');
    Route::post('/most-important/save', [SectionController::class, 'MostImportantNewsSave'])->name('section.mostImportantNews.save');
    Route::get('/audio-news',[SectionController::class, 'AudioNews'])->name('section.audioNews');
    Route::post('/audio-news/save', [SectionController::class, 'AudioNewsSave'])->name('section.audioNews.save');
    Route::post('/audio-news/save', [SectionController::class, 'AudioNewsSave'])->name('section.audioNews.save');
    Route::get('/category-segments',[SegmentController::class, 'Index'])->name('index.category_segment');
    Route::get('/add-category-segments',[SegmentController::class, 'AddSegment'])->name('add.category_segment');
    Route::get('/edit-category-segments/{id}',[SegmentController::class, 'EditSegment'])->name('edit.category_segment');
    Route::post('/segment/save', [SegmentController::class, 'StoreSegment'])->name('store.category_segment');
    Route::post('/segment/update', [SegmentController::class, 'UpdateSegment'])->name('update.segment');
    Route::get('/delete/segment/{id}', [SegmentController::class, 'DeleteSegment'])->name('delete.segment');

});

//Admin Subcategory
Route::get('/subcategories', [SubCategoryController::class, 'Index'])->name('subcategories');
Route::get('/add/subcategory', [SubCategoryController::class, 'AddSubCategory'])->name('add.subcategory');
Route::get('/edit/subcategory/{id}', [SubCategoryController::class, 'EditSubCategory'])->name('edit.subcategory');
Route::post('/store/subcategory', [SubCategoryController::class, 'StoreSubCategory'])->name('store.subcategory');
Route::post('/update/subcategory/{id}', [SubCategoryController::class, 'UpdateSubCategory'])->name('update.subcategory');
Route::get('/delete/subcategory/{id}', [SubCategoryController::class, 'DeleteSubCategory'])->name('delete.subcategory');
//Route::get('/delete/subcategory/{id}', [SubCategoryController::class, 'DeleteSubCategory'])->name('delete.subcategory');


Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
//Admin Post
    Route::get('/all/lang', [LanguageController::class, 'Index'])->name('language.all');
    Route::get('/create/lang', [LanguageController::class, 'Create'])->name('language.create');
    Route::post('/store/lang', [LanguageController::class, 'Store'])->name('language.store');
    Route::get('/edit/lang/{id}', [LanguageController::class, 'Edit'])->name('language.edit');
    Route::get('pages/config/edittranslation{edit?}', [LanguageController::class, 'EditTranslation'])->name('translation.edit');
    Route::post('/save/translation', [LanguageController::class, 'SaveTranslation'])->name('translation.save');
    Route::get('/language/delete/{id}', [LanguageController::class, 'Delete'])->name('language.delete');
    Route::post('/language/update/{id}', [LanguageController::class, 'Update'])->name('language.update');

    Route::get('/create/post', [PostController::class, 'Create'])->name('create.post');
    Route::post('/store/post', [PostController::class, 'StorePost'])->name('store.post');
    Route::get('/all_post', [PostController::class, 'Index'])->name('all.post');
    Route::get('/edit_post/{id}', [PostController::class, 'EditPost'])->name('edit.post');
    Route::get('/delete_post/{id}', [PostController::class, 'DeletePost'])->name('delete.post');
    Route::post('/update/post/{id}', [PostController::class, 'UpdatePost'])->name('update.post');
});




//About us
Route::get('/website/aboutUs', [AboutUsController::class, 'Index'])->name('AboutUs');

//settings
Route::get('/social/setting', [SettingController::class, 'SocialSetting'])->name('social.setting');
Route::post('/social/update/{id?}', [SettingController::class, 'SocialUpdate'])->name('update.social');
Route::get('/seo/setting', [SettingController::class, 'SeoSetting'])->name('seo.setting');
Route::post('/seo/update/{id}', [SettingController::class, 'SeoUpdate'])->name('update.seo');
Route::get('/live_tv/setting', [SettingController::class, 'LiveTvSetting'])->name('live_tv.setting');
Route::post('/live_tv/update/{id}', [SettingController::class, 'LiveTvUpdate'])->name('update.live_tv');
//live_tv active inactive
Route::get('/live_tv/active/{id}', [SettingController::class, 'Active_LiveTV'])->name('active.live_tv');
Route::get('/live_tv/inactive/{id}', [SettingController::class, 'Inactive_LiveTV'])->name('inactive.live_tv');
Route::get('/notice/setting', [SettingController::class, 'NoticeSetting'])->name('notice.setting');
Route::post('/notice/update/{id}', [SettingController::class, 'NoticeUpdate'])->name('update.notice');
Route::get('/notice/active/{id}', [SettingController::class, 'ActiveNoticeSetting'])->name('active.notice');
Route::get('/notice/inactive/{id}', [SettingController::class, 'InActiveNoticeSetting'])->name('inactive.notice');

//Website
Route::get('/website/setting', [SettingController::class, 'WebsiteSetting'])->name('all.website');
Route::get('/logo/setting', [SettingController::class, 'Logo'])->name('settings');
Route::get('/logo/edit/{id}', [SettingController::class, 'LogoEdit'])->name('settings.edit.logo');
Route::post('/logo/update/{id}', [SettingController::class, 'LogoUpdate'])->name('settings.update.logo');
Route::get('/add/website', [SettingController::class, 'AddWebsiteSetting'])->name('add.website');
Route::post('/store/website', [SettingController::class, 'StoreWebsite'])->name('store.website');
Route::get('/edit_website/{id}', [SettingController::class, 'EditWebsite'])->name('edit.website');
Route::post('/update/website/{id}', [SettingController::class, 'UpdateWebsite'])->name('update.website');
Route::get('/delete_website/{id}', [SettingController::class, 'DeleteWebsite'])->name('delete.website');

//Gallery
Route::get('/photo/gallery', [GalleryController::class, 'PhotoGallery'])->name('photo.gallery');
Route::get('/add/photo', [GalleryController::class, 'AddPhoto'])->name('add.photo');
Route::post('/store/photo', [GalleryController::class, 'StorePhoto'])->name('store.photo');
Route::get('/edit_photo/{id}', [GalleryController::class, 'EditPhoto'])->name('edit.photo');
Route::post('/update/photo/{id}', [GalleryController::class, 'UpdatePhoto'])->name('update.photo');
Route::get('/delete_photo/{id}', [GalleryController::class, 'DeletePhoto'])->name('delete.photo');

Route::get('/video/gallery', [GalleryController::class, 'VideoGallery'])->name('video.gallery');
Route::get('/add/video', [GalleryController::class, 'AddVideo'])->name('add.video');
Route::post('/store/video', [GalleryController::class, 'StoreVideo'])->name('store.video');
Route::get('/edit_video/{id}', [GalleryController::class, 'EditVideo'])->name('edit.video');
Route::post('/update/video/{id}', [GalleryController::class, 'UpdateVideo'])->name('update.video');
Route::get('/delete_video/{id}', [GalleryController::class, 'DeleteVideo'])->name('delete.video');

Route::get('/add/writer', [RoleController::class, 'InsertWriter'])->name('add.writer');
Route::get('/allWriters', [RoleController::class, 'AllWriter'])->name('all.writer');
Route::get('/users', [UserController::class, 'Index'])->name('users.index');
Route::post('/store/writer', [RoleController::class, 'StoreWriter'])->name('store.writer');
Route::get('/edit/role/{id}', [RoleController::class, 'EditWriter'])->name('edit.writer');
Route::post('/update/writer/{id}', [RoleController::class, 'UpdateWriter'])->name('update.writer');
Route::get('/delete/role/{id}', [GalleryController::class, 'DeleteVideo'])->name('delete.writer');


//Route::resource('lang',AdminController::class);
//Route::get('GetLang',function ()
//{
//    return view('backend.admin.body.header');
//});
//Single Post
//Post Category and SubCategory single pages


//Ads Backend Section
Route::get('/ad/list', [AdsController::class, 'AdList'])->name('ad.list');
Route::get('/ad/add', [AdsController::class, 'Create'])->name('ad.add');
Route::post('/store/ad', [AdsController::class, 'Store'])->name('store.ads');


Route::group(["prefix"=> 'subscribers'],function ()
{
    Route::get('/', [SubscribeController::class, 'index'])->name('subscriber.index');
});



//Lang


Route::get('/contact-form', [ContactController::class, 'contactForm'])->name('contact-form');
Route::post('/contact-form', [ContactController::class, 'storeContactForm'])->name('contact-form.store');
Route::post('/subscribe/store',[SubscribeController::class,'store'])->name('subscriber.store');
Route::post('/subscribe/store',[SubscribeController::class,'store'])->name('subscriber.store');
//Route::get('/compose', [SubscribeController::class, 'compose']);
//Route::post('/send', [EmailController::class, 'send']);


Route::group(['prefix'=>LaravelLocalization::setLocale(),
    'as'=>'front.',
    'middleware'=>['localeSessionRedirect','localizationRedirect',]],function ()
{
    Route::get('/', function () {
        return view('main.home');
    });

    Route::get('/catposts/{id}', [ExtraController::class, 'CatPost'])->name('catPost');
    Route::get('/subcatposts/{id}', [ExtraController::class, 'SubCatPost'])->name('subCatPost');
    Route::get('/area_post/{id}',[ExtraController::class,'AreaPost'])->name('areaPosts');
    Route::get('/aboutUs/',[ExtraController::class,'AboutUs'])->name('aboutUs');
    Route::get('/search/', [ExtraController::class, 'search'])->name('search');
    Route::get('/lastNews/', [ExtraController::class, 'LastNews'])->name('lastNews');
    Route::get('/lastNews/today', [ExtraController::class, 'LastNewsToday'])->name('lastNewsToday');
    Route::get('/lastNews/yesterday', [ExtraController::class, 'LastNewsYesterday'])->name('lastNewsYesterday');
    Route::get('/lastNews/thisWeek', [ExtraController::class, 'LastNewsThisWeek'])->name('lastNewsThisWeek');
    Route::get('/lastNews/thisMonth', [ExtraController::class, 'LastNewsThisMonth'])->name('lastNewsThisMonth');
    Route::get('/lastNews/prevWeek', [ExtraController::class, 'LastNewsPrevWeek'])->name('lastNewsPrevWeek');
    Route::get('/lastNews/lastMonth', [ExtraController::class, 'LastNewsLastMonth'])->name('lastNewsLastMonth');
    Route::get('/videoNews', [ExtraController::class, 'VideoNews'])->name('videoNews');
    Route::get('/photoNews', [ExtraController::class, 'PhotoNews'])->name('photoNews');
    Route::get('/areaNews', [ExtraController::class, 'AreaNews'])->name('areaNews');
    Route::get('/multimedia', [ExtraController::class, 'MultiMedia'])->name('multiMedia');
    Route::get('/contact/', [ExtraController::class, 'contact'])->name('contact');
    Route::get('/view/post/{id}', [ExtraController::class, 'SinglePost'])->name('post_view');
    Route::get('/{lang}',[LanguageController::class,'GetChange'])->name('change');
    Route::get('/',[FrontendController::class,'Home'])->name('getHome');
//    Route::get('/',[FrontendController::class,'Header'])->name('getHeader');

});




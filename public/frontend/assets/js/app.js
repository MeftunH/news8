import $ from 'jquery'; window.jQuery = $; window.$ = $ // import jQuery module (npm i -D jquery)
import Swiper from 'swiper';
// require('~/app/libs/mmenu/js/jquery.mmenu.all.min.js') // import vendor jQuery plugin example (not module)

$(()=> {
	
/*================================
      1. Dropdown
================================*/
let dropdownButton = $('.dropdownButton');
let dropdownMenu = $('.dropdownMenu');

// Button click
dropdownButton.on('click', function () {
	let _this = $(this);

	dropdownButton.not(_this).removeClass('active').siblings('.dropdownMenu').slideUp(300);
	_this.toggleClass('active').siblings('.dropdownMenu').slideToggle(300);
});

// Click outside target
$(document).on('click touchstart', function(e) {
	let target = e.target;

	if (!$(target).is('.dropdownButton') && !$(target).parents('.dropdownButton').length > 0 && !$(target).parents('.dropdownMenu').length > 0) {
		dropdownMenu.slideUp(300);
		dropdownButton.removeClass('active');
	}

});
/*================================
		/. Dropdown
================================*/

/*================================
    2. Swiper JS
================================*/
const slider = $('.swiper-container');

slider.each(function () {
	let $slider = $(this);
	let $slideLength = $slider.find('.swiper-slide').length;

  // For slide count
  if($slider.data('slide-index') != null) {
    let selector = $($slider.data('slide-index'));
    
    selector.each(function(index) {
      $(this).html('0' + parseInt(index+1))
    })
  }

	let mySwiper = new Swiper($(this), {
		loop: $slideLength >= $slider.data('slides-lg') ? $slider.data('loop') : false,
		slidesPerView: $slider.data('slides-lg'),
		autoplay: $slider.data('autoplay'),
		spaceBetween: $slider.data('margin'),
		observer: true,
		slidesPerGroup: $slider.data('per-group-lg') == null ? 1 : $slider.data('per-group-lg'),
    disableOnInteraction: true,
    observeParents: true,
		pagination: {
			el: $slider.find('.swiper-pagination'),
			clickable: true
		},
		navigation: {
			nextEl: $slider.data('navigation-next') == null ? $slider.find('.swiper-button-next') : $slider.data('navigation-next'),
			prevEl: $slider.data('navigation-prev') == null ? $slider.find('.swiper-button-prev') : $slider.data('navigation-prev'),
		},
		breakpoints: {
			0: {
				slidesPerView: $slider.data('slides-xs'),
				slidesPerGroup: $slider.data('per-group-xs') == null ? 1 : $slider.data('per-group-xs')
			},
			576: {
				slidesPerView: $slider.data('slides-sm'),
				slidesPerGroup: $slider.data('per-group-sm') == null ? 1 : $slider.data('per-group-sm')
			},
			// when window width is >= 480px
			768: {
				slidesPerView: $slider.data('slides-md'),
				slidesPerGroup: $slider.data('per-group-md') == null ? 1 : $slider.data('per-group-md')
			},
			// when window width is >= 640px
			992: {
				slidesPerView: $slider.data('slides-lg')
			}
		}
  });
  

  $slider.mouseleave(function() {
    $slider.data('autoplay') ? mySwiper.autoplay.start() : false;
  });

});


// $('.nav-item').on('click', function(){
//   $(this).parents('.about__tabs').find('.swiper-container')[1].swiper.update()
// });

/*================================
		/. Swiper JS
================================*/

/*================================
		3. Burger append
================================*/
$(() => {
	let _weather = $('.nav__info-weather');
	let _languages = $('.nav__language');
	let _menu = $('.nav__categories-list');
	let _about = $('.nav__info-about');
	let _social = $('.nav__social-list:eq(0)');
	
	_weather.clone().appendTo($('.nav__burger-weather'));
	_languages.clone().appendTo($('.nav__burger-languages'));
	_menu.clone().appendTo($('.nav__burger-content'));
	_about.clone().appendTo($('.nav__burger-about'));
	_social.clone().appendTo($('.nav__burger-social'));

})

let _overlay = $('.nav__burger-menu-overlay');
let _menu = $('.nav__burger-menu');

$('.nav__burger, .nav__burger-close, .nav__burger-menu-overlay').on('click', function() {
	_overlay.toggleClass('nav__burger-menu_active');
	_menu.toggleClass('nav__burger-menu_active')
})
/*================================
		/. Burger append
================================*/

/*================================
		4. Search
================================*/
$('.nav__main-search').on('click', function() {
	$(this).toggleClass('nav__main-search_close');
	$('.nav__main-search-input').toggleClass('nav__main-search-input_active');
})
/*================================
		/. Search
================================*/


})
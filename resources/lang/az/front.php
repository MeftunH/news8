<?php
return [
    "Languages" => "Diller edit",
    "Contact" => "Bizimle elaqe saxlayin",
    "Sign_up" => "Qeydiyyatdan kecin",
    "all_news" => "Son xeberler",
    "popular_news" => "Popular Xeberler",
    "read_more" => "daha cox oxu",
    "heading_news" => "manset xeberleri",
    "more" => "Daha cox",
    "Weekly_Trend" => "Heftelik Trend",
    "main news" => "Esas Xeberler",
    "most_important_news"=>"En vacib Xeberler",
    "video" => "video",
    "most_viewed_news" => "en cox oxunan xeberler",
    "about_us" => "Haqqimizda",
];

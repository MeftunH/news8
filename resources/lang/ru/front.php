<?php
return [
    'Languages' => 'Rusca Languages',
    'Contact' => 'Rusca Contacts',
    'Sign up'=>'Rusca Sign up',
    'latest news' => 'Rusca son xeberler',
    'popular news' => 'Rusca Popular Xeberler',
    'read more'=>'read more rusca',
    'heading news' => 'heading news rusca',
    'more'=>'rusca more',
    'Weekly Trend'=>'Rusca Weekly Trend',
    'main news' => 'Rusca Main News',
    'most_important_news' => 'Rusca En vacib xeberler',
    'video' => 'video rusca',
    "most_viewed_news" => " rusca most viewed news",
    "about_us"=>"About us rusca",
];

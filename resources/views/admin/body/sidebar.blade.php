<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
        <a class="sidebar-brand brand-logo" href="{{route('dashboard')}}"><img src="{{asset($settings->logo)}}" class="img-fluid"
                                                                   alt="logo"/></a>
        <a class="sidebar-brand brand-logo-mini" href="index.html"><img
                src="{{asset('backend/assets/images/logo-mini.svg')}}" alt="logo"/></a>
    </div>
    <ul class="nav">
        <li class="nav-item profile">
            <div class="profile-desc">
                <div class="profile-pic">
                    <div class="count-indicator">
                        <img class="img-xs rounded-circle " src="{{asset(auth()->user()->profile_photo_path)}}"
                             alt="">
                        <span class="count bg-success"></span>
                    </div>
                    <div class="profile-name">
                        <h5 class="mb-0 font-weight-normal">{{\Illuminate\Support\Facades\Auth::user()->name}}</h5>
                    </div>
                </div>
                <a href="#" id="profile-dropdown" data-toggle="dropdown"><i class="mdi mdi-dots-vertical"></i></a>
                <div class="dropdown-menu dropdown-menu-right sidebar-dropdown preview-list"
                     aria-labelledby="profile-dropdown">
                    <a href="{{route('profile')}}" class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-dark rounded-circle">
                                <i class="mdi mdi-settings text-primary"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <p class="preview-subject ellipsis mb-1 text-small">Account settings</p>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{route('password')}}" class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-dark rounded-circle">
                                <i class="mdi mdi-onepassword  text-info"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <p class="preview-subject ellipsis mb-1 text-small">Change Password</p>
                        </div>
                    </a>

            </div>
        </li>
        <li class="nav-item nav-category">
            <span class="nav-link">Navigation</span>
        </li>

            <li class="nav-item menu-items">
                <a class="nav-link" href="{{route('dashboard')}}">
              <span class="menu-icon">
                <i class="mdi mdi-speedometer"></i>
              </span>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li>
        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#profile" aria-expanded="false" aria-controls="profile">
              <span class="menu-icon">
                <i class="mdi mdi-account"></i>
              </span>
                <span class="menu-title">Profile</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="profile">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('profile') }}">Profile</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('password') }}">Update Password</a></li>
                </ul>
            </div>
        </li>

            <li class="nav-item menu-items">
                <a class="nav-link" data-toggle="collapse" href="#users" aria-expanded="false" aria-controls="users">
              <span class="menu-icon">
                <i class="mdi mdi-account"></i>
              </span>
                    <span class="menu-title">User Actions</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="users">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"><a class="nav-link" href="{{ route('users.create') }}">Add User</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('users.index') }}">Manage User</a></li>
                    </ul>
                </div>
            </li>


        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#roles" aria-expanded="false" aria-controls="roles">
              <span class="menu-icon">
                <i class="mdi mdi-account-badge"></i>
              </span>
                <span class="menu-title">Role Actions</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="roles">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('roles.index') }}">Manage Roles</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('roles.create') }}">Add Role</a></li>
                </ul>
            </div>
        </li>

            <li class="nav-item menu-items">
                <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false"
                   aria-controls="ui-basic">
              <span class="menu-icon">
                <i class="mdi mdi-playlist-play"></i>
              </span>
                    <span class="menu-title">Categories</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"><a class="nav-link" href="{{route('categories')}}">Category</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('subcategories')}}">SubCategory</a></li>
                    </ul>
                </div>
            </li>



        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#areas" aria-expanded="false" aria-controls="areas">
              <span class="menu-icon">
                <i class="mdi mdi-playlist-play"></i>
              </span>
                <span class="menu-title">Areas</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="areas">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{route('index.area')}}">All Areas</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('add.area')}}">Add Area</a></li>
                </ul>
            </div>
        </li>

        @can('post-create')
        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#post" aria-expanded="false" aria-controls="post">
        <span class="menu-icon">
          <i class="mdi mdi-forum"></i>
        </span>
                <span class="menu-title">Post</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="post">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href={{route("create.post")}}>Add post </a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route("all.post")}}"> All posts </a></li>

                </ul>
            </div>
        </li>
        @endcan
        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#setting" aria-expanded="false" aria-controls="setting">
              <span class="menu-icon">
                <i class="mdi mdi-settings"></i>
              </span>
                <span class="menu-title">Settings</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="setting">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('social.setting') }}">Social Setting </a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('seo.setting') }}">Seo Setting </a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('live_tv.setting') }}">Live Tv Setting </a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('notice.setting') }}">Notice Setting </a>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('settings') }}">Logo and icon </a>
                    </li>


                </ul>
            </div>
        </li>

        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#website" aria-expanded="false" aria-controls="website">
              <span class="menu-icon">
                <i class="mdi mdi-apps"></i>
              </span>
                <span class="menu-title">Website</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="website">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('add.website') }}">Add Website Link</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('all.website') }}">All Website Link </a>
                    <li class="nav-item"><a class="nav-link" href="{{ route('AboutUs') }}">About Us </a>
                    </li>

                </ul>
            </div>
        </li>

        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#section" aria-expanded="false" aria-controls="section">
              <span class="menu-icon">
                <i class="mdi mdi-account-network"></i>
              </span>
                <span class="menu-title">Sections</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="section">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item menu-items">
                        <a class="nav-link" data-toggle="collapse" href="#slider" aria-expanded="false"
                           aria-controls="slider">
              <span class="menu-icon">
                <i class="mdi mdi-account-badge-alert"></i>
              </span>
                            <span class="menu-title">Slider</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="slider">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"><a class="nav-link" href="{{ route('section.slider') }}">Added
                                        Posts</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('section.slider.add') }}">Add
                                        Post to Slider
                                        Link </a></li>

                            </ul>
                        </div>
                    </li>
                    <li class="nav-item menu-items">
                        <a class="nav-link" data-toggle="collapse" href="#most_important_news" aria-expanded="false"
                           aria-controls="most_important_news">
              <span class="menu-icon">
                <i class="mdi mdi-monitor-star"></i>
              </span>
                            <span class="menu-title">Most Important</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="most_important_news">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"><a class="nav-link"
                                                        href="{{ route('section.mostImportantNews') }}">Added
                                        Posts</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('section.slider.add') }}">Add
                                        Post to Most Important News Section
                                        Link </a></li>

                            </ul>
                        </div>
                    </li>

                    <li class="nav-item menu-items">
                        <a class="nav-link" data-toggle="collapse" href="#audio_news" aria-expanded="false"
                           aria-controls="audio_news">
              <span class="menu-icon">
                <i class="mdi mdi-audio-video"></i>
              </span>
                            <span class="menu-title">Audio news</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="audio_news">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"><a class="nav-link" href="{{ route('section.audioNews') }}">Added
                                        Posts</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('section.slider.add') }}">Add
                                        Post</a></li>

                            </ul>
                        </div>
                    </li>

                    <li class="nav-item menu-items">
                        <a class="nav-link" data-toggle="collapse" href="#categories_module" aria-expanded="false"
                           aria-controls="categories_module">
              <span class="menu-icon">
                <i class="mdi mdi-barcode"></i>
              </span>
                            <span class="menu-title">Categories</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="categories_module">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"><a class="nav-link" href="{{ route('index.category_segment') }}">Added
                                        Segments</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('add.category_segment') }}">Add
                                        Segment</a></li>

                            </ul>
                        </div>
                    </li>

                </ul>
            </div>
        </li>

        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#gallery" aria-expanded="false" aria-controls="gallery">
              <span class="menu-icon">
                <i class="mdi mdi-image-filter-center-focus-weak"></i>
              </span>
                <span class="menu-title">MultiMedia</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="gallery">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('photo.gallery') }}">Photo Gallery</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('video.gallery') }}">Video Gallery </a></li>

                </ul>
            </div>
        </li>

        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#ads" aria-expanded="false" aria-controls="gallery">
              <span class="menu-icon">
                <i class="mdi mdi-bulletin-board"></i>
              </span>
                <span class="menu-title">Advertisement</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ads">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('ad.list') }}">Ads List</a></li>
                </ul>
            </div>
        </li>


        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#language" aria-expanded="false" aria-controls="language">
              <span class="menu-icon">
                <i class="mdi mdi-flag"></i>
              </span>
                <span class="menu-title">Language</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="language">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('language.all') }}">All Languages</a></li>

                    <li class="nav-item"><a class="nav-link" href="{{ route('language.create') }}">Create Language</a>

                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#subscriber" aria-expanded="false"
               aria-controls="subscriber">
              <span class="menu-icon">
                <i class="mdi mdi-account"></i>
              </span>
                <span class="menu-title">Subscribers</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="subscriber">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('subscriber.index') }}">Subscriber List</a>
                    </li>
                </ul>
            </div>
        </li>

    </ul>
</nav>


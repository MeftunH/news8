@extends('admin.admin_master')
@section('admin')


    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}" class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Welcome to Easy News </h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
        <a href=" {{ url('/') }} " target="_blank" class="btn btn-outline-light btn-rounded get-started-btn">Visit Fontend ? </a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-12 margin-tb">

                <div class="pull-left">

                    <h2> Show Role</h2>

                </div>

                <div class="pull-right">

                    <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>

                </div>

            </div>

        </div>



        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Name:</strong>

                    {{ $role->name }}

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Permissions:</strong>

                    @if(!empty($rolePermissions))

                        @foreach($rolePermissions as $v)

                            <label class="label label-success">{{ $v->name }},</label>

                        @endforeach

                    @endif

                </div>

            </div>

        </div>

@endsection

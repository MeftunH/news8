@extends('admin.admin_master')
@section('admin')


    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}" class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Welcome to Easy News </h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
        <a href=" {{ url('/') }} " target="_blank" class="btn btn-outline-light btn-rounded get-started-btn">Visit Fontend ? </a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12 margin-tb">

                <div class="pull-left">

                    <h2>Role Management</h2>

                </div>

                <div class="pull-right">

                    @can('role-create')

                        <a class="btn btn-success" href="{{ route('roles.create') }}"> Create New Role</a>

                    @endcan

                </div>

            </div>

        </div>



        @if ($message = Session::get('success'))

            <div class="alert alert-success">

                <p>{{ $message }}</p>

            </div>

        @endif



        <table class="table table-bordered">

            <tr>

                <th>No</th>

                <th>Name</th>

                <th width="280px">Action</th>

            </tr>

            @foreach ($roles as $key => $role)

                <tr>

                    <td>{{ ++$i }}</td>

                    <td>{{ $role->name }}</td>

                    <td>

                        <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>

                        @can('role-edit')

                            <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>

                        @endcan

                        @can('role-delete')

                            {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}

                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

                            {!! Form::close() !!}

                        @endcan

                    </td>

                </tr>

            @endforeach

        </table>



    {!! $roles->render() !!}
@endsection

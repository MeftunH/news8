@extends('admin.admin_master')
@section('admin')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin panel</h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
                          <a href="{{url('/')}}" target="_blank"
                             class="btn btn-outline-light btn-rounded get-started-btn">Go to site</a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Added Posts</h4>


                    <div class="template-demo">
                        {{--                    <button type="button" class="btn btn-success btn-rounded btn-fw">Success</button>--}}
                        <a href="{{route('section.slider.add')}}">
                            <button type="button" class="btn btn-success btn-rounded btn-fw"
                                    style="float: right;scrollbar-base-color: #0a0a0a;">Add Post To Slider
                            </button>
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th> #</th>
                                <th>Post Title</th>
                                <th> Category</th>
                                <th> Image</th>
                                <th> Post Date</th>
                                <th> Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php($i=1)
                            <form id="calidate" method="post" action="{{route('section.audioNews.save')}}">
                                @csrf
                                @foreach($addedPosts as $row)

                                    <tr>
                                        <td> {{$i++}}</td>
                                        <td>{{$row->title}}</td>
                                        <td>{{$row->category_name}}</td>

                                        <td><img src="{{ $row->image }}" style="width: 100px; height: 100px;"></td>
                                        <td> {{$row->post_date}}</td>
                                        <td>
                                            <div class="form-check form-check-success col-md-4">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input"
                                                           id="slider_little_posts"
                                                           name="slider_little_posts[{{$row->id}}]"
                                                           value="1" <?php if ($row->is_audio_content == 1) {
                                                        echo "checked";
                                                    } ?>>
                                                    Added to Audio Content <i class="input-helper"></i></label>
                                                @if(request()->get('slider_little_posts') == 0)
                                                    <input type="hidden" name="id[{{$row->id}}]" value="{{$row->id}}">
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                <div class="template-demo">
                                <button type="submit" class="btn btn-primary mr-2">Save</button>
                                </div>
                            </form>
                            </tbody>
                        </table>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('admin.admin_master')
@section('admin')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin Panel </h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
        <a href=" {{ url('/') }} " target="_blank" class="btn btn-outline-light btn-rounded get-started-btn">Go to Site ? </a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Gallery Photos</h4>

                    <form class="forms-sample" action="{{ route('update.photo',$photo->id ) }}" method="post"
                          enctype="multipart/form-data">
                        @csrf

                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Title</label>
                                <input type="text" class="form-control" id="exampleInputName1" name="title"
                                       value="{{ $photo->title }}">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="type">Photo Type</label>
                                <select class="form-control" name="type">
                                    <option value="1" @if($photo->type=='1') selected='selected' @endif >Big Photo
                                    </option>
                                    <option value="0" @if($photo->type=='0') selected='selected' @endif >Small Photo
                                    </option>

                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Photo Upload</label>
                                <input type="file" name="photo" class="form-control-file" id="photo">
                            </div>
                        </div>
                        <br>
                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Old photo</label>
                                <img src="{{ URL::to($photo->photo)  }}" style="width: 70px; height: 50px;">
                                <input type="hidden" name="oldimage" value="{{ $photo->photo }}">
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary mr-2">Update</button>

                    </form>
                </div>
            </div>
        </div>

        <!--
         This is for Category  -->
        <script type="text/javascript">
            $(document).ready(function () {
                $('select[name="category_id"]').on('change', function () {
                    var category_id = $(this).val();
                    if (category_id) {
                        $.ajax({
                            url: "{{  url('/get/subcategory/') }}/" + category_id,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                $("#subcategory_id").empty();
                                $.each(data, function (key, value) {
                                    $("#subcategory_id").append('<option value="' + value.id + '">' + value.subcategory_en + '</option>');
                                });
                            },

                        });
                    } else {
                        alert('danger');
                    }
                });
            });
        </script>


@endsection

@extends('admin.admin_master')
@section('admin')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin panel</h4>
                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
                          <a href="{{url('/')}}" target="_blank"
                             class="btn btn-outline-light btn-rounded get-started-btn">Go to site</a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(session("status"))
                <div class="alert alert-primary">{{session("status")}}</div>
            @endif
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Language</h4>

                        <form class="forms-sample" method="POST" action="{{route('language.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="category_en" class="col-sm-3 col-form-label">Language Name</label>
                                <div class="col-sm-9">
                                    <label>
                                        <input type="text" class="form-control" name="name"
                                               placeholder="Name" required>
                                    </label>
                                    @error('name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="category_az" class="col-sm-3 col-form-label">Code</label>
                                <div class="col-sm-9">
                                    <label>
                                        <input type="text" class="form-control" name="code"
                                               placeholder="Code" required>
                                    </label>
                                </div>
                                @error('code')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>


                            <div class="form-group row">
                                <label for="category_az" class="col-sm-3 col-form-label">Locale</label>
                                <div class="col-sm-9">
                                    <label>
                                        <input type="text" class="form-control" name="local"
                                               placeholder="locale" required>
                                    </label>
                                </div>
                                @error('locale')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <label for="flag" class="col-sm-3 col-form-label">Flag</label>
                                <div class="col-sm-6">
                                    <input type="file" name="flag" class="form-control-file" id="flag"  onchange="readURL(this);">

                                </div>
                                <div class="col-sm-3">

                                <img id="blah" src="#" alt="" style="width: 16px;height: 16px"/>

                                </div>

                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="status" value="1">
                                        Active <i class="input-helper"></i></label>
                                </div>

                            </div>

                            <br/>

                            <button type="submit" class="btn btn-secondary mr-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#blah').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }
            </script>
@endsection

@extends('admin.admin_master')
@section('admin')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin panel</h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
                          <a href="{{url('/')}}" target="_blank"
                             class="btn btn-outline-light btn-rounded get-started-btn">Go to site</a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                        <h4 class="card-title">Languages Page</h4>

                    @can('language create')
                    <div class="template-demo">
                        {{--                    <button type="button" class="btn btn-success btn-rounded btn-fw">Success</button>--}}
                        <a href="{{route('language.create')}}">
                            <button type="button" class="btn btn-success btn-rounded btn-fw"
                                    style="float: right;scrollbar-base-color: #0a0a0a;">Add Language
                            </button>
                        </a>
                    </div>
                    @endcan
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th> #</th>
                                <th>Name</th>
                                <th>Locale</th>
                                <th> Code</th>
                                <th> Flag</th>
                                <th> Action
                                <th>Translation</th>
                                <th> Status</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php($i=1)
                            @if(isset($languages))
                                @foreach($languages as $row)
                                    <tr>
                                        <td> {{$i++}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->local}}</td>
                                        <td>{{$row->code}}</td>
                                        <td class="flex-column"><img src="{{asset($row->flag)}}"
                                                                     style="width: 16px; height: 16px;"
                                                                     alt="{{ $row->flag}}"></td>
                                        <td>
                                            <a class="btn btn-info" href="{{route('language.edit',$row->id)}}">Edit </a>
                                            @if($row->code != 'en')
                                                <a class="btn btn-danger" href="{{route('language.delete',$row->id)}}"
                                                   onclick="return confirm('Are you sure to delete language?')">Delete </a>
                                            @endif
                                        </td>
                                        <td>

                                            <a onclick="EditModal(this.href, 'Edit Translation');return false;"
                                               href="{{url('pages/config/edittranslation?edit='.$row->local.'&file=')}}"
                                               class="btn btn-primary">
                                                Edit Translations
                                            </a>
                                        </td>
                                        <td>{{$row->status}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>


                        <br>
                        {{ $languages->links('pagination::bootstrap-4')}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>

        $(document).ready(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('click', '#submit', function (event) {
                event.preventDefault()
                var id = $("#color_id").val();
                var name = $("#name").val();

                $.ajax({
                    url: 'color/' + id,
                    type: "POST",
                    data: {
                        id: id,
                        name: name,
                    },
                    dataType: 'json',
                    success: function (data) {

                        $('#companydata').trigger("reset");
                        $('#practice_modal').modal('hide');
                        window.location.reload(true);
                    }
                });
            });

            $('body').on('click', '#editCompany', function (event) {

                event.preventDefault();
                var id = $(this).data('id');
                console.log(id)
                $.get('color/' + id + '/edit', function (data) {
                    $('#userCrudModal').html("Edit category");
                    $('#submit').val("Edit category");
                    $('#practice_modal').modal('show');
                    $('#color_id').val(data.data.id);
                    $('#name').val(data.data.name);
                })
            });

        });
    </script>
@endpush

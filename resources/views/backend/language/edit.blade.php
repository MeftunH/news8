@extends('admin.admin_master')
@section('admin')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin Panel </h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
        <a href=" {{ url('/') }} " target="_blank" class="btn btn-outline-light btn-rounded get-started-btn">Go to Site ? </a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Post</h4>

                    <form class="forms-sample" action="{{ route('language.update',$language->id ) }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Name</label>
                                <input type="text" class="form-control" id="exampleInputName1" name="name" required
                                       value="{{ $language->name }}">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Code</label>
                                <input type="text" class="form-control" id="exampleInputName1" name="code" required
                                       value="{{ $language->code }}">
                            </div>

                        </div> <!-- End Row  -->

                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Update Flag Image</label>
                                <input type="file" name="flag" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Old Image</label>
                                <img src="{{ URL::to($language->flag)  }}" style="width: 70px; height: 50px;">
                                <input type="hidden" name="oldflag" value="{{ $language->flag }}">
                            </div>

                        </div> <!-- End Row  -->


                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Locale</label>
                                <input type="text" class="form-control" id="exampleInputName1" name="local" required
                                       value="{{ $language->local }}">
                            </div>
                        </div> <!-- End Row  -->


                        <div class="row">
                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label ">
                                    <input type="checkbox" class="form-check-input" name="status"
                                           value="1" <?php if ( $language->status == 1) {
                                        echo "checked";
                                    } ?> >
                                    Active <i class="input-helper"></i></label>

                            </div>
                        </div>
                        <br><br>

                        <button type="submit" class="btn btn-primary mr-2">Update</button>

                    </form>
                </div>


                <!--
                 This is for Category  -->



@endsection

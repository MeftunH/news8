@extends('admin.admin_master')
@section('admin')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin panel</h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
                          <a href="{{url('/')}}" target="_blank"
                             class="btn btn-outline-light btn-rounded get-started-btn">Go to site</a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Added Posts</h4>


                    <div class="template-demo">
                        {{--                    <button type="button" class="btn btn-success btn-rounded btn-fw">Success</button>--}}
                        <a href="{{route('add.category_segment')}}">
                            <button type="button" class="btn btn-success btn-rounded btn-fw"
                                    style="float: right;scrollbar-base-color: #0a0a0a;">Add Category To Segment
                            </button>
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th> #</th>
                                <th>Section Name</th>
                                <th> Section categories</th>
                                <th> Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php($i=1)

                                @csrf
                                @foreach($seg as $row)

                                    <tr>
                                        <td> {{$i++}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>
                                    @foreach($seg_cat as $item)
                                            @if($item->segment_id == $row->id)
                                                @foreach($categories as $category)
                                                    @if($item->category_id == $category->id)
                                                            {{$category->category_name}},
                                                        @endif
                                                    @endforeach
                                         @endif
                                            @endforeach
                                        </td>
                                        <td><a class="btn btn-info" href="{{route('edit.category_segment',$row->id)}}">Edit </a>
                                            <a class="btn btn-danger" href="{{route('delete.segment',$row->id)}}"
                                               onclick="return confirm('Are you sure to delete segment?')">Delete </a>
                                        </td>
                                    </tr>
                                @endforeach
                                <div class="template-demo">
                                    <button type="submit" class="btn btn-primary mr-2">Save</button>
                                </div>

                            </tbody>
                        </table>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('admin.admin_master')
@section('admin')


    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin Panel </h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
        <a href=" {{ url('/') }} " target="_blank" class="btn btn-outline-light btn-rounded get-started-btn">Go to Site ? </a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Segment</h4>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="forms-sample" action="{{ route('update.segment',$seg->id ) }}" method="post"
                          enctype="multipart/form-data">
                        @csrf

                        <div class="col-12">
                            <label for="name">Segment Name
                            </label><input type="text" class="form-control" id="exampleInputName1"
                                           name="name" value="{{$seg->name}}">
                        </div>

                        <div class="row">
                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label ">
                                    <input type="checkbox" class="form-check-input" name="is_photo_content"
                                           value="1" <?php if ($seg->is_active == 1) {
                                        echo "checked";
                                    } ?> >
                                    Active <i class="input-helper"></i></label>
                            </div>

                        </div>
                        <div class="table-responsive">

                        </div>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th> #</th>
                                <th>Category Name</th>
                                <th> Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php($i=1)  ***Each segment must have 1 main and 3 category content
                            @foreach($categories as $row)

                                <tr>
                                    <td> {{$i++}}</td>
                                    <td>{{$row->category_name}}</td>

                                    <td>
                                        <div class="col-8">
                                            <div class="form-check form-check-success col-md-4">
                                                <label class="form-check-label">
                                                    <input type="hidden" type="checkbox"
                                                           class="form-check-light"
                                                           name="category_id[{{$row->id}}]" id="cat_id" value=0>
                                                    <input type="checkbox" class="form-check-light"
                                                           name="category_id[{{$row->id}}]" id="cat_id" type="checkbox"

                                                    @if(in_array($row->id,$cat_array) )
                                                       echo: checked
                                                           value={{$row->id}}
                                                        @endif
                                                    > Add to Segment <i
                                                        class="input-helper"></i></label>
                                            </div>
                                            <div class="form-check form-check-success-4">
                                                <label class="form-check-label">
                                                    <input type="hidden"
                                                           class="form-check-input"
                                                           name="is_main_category[{{$row->id}}]" value=0>
                                                    <input type="checkbox" class="form-check-input"
                                                           name="is_main_category[{{$row->id}}]"  id="is_main"
                                                           @if($row->id == $main)
                                                           echo : checked
                                                           value=1
                                                           @endif
                                                           > Main <i class="input-helper"></i></label>

                                                <script>

                                                    $('input[id=is_main]').change(function () {
                                                        if ($(this).is(':checked')) {
                                                            $('input[id=is_main]').attr('disabled', true);
                                                            $(this).attr('disabled', false);
                                                        } else {
                                                            $('input[id=is_main]').attr('disabled', false);
                                                        }
                                                    });
                                                </script>
                                                <script>
                                                    $('.form-check-light').on('change', function() {
                                                        if ($('.form-check-light:checked').length > 3) {
                                                            this.checked = false;
                                                        }
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <button type="submit" class="btn btn-primary mr-2">Update</button>

                    </form>
                </div>



@endsection

@extends('admin.admin_master')
@section('admin')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin panel</h4>
                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
                          <a href="{{url('/')}}" target="_blank"
                             class="btn btn-outline-light btn-rounded get-started-btn">Go to site</a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Update SubCategory</h4>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="forms-sample" method="POST" action="{{route('update.subcategory',$get_subcategory->id)}}">
                            @csrf
{{--                            @method('put')--}}
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <ul class="nav nav-pills nav-pills-success" id="pills-tab" role="tablist">
                                            @foreach(\App\Models\Language::all() as $key=> $value)
                                                <li class="nav-item">
                                                    <a class="nav-link {{$loop->index == 0 ? 'active' : null}}"
                                                       id="pills-{{$value->id}}-tab" data-toggle="pill"
                                                       href="#pills-{{$value->id}}" role="tab"
                                                       aria-controls="pills-{{$value->id}}"
                                                       aria-selected="{{$loop->index == 0 ? 'true' : 'false'}}"><img src="{{ URL::to($value->flag)}}" style="width: 16px;height: 16px" alt=""></a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <div class="col-12">
                                            <div class="tab-content" id="pills-tabContent">

                                                @foreach($data as $translation)
                                                    <div
                                                        class="tab-pane fade {{$loop->index == 0 ? 'active show' : null}}"
                                                        id="pills-{{$translation->language_id}}" role="tabpanel"
                                                        aria-labelledby="pills-{{$translation->language_id}}-tab">


                                                        <label>SubCategory Name </label>
                                                        <input type="text" class="form-control" id="exampleInputName1"
                                                               name="subcategory_name[{{$translation->language_id}}]"
                                                               value="{{ $translation->subcategory_name }}">
                                                        <br>


                                                        <script type="text/javascript">
                                                            $(document).ready(function() {
                                                                $('.summernote').summernote();
                                                            });
                                                        </script>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">

                                <label for="exampleFormControlSelect1" class="col-sm-3 col-form-label">
                                    Select Category</label>
                                <div class="col-sm-9">
                                    <label>
                                        <select class="form-control" name="category_id" id="exampleFormControlSelect1">

                                            <option disabled="" selected="">--Select Category--</option>
                                            @foreach($get_category as $cat)

                                                <option  <?php if ($cat->cat_id == $get_subcategory->category_id) echo "selected"?> value="{{$cat->cat_id}}">{{$cat->category_name}}</option>
                                            @endforeach
                                        </select>
                                    </label>
                                </div>

                            </div>

                            <button type="submit" class="btn btn-secondary mr-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
@endsection

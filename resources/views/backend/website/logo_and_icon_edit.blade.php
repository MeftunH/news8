@extends('admin.admin_master')
@section('admin')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin panel</h4>
                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
                          <a href="{{url('/')}}" target="_blank"
                             class="btn btn-outline-light btn-rounded get-started-btn">Go to site</a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Update Website</h4>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="forms-sample" method="POST" action="{{route('settings.update.logo',$settings->id)}}"  enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Logo</label>
                                    <input type="file" name="logo" class="form-control-file" id="exampleFormControlFile1">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Old Logo</label>
                                    <img src="{{ URL::to($settings->logo)  }}" style="width: 70px; height: 50px;">
                                    <input type="hidden" name="oldlogo" value="{{ $settings->logo }}">
                                </div>
                            </div>
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Icon</label>
                                    <input type="file" name="icon" class="form-control-file" id="exampleFormControlFile1">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Old Icon</label>
                                    <img src="{{ URL::to($settings->icon)  }}" style="width: 70px; height: 50px;">
                                    <input type="hidden" name="oldicon" value="{{ $settings->icon }}">
                                </div>
                            </div>


                                <button type="submit" class="btn btn-secondary mr-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
@endsection

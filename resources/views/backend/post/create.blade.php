@extends('admin.admin_master')
@section('admin')

    <style>
        .container {
            max-width: 500px;
        }

        dl, ol, ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .imgPreview img {
            padding: 8px;
            max-width: 100px;
        }
    </style>
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretcha-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin panel</h4>
                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
                          <a href="{{url('/')}}" target="_blank"
                             class="btn btn-outline-light btn-rounded get-started-btn">Go to site</a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add new form</h4>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="forms-sample" action="{{route('store.post')}}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <ul class="nav nav-pills nav-pills-success" id="pills-tab" role="tablist">
                                            @foreach(\App\Models\Language::all() as $key=> $value)
                                                <li class="nav-item">
                                                    <a class="nav-link {{$loop->index == 0 ? 'active' : null}}"
                                                       id="pills-{{$value->id}}-tab" data-toggle="pill"
                                                       href="#pills-{{$value->id}}" role="tab"
                                                       aria-controls="pills-{{$value->id}}"
                                                       aria-selected="{{$loop->index == 0 ? 'true' : 'false'}}">{{ \Illuminate\Support\Str::ucfirst($value->code)}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <div class="col-12">
                                            <div class="tab-content" id="pills-tabContent">

                                                @foreach(\App\Models\Language::all() as $key=> $value)
                                                    <div
                                                        class="tab-pane fade {{$loop->index == 0 ? 'active show' : null}}"
                                                        id="pills-{{$value->id}}" role="tabpanel"
                                                        aria-labelledby="pills-{{$value->id}}-tab">
                                                        <label>Title {{$value['name']}} </label>
                                                        <input type="text" class="form-control" id="exampleInputName1"
                                                               name="title[{{$value['id']}}]">
                                                    <!--                                                        <img src="{{asset($value['flag'])}}"
                                                             style="width: 16px; height: 16px;"
                                                             alt="{{ $value['flag']}}">-->
                                                        <br>

                                                        <div class="form-group">
                                                            <label>Details {{$value['name']}} </label>

                                                            <textarea class="ckeditor form-control" name="details[{{$value['id']}}]"></textarea>

                                                        </div>
                                                        <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

                                                        <script type="text/javascript">

                                                            $(document).ready(function() {
                                                                $('.ckeditor').ckeditor().backgroundColor('#7D001D');


                                                            });
                                                        </script>
                                                        <div class="form-group">
                                                            <label
                                                                for="exampleInputName1">Tags {{$value['name']}}</label>
                                                            <input name="tags[{{$value['id']}}]"
                                                                   id="input-tags{{$loop->index}}"
                                                                   style="background-color: grey !important;"/>
                                                        </div>
                                                        <script type="text/javascript">

                                                            $('#input-tags' + {{$loop->index}}).tagsInput();
                                                        </script>


                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Area</label>

                                    <select class="form-control" id="area_id" name="area_id">
                                        <option disabled="" selected="">--SELECT Area---(Optional)</option>
                                        @foreach($area as $ar)
                                            <option value="{{$ar->id}}">{{$ar->area_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="exampleInputName1">Category</label>

                                    <select class="form-control" id="category_id" name="category_id">
                                        <option disabled="" selected="">--SELECT CATEGORY</option>
                                        @foreach($category as $cat)
                                            <option value="{{$cat->id}}">{{$cat->category_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="subcategory_id">SubCategory</label>

                                    <select class="form-control" id="subcategory_id" name="subcategory_id">
                                        <option disabled="" selected="">--SELECT SUBCATEGORY</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Thumbnail Image Upload</label>
                                <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <br/>
                            <hr>

                            <h4 class="text-center">Options</h4>
                            <div class="row">
                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label ">
                                        <input type="hidden" value="0" id="is_photo_content" name="is_photo_content">
                                        <input type="checkbox" class="form-check-input" id="is_photo_content" name="is_photo_content"
                                               value="1">
                                        Photo <i class="input-helper"></i></label>
                                </div>
                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label ">
                                        <input type="hidden" value="0" name="is_video_content">
                                        <input type="checkbox" class="form-check-input" name="is_video_content"
                                               id="is_video_content" value="1">
                                        Video <i class="input-helper"></i></label>
                                </div>
                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label">
                                        <input type="hidden" value="0" name="is_most_important">
                                        <input type="checkbox" class="form-check-input" name="is_most_important"
                                               value="1">
                                        Most Important News <i class="input-helper"></i></label>
                                </div>
                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="is_big_thumbnail"
                                               value="1">
                                        Big Thumbnail <i class="input-helper"></i></label>
                                </div>
                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label ">
                                        <input type="hidden" value="0" name="is_sub_heading">
                                        <input type="checkbox" class="form-check-input" name="is_sub_heading"
                                               value="1">
                                        Add To Sub Heading Post Section<i class="input-helper"></i></label>
                                </div>
                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label">
                                        <input type="hidden" value="0" name="slider_little_posts">
                                        <input type="checkbox" class="form-check-input" name="slider_little_posts"
                                               value="1">
                                        Add to Slider <i class="input-helper"></i></label>
                                </div>

                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label">
                                        <input type="hidden" value="0" name="heading">
                                        <input type="checkbox" class="form-check-input" name="heading"
                                               value="1">
                                        Heading <i class="input-helper"></i></label>
                                </div>

                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label">
                                        <input type="hidden" value="0" name="is_audio_content">
                                        <input type="checkbox" id="is_audio_content" class="form-check-input"
                                               name="is_audio_content"
                                               value="1">
                                        Audio Content <i class="input-helper"></i></label>
                                </div>


                                <div class="form-check form-check-success col-md-4">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="is_active" value="1">
                                        Active <i class="input-helper"></i></label>
                                </div>

                            </div>

                            @if ($errors->has('files'))
                                @foreach ($errors->get('files') as $error)
                                    <span class="invalid-feedback" role="alert">
                                  <strong>{{ $error }}</strong>
                               </span>
                                @endforeach
                            @endif
                        <br>  <span class="text-center">Album</span><br>
                            <div class="input-group hdtuto control-group lst increment" >
                                <input type="file" id="photo" name="photo_link[]" class="form-control" >
                                <div class="input-group-btn">
                                    <button class="btn btn-success" id="photo" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                                </div>
                            </div>
                            <div class="clone hide">
                                <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                    <input type="file" name="photo_link[]" class="myfrm form-control" >
                                    <div class="input-group-btn">
                                        <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove" ></i> Remove</button>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <span class="text-center">VIDEO</span>
                            <br>
                            <div class="form-group col-md-6">
                                <label for="video_link">Link</label><input type="text" class="form-control"
                                                                           name="video_link" id="video_link" >
                            </div>
                            <span class="text-center">AUDIO</span>
                            <br>
                            <div class="form-group col-md-6">
                                <label for="audio-link">Link</label><input type="text" class="form-control" name="link"
                                                                           id="audio-link" disabled>
                                <label for="url">Or Upload File</label><input type="file" class="form-control"
                                                                              name="url" id="url" disabled>
                            </div>

                            <input type="datetime-local" class="form-control datetimepicker" id="datetime"
                                   name="post_date">
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-dark">Cancel</button>

                        </form>
                    </div>

<script>
    $(function () {
        //listen for checkbox clicked, works for check and uncheck
        $("#is_photo_content").click(function () {
            //get if checked, which returns a boolean value
            var formElementVisible = $(this).is(":checked");

            //show if checked
            if (formElementVisible) {
                $("#photo").removeAttr("disabled");
                return true;
            } else {
                $("#photo").attr("disabled", "disabled");
            }
        });

    });
</script>
                    <script>
                        $(document).ready(function() {
                            $(".btn-success").click(function(){
                                var lsthmtl = $(".clone").html();
                                $(".increment").after(lsthmtl);
                            });
                            $("body").on("click",".btn-danger",function(){
                                $(this).parents(".hdtuto").remove();
                            });
                        });

                        $(function () {
                            //listen for checkbox clicked, works for check and uncheck
                            $("#is_video_content").click(function () {
                                //get if checked, which returns a boolean value
                                var formElementVisible = $(this).is(":checked");

                                //show if checked
                                if (formElementVisible) {
                                    $("#video_link").removeAttr("disabled");
                                    return true;
                                } else {
                                    $("#video_link").attr("disabled", "disabled");
                                }
                            });

                        });
                        $(function () {
                            //listen for checkbox clicked, works for check and uncheck
                            $("#is_audio_content").click(function () {
                                //get if checked, which returns a boolean value
                                var formElementVisible = $(this).is(":checked");

                                //show if checked
                                if (formElementVisible) {
                                    $("#audio-link").removeAttr("disabled");
                                    $("#url").removeAttr("disabled");
                                    return true;
                                } else {
                                    $("#audio-link").attr("disabled", "disabled");
                                    $("#url").attr("disabled", "disabled");
                                }
                            });

                        });

                    </script>
                    <script>
                        $(function () {
                            $("#datetime").datepicker({
                                // dateFormat: 'dd/mm/yyyy',
                                dateFormat: 'd/m/Y',
                                changeMonth: true,
                                changeYear: true
                            });
                        });
                    </script>
                </div>
            </div>
        </div>

        <!--   Sub Category  -->
        <script type="text/javascript">

            $(document).ready(function () {
                $('select[name="category_id"]').on('change', function () {
                    var category_id = $(this).val();
                    if (category_id) {
                        $.ajax({
                            url: "{{  url('/get/subcategory/') }}/" + category_id,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                $("#subcategory_id").empty();
                                $.each(data, function (key, value) {
                                    $("#subcategory_id").append('<option value="' + value.subcategory_id + '">' + value.subcategory_name + '</option>');
                                });
                                console.log(data)
                            },

                        });
                    } else {
                        alert('Sub category is not found');
                    }
                });
            });
        </script>


@endsection

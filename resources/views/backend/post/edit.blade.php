@extends('admin.admin_master')
@section('admin')


    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}"
                                     class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Admin Panel </h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
        <a href=" {{ url('/') }} " target="_blank" class="btn btn-outline-light btn-rounded get-started-btn">Go to Site ? </a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Post</h4>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="forms-sample" action="{{ route('update.post',$post->id ) }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-pills nav-pills-success" id="pills-tab" role="tablist">
                                        @foreach(\App\Models\Language::all() as $key=> $value)
                                            <li class="nav-item">
                                                <a class="nav-link {{$loop->index == 0 ? 'active' : null}}"
                                                   id="pills-{{$value->id}}-tab" data-toggle="pill"
                                                   href="#pills-{{$value->id}}" role="tab"
                                                   aria-controls="pills-{{$value->id}}"
                                                   aria-selected="{{$loop->index == 0 ? 'true' : 'false'}}"><img
                                                        src="{{ URL::to($value->flag)}}"
                                                        style="width: 16px;height: 16px" alt=""></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="col-12">
                                        <div class="tab-content" id="pills-tabContent">

                                            @foreach($translations as $translation)
                                                <div
                                                    class="tab-pane fade {{$loop->index == 0 ? 'active show' : null}}"
                                                    id="pills-{{$translation->language_id}}" role="tabpanel"
                                                    aria-labelledby="pills-{{$translation->language_id}}-tab">


                                                    <label>Title </label>
                                                    <input type="text" class="form-control" id="exampleInputName1"
                                                           name="title[{{$translation->language_id}}]"
                                                           value="{{ $translation->title }}">
                                                    <br>
                                                    <label>Details </label>
                                                    <div class="form-group">
                                                    <textarea class="summernote"
                                                              name="details[{{ $translation->language_id}}]"
                                                              value="{{ $translation->description }}" rows="30"
                                                              id="summernote">{{$translation->description}}</textarea>
                                                    </div>
                                                    <br>
                                                    <label for="exampleInputName1">Tags
                                                    </label>

                                                    <br>

                                                    <input name="tags[{{$translation->language_id}}]"
                                                           id="input-tags{{$loop->index}}"
                                                           @foreach($tags as $language_id => $tag)
                                                           @if($language_id == $translation->language_id)
                                                           value="{{implode(',',$tag)}}"
                                                        @endif

                                                        @endforeach
                                                    />
                                                    <br>
                                                    <script type="text/javascript">
                                                        $('#input-tags' + {{$loop->index}}).tagsInput();
                                                    </script>
                                                    <script type="text/javascript">

                                                        $(document).ready(function () {
                                                            $('.summernote').summernote();
                                                        });
                                                    </script>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">


                        </div> <!-- End Row  -->


                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Area</label>
                                <select class="form-control" id="area_id" name="area_id">
                                    <option disabled="" selected="">--Select Area--</option>
                                    @if($area!=null)
                                        @foreach($area as $row)
                                            <option value="{{ $row->id }}"
                                            <?php if ($row->id == $post->area_id) {
                                                echo "selected";
                                            } ?> >{{ $row->area_name  }} </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Category</label>
                                <select class="form-control" id="category_id" name="category_id">
                                    <option disabled="" selected="">--Select Category--</option>
                                    @foreach($category as $row)
                                        <option value="{{ $row->id }}"
                                        <?php if ($row->id == $post->category_id) {
                                            echo "selected";
                                        } ?> >{{ $row->category_name  }} </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">SubCategory</label>
                                <select class="form-control" name="subcategory_id" id="subcategory_id">
                                    <option disabled="" selected="">--Select SubCategory--</option>

                                    @foreach($sub as $row)
                                        <option value="{{ $row->id }}"
                                        <?php if ($row->id == $post->subcategory_id) {
                                            echo "selected";
                                        } ?> >{{ $row->subcategory_name }} </option>
                                    @endforeach

                                </select>
                            </div>

                        </div> <!-- End Row  -->


                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">News Image Upload</label>
                                <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="exampleInputName1">Old Image</label>
                                <img src="{{ URL::to($post->image)  }}" style="width: 70px; height: 50px;">
                                <input type="hidden" name="oldimage" value="{{ $post->image }}">
                            </div>

                        </div> <!-- End Row  -->


                        <hr>
                        <h4 class="text-center">Extra Options </h4>
                        <br>

                        <div class="row">
                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label ">
                                    <input type="checkbox" class="form-check-input" name="is_photo_content"
                                           value="1" <?php if ($post->is_photo_content == 1) {
                                        echo "checked";
                                    } ?> >
                                    Headline <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label ">
                                    <input type="checkbox" class="form-check-input" id="is_video_content"
                                           name="is_video_content"
                                           value="1" <?php if ($video != null) {
                                        echo "checked";
                                    } ?>>
                                    Video <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label">
                                    <input type="hidden" value="0" name="is_most_important">
                                    <input type="checkbox" class="form-check-input" name="is_most_important"
                                           value="1" <?php if ($post->is_most_important == 1) {
                                        echo "checked";
                                    } ?>>
                                    Most Important News <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="is_big_thumbnail"
                                           value="1" <?php if ($post->is_big_thumbnail == 1) {
                                        echo "checked";
                                    } ?>>
                                    Big Thumbnail <i class="input-helper"></i></label>
                            </div>


                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label ">
                                    <input type="checkbox" class="form-check-input" name="is_sub_heading"
                                           value="1" <?php if ($post->is_sub_heading == 1) {
                                        echo "checked";
                                    } ?>>
                                    Add To Sub Heading Post Section<i class="input-helper"></i></label>
                            </div>

                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label">
                                    <input type="hidden" value="0" name="slider_little_posts">
                                    <input type="checkbox" class="form-check-input" name="slider_little_posts"
                                           value="1" <?php if ($post->slider_little_posts == 1) {
                                        echo "checked";
                                    } ?>>
                                    Add to Slider <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label">
                                    <input type="hidden" value="0" name="heading">
                                    <input type="checkbox" class="form-check-input" name="heading"
                                           value="1" <?php if ($post->heading == 1) {
                                        echo "checked";
                                    } ?>>
                                    Heading<i class="input-helper"></i></label>
                            </div>

                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label">
                                    <input type="hidden" value="0" name="is_audio_content">
                                    <input type="checkbox" id="is_audio_content" class="form-check-input"
                                           name="is_audio_content"
                                           value="1"<?php if ($post->is_audio_content == 1) {
                                        echo "checked";
                                    } ?>>
                                    Audio Content <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check form-check-success col-md-4">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="is_active"
                                           value="1" <?php if ($post->is_active == 1) {
                                        echo "checked";
                                    } ?>>
                                    Active <i class="input-helper"></i></label>
                            </div>
                            <br>
                            <br>
                            <div class="form-group col-md-6">
                                @if ($errors->has('files'))
                                    @foreach ($errors->get('files') as $error)
                                        <span class="invalid-feedback" role="alert">
                                  <strong>{{ $error }}</strong>
                               </span>
                                    @endforeach
                                @endif
                                <br> <span class="text-center">Album</span><br>
                                <div class="input-group hdtuto control-group lst increment">
                                    <input type="file" id="photo" name="photo_link[]" class="form-control">
                                    <div class="input-group-btn">
                                        <button class="btn btn-success" id="photo" type="button"><i
                                                class="fldemo glyphicon glyphicon-plus"></i>Add
                                        </button>
                                    </div>
                                </div>
                                @if(isset($photo))
                                    @foreach(json_decode($photo->photo_link) as $photos)

                                        <div class="clone hide">
                                            <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                                <input type="file" name="photo_link[]" class="myfrm form-control"
                                                       value="{{$photos}}">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-danger" type="button"><i
                                                            class="fldemo glyphicon glyphicon-remove"
                                                            onclick="unset($loop=>index)"></i> Remove
                                                    </button>
                                                </div>
                                            </div>

                                        </div>


                                        <label for="exampleInputName1">Old Image</label>
                                        <img src="{{ asset($photos)  }}" style="width: 70px; height: 50px;">
                                        <input type="hidden" name="old_photos[]" value="{{ $photos }}">

                                    @endforeach
                                @else
                                    <div class="clone hide">
                                        <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                            <input type="file" name="photo_link[]" class="myfrm form-control"
                                                   value="{{null}}">
                                            <div class="input-group-btn">
                                                <button class="btn btn-danger" type="button"><i
                                                        class="fldemo glyphicon glyphicon-remove"></i> Remove
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                @endif
                            </div>

                            <span class="text-center">VIDEO</span>

                            @if(isset($video))
                                <div class="form-group col-md-6">
                                    <label for="video_link">Link</label><input type="text"
                                                                               value="{{$video->video_link}}"
                                                                               class="form-control" name="video_link"
                                                                               id="video_link" disabled>
                                </div>
                            @else
                                <div class="form-group col-md-6">
                                    <label for="video_link">Link</label><input type="text" value="{{null}}"
                                                                               class="form-control" name="video_link"
                                                                               id="video_link">
                                </div>
                            @endif
                            <br>
                            @if(isset($audio))
                                <div class="form-group col-md-6">
                                    <span class="text-center">AUDIO</span>
                                    <br>
                                    <label for="audio-link">Link</label>
                                    <input type="text" class="form-control" value="{{$audio->link}}" name="link"
                                           id="audio-link" @if($post->is_audio_content == 0)   disabled @endif">

                                    <label for="url">Or Upload File</label><input type="file" value="{{$audio->url}}"
                                                                                  class="form-control" name="url"
                                                                                  id="url"
                                                                                  @if($post->is_audio_content == 0)   disabled @endif>

                                </div>
                                <div class="form-group col-md-6">
                                    <label>Old Audio Content</label>
                                    <br>
                                    @if($audio->url != null)
                                        <audio controls>
                                            <source src="
                                    {{$audio->url}}" type="audio/mpeg">
                                        </audio>
                                    @else
                                        <audio controls>
                                            <source src="
                                    {{$audio->link}}" type="audio/mpeg">
                                        </audio>
                                    @endif
                                </div>
                            @else
                                <div class="form-group col-md-6">
                                    <span class="text-center">AUDIO</span>
                                    <label for="audio-link">Link</label>
                                    <input type="text" class="form-control" value="{{null}}" name="link" id="audio-link"
                                           disabled>

                                    <label for="url">Or Upload File</label><input type="file" value="{{null}}"
                                                                                  class="form-control" name="url"
                                                                                  id="url" disabled>
                                </div>
                            @endif
                        </div>
                        <input type="datetime-local" class="form-control datetimepicker" id="datetime" name="post_date"
                               value="{{(\Carbon\Carbon::parse($post->post_date))->toDateTimeLocalString()}}">
                        <br><br>

                        <button type="submit" class="btn btn-primary mr-2">Update</button>

                    </form>
                </div>

                <!--
                              This is for Category  -->
                <script>
                    $(document).ready(function () {
                        $(".btn-success").click(function () {
                            var lsthmtl = $(".clone").html();
                            $(".increment").after(lsthmtl);
                        });
                        $("body").on("click", ".btn-danger", function () {
                            $(this).parents(".hdtuto").remove();

                        });
                    });
                </script>
                <script>


                    $(function () {
                        //listen for checkbox clicked, works for check and uncheck
                        $("#is_video_content").click(function () {
                            //get if checked, which returns a boolean value
                            var formElementVisible = $(this).is(":checked");

                            //show if checked
                            if (formElementVisible) {
                                $("#video_link").removeAttr("disabled");
                                return true;
                            } else {
                                $("#video_link").attr("disabled", "disabled");
                            }
                        });

                    });
                    $(function () {
                        //listen for checkbox clicked, works for check and uncheck
                        $("#is_audio_content").click(function () {
                            //get if checked, which returns a boolean value
                            var formElementVisible = $(this).is(":checked");

                            //show if checked
                            if (formElementVisible) {
                                $("#audio-link").removeAttr("disabled");
                                $("#url").removeAttr("disabled");
                                return true;
                            } else {
                                $("#audio-link").attr("disabled", "disabled");
                                $("#url").attr("disabled", "disabled");
                            }
                        });

                    });

                </script>
                <script>
                    $(function () {
                        $("#datetime").datepicker({
                            // dateFormat: 'dd/mm/yyyy',
                            dateFormat: 'd/m/Y',
                            changeMonth: true,
                            changeYear: true
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('select[name="category_id"]').on('change', function () {
                            var category_id = $(this).val();
                            console.log(category_id)
                            if (category_id) {
                                $.ajax({
                                    url: "{{  url('/get/subcategory/') }}/" + category_id,
                                    type: "GET",
                                    dataType: "json",
                                    success: function (data) {
                                        $("#subcategory_id").empty();
                                        $.each(data, function (key, value) {
                                            $("#subcategory_id").append('<option value="' + value.id + '">' + value.subcategory_name + '</option>');
                                        });
                                    },

                                });
                            } else {
                                alert('danger');
                            }
                        });
                    });
                </script>


@endsection

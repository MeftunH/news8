@extends('admin.admin_master')
@section('admin')


    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card corona-gradient-card">
                    <div class="card-body py-0 px-0 px-sm-3">
                        <div class="row align-items-center">
                            <div class="col-4 col-sm-3 col-xl-2">
                                <img src="{{asset('backend/assets/images/dashboard/Group126@2x.png')}}" class="gradient-corona-img img-fluid" alt="">
                            </div>
                            <div class="col-5 col-sm-7 col-xl-8 p-0">
                                <h4 class="mb-1 mb-sm-0">Welcome to Easy News </h4>

                            </div>
                            <div class="col-3 col-sm-2 col-xl-2 pl-0 text-center">
                        <span>
        <a href=" {{ url('/') }} " target="_blank" class="btn btn-outline-light btn-rounded get-started-btn">Visit Fontend ? </a>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12 margin-tb">

                <div class="pull-left">

                    <h2>Users Management</h2>

                </div>

                <div class="pull-right">

                    <a class="btn btn-success" href="{{ route('users.create') }}"> Create New User</a>

                </div>

            </div>

        </div>



        @if ($message = Session::get('success'))

            <div class="alert alert-success">

                <p>{{ $message }}</p>

            </div>

        @endif



        <table class="table table-bordered">

            <tr>

                <th>No</th>

                <th>Name</th>

                <th>Email</th>

                <th>Roles</th>

                <th width="280px">Action</th>

            </tr>

            @foreach ($data as $key => $user)

                <tr>

                    <td>{{ ++$i }}</td>

                    <td>{{ $user->name }}</td>

                    <td>{{ $user->email }}</td>

                    <td>

                        @if(!empty($user->getRoleNames()))

                            @foreach($user->getRoleNames() as $v)

                                <label class="badge badge-success">{{ $v }}</label>

                            @endforeach

                        @endif

                    </td>

                    <td>

                        <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>

                        <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>

                        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}

                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

                        {!! Form::close() !!}

                    </td>

                </tr>

            @endforeach

        </table>



    {!! $data->render() !!}




@endsection

@extends('main.home_master')
@section('content')
    <br>
    <section class="categories-news">
        <div class="container">


            <div class="row">

                @foreach($multimedia as $cp)
                    <div class="col-sm-3 col-6">
                        <div class="news-block__medium news-block__medium_media news-block__medium_region">
                            <a href=""><img src="{{url($cp->image)}}" alt="" class="news-block__image img-fluid"></a>
                            <div class="news-block__info-wrapper">
                                <div class="news-block__info">
                                    <a href="" class="news-block__info-link">{{$cp->title}}</a>

                                    @if($cp->is_video_content==1)
                                        <span>{{trans('front.video')}}</span>
                                    @elseif($cp->is_photo_content==1)
                                        <span>{{trans('front.photo')}}</span>
                                    @elseif($cp->is_audio_content==1)
                                        <span>{{trans('front.audio')}}</span>
                                    @endif
                                    <span class="news-block__date">{{\App\Models\Post::DateTranslate($cp->post_date)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection

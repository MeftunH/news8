<!-- Footer -->
<footer class="footer">
    <div class="footer__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-12">

                    <a href=""><img src="  {{asset('frontend/assets/img/logo-white.svg')}}" alt="" class="footer__logo img-fluid"></a>
                </div>
                <div class="col-lg-5 col-md-4 col-sm-6">
                    <h3 class="footer__title">{{trans('front.categories')}}</h3>
                    <ul class="footer__list">
                        <li><a href="{{url('/')}}" class="footer__list-link">{{trans('front.main news')}}</a></li>
                        <li><a href="{{route('front.lastNews')}}" class="footer__list-link">{{trans('front.latest news')}}</a></li>
                        @foreach($category_head as $cat)
                            <li><a href="{{( route('front.catPost',['id'=>$cat->id] ) )}}" class="footer__list-link">{{$cat->category_name}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-6">
                    <h3 class="footer__title">{{trans('front.multimedia')}}</h3>
                    <ul class="footer__list">
                        <li><a href="{{route('front.photoNews')}}" class="footer__list-link">{{trans('front.photo')}}</a></li>
                        <li><a href="{{route('front.videoNews')}}" class="footer__list-link">{{trans('front.video')}}</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-3 col-6">
                    <h3 class="footer__title">{{trans('front.contact_us')}}</h3>
                    <ul class="nav__social-list">
                        <li>
                            <a href="#" target="_blank"><i ></i></a>
                        </li>
                        @if($socials_head)
                            @if($socials_head->facebook!=null)
                                <a href="{{$socials_head->facebook}}" target="_blank"><i class="so-facebook"></i></a>
                            @endif
                            @if($socials_head->twitter!=null)
                                <a href="{{$socials_head->twitter}}" target="_blank"><i class="so-twitter"></i></a>
                            @endif
                            {{--                                    @if($socials_head->youtube!=null)--}}
                            {{--                                        <a href="{{$socials_head->youtube}}" target="_blank"><i class="so-youtube"></i></a>--}}
                            {{--                                    @endif--}}
                            @if($socials_head->linkedin!=null)
                                <a href="{{$socials_head->linkedin}}" target="_blank"><i class="so-linkedin"></i></a>
                            @endif
                            @if($socials_head->instagram!=null)
                                <a href="{{$socials_head->instagram}}" target="_blank"><i class="so-instagram"></i></a>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-auto">
                    <span class="footer__copyright">© 2020 Pronews Company</span>
                </div>
                <div class="col-auto">
                    <a href="{{( route('front.aboutUs' ))}}" class="footer__about">{{trans('front.about_us')}}</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- /.Footer -->

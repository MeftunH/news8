<!-- Header -->
<header class="header">
    <div class="nav__info d-none d-lg-block">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-7">
                    <div class="nav__info-left">
                        <div class="nav__info-weather">
                            <i class="so-cloudy"></i>

                            <span>Bakı 14° C – 4 m/s</span>
                        </div>
                        <div class="nav__info-currecny">
                            <ul class="nav__currency-list">
                                <li>USD - 1.7000</li>
                                <li>EUR - 1.9956</li>
                                <li>RUB - 0.0219</li>
                                <li>BRENT - 42.44</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex justify-content-end">
                    <div class="nav__info-right">
                        <div class="nav__info-social">
                            <ul class="nav__social-list">
                                <li>
                                    <a href="#" target="_blank"><i ></i></a>
                                </li>
                                @if($socials_head)
                                    @if($socials_head->facebook!=null)
                                        <a href="{{$socials_head->facebook}}" target="_blank"><i class="so-facebook"></i></a>
                                    @endif
                                    @if($socials_head->twitter!=null)
                                        <a href="{{$socials_head->twitter}}" target="_blank"><i class="so-twitter"></i></a>
                                    @endif
{{--                                    @if($socials_head->youtube!=null)--}}
{{--                                        <a href="{{$socials_head->youtube}}" target="_blank"><i class="so-youtube"></i></a>--}}
{{--                                    @endif--}}
                                    @if($socials_head->linkedin!=null)
                                        <a href="{{$socials_head->linkedin}}" target="_blank"><i class="so-linkedin"></i></a>
                                    @endif
                                    @if($socials_head->instagram!=null)
                                        <a href="{{$socials_head->instagram}}" target="_blank"><i class="so-instagram"></i></a>
                                    @endif
                                @endif
                            </ul>
                        </div>

                        <div class="nav__info-about">
                            <a href="{{( route('front.aboutUs' ))}}" class="nav__info-link">{{trans('front.about_us')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nav__main">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-auto">
                    <div class="nav__main-left">
                        <a href="/"><img src="{{asset('frontend/assets/img/logo.svg')}}" class="img-fluid nav__main-logo" alt="ProNews.az"></a>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="nav__main-right">

                        <form action="{{ route('front.search')}}" method="GET">
                            <input type="text" name="search" id="search-input" class="nav__main-search-input" placeholder="{{trans('front.search')}}...">
                        </form>

                        <a href="javascript:void(0);" class="nav__main-search">
                            <i class="so-search"></i>
                        </a>
                        <a href="javascript:void(0);" class="nav__burger d-lg-none d-flex"><span class="nav__burger-line"></span></a>
                        <div class="nav__language d-none d-lg-block">
                            <a href="javascript:void(0);" class="nav__language-link dropdownButton">{{app()->getLocale()}}<i
                                    class="so-arrow-down"></i></a>
                            @foreach($languages as $key=>$lang)
                            <ul class="nav__language-list dropdownMenu">
                                <li class="nav__language-item">
                                    <a href="{{LaravelLocalization::getLocalizedURL($lang->code, null, [], true)}}">{{$lang->code}}</a>
                                </li>

                            </ul>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nav__categories d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="nav__categories-list">
                        <li><a href="{{url('/')}}" class="nav__categories-link">{{trans('front.main news')}}</a></li>
                        <li><a href="{{route('front.lastNews')}}" class="nav__categories-link">{{trans('front.latest news')}}</a></li>
                        @foreach($category_head as $cat)
                            <li><a href="{{( route('front.catPost',['id'=>$cat->id] ) )}}" class="nav__categories-link">{{$cat->category_name}}</a></li>
                        @endforeach
                        <li><a href="{{route('front.multiMedia')}}" class="nav__categories-link">{{trans('front.multimedia')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="nav__burger-menu-wrapper">
        <div class="nav__burger-menu-overlay"></div>
        <div class="nav__burger-menu">
            <a href="javascript:void(0);" class="nav__burger-close"><i class="so-close"></i></a>
            <div class="nav__burger-header">
                <div class="nav__burger-weather"></div>
                <div class="nav__burger-languages"></div>
            </div>
            <div class="nav__burger-content"></div>
            <div class="nav__burger-footer">
                <div class="nav__burger-about"></div>
                <div class="nav__burger-social"></div>
            </div>
        </div>
    </div>
</header>
<!-- /.Header -->

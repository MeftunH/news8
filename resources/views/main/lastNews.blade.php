@extends('main.home_master')
@section('content')
    <br>
    <section class="categories-news">
        <div class="container">

            <div class="row">
                <div class="col-12">
                    <ul class="region-news__button-list">
                        <li><a href="{{route('front.lastNewsToday')}}"  class="region-news__button">{{trans('front.today')}}</a></li>
                        <li><a href="{{route('front.lastNewsYesterday')}}"  class="region-news__button">{{trans('front.yesterday')}}</a></li>
                        <li><a href="{{route('front.lastNewsThisWeek')}}"  class="region-news__button">{{trans('front.this_week')}}</a></li>
                        <li><a href="{{route('front.lastNewsThisMonth')}}"  class="region-news__button">{{trans('front.this_month')}}</a></li>
                        <li><a href="{{route('front.lastNewsPrevWeek')}}"  class="region-news__button">{{trans('front.prev_week')}}</a></li>
                        <li><a href="{{route('front.lastNewsLastMonth')}}"  class="region-news__button">{{trans('front.last_month')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">

                @foreach($lastNews as $cp)
                    <div class="col-sm-3 col-6">
                        <div class="news-block__medium news-block__medium_media news-block__medium_region">
                            <a href=""><img src="{{url($cp->image)}}" alt="" class="news-block__image img-fluid"></a>
                            <div class="news-block__info-wrapper">
                                <div class="news-block__info">
                                    <a href="" class="news-block__info-link">{{$cp->title}}</a>
                                    <span class="news-block__date">{{\App\Models\Post::DateTranslate($cp->post_date)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function() {
            $("#button_1").click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/pages/test/",
                    data: {
                        id: $("#button_1").val(),
                        access_token: $("#access_token").val()
                    },
                    success: function(result) {
                        alert('ok');
                    },
                    error: function(result) {
                        alert('error');
                    }
                });
            });
    </script>
@endsection

<!DOCTYPE html>
<html lang="az">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
          name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>@yield('title')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <meta content="" property="og:title" />
    <meta content="" property="og:description" />
    <meta content="" property="og:image" />
    <meta content="website" property="og:type" />
    <meta content="" property="og:url" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('frontend/assets/img/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('frontend/assets/img/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('frontend/assets/img/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('frontend/assets/img/site.webmanifest')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('node_modules/air-datepicker/dist/css/datepicker.css')}}">

    @yield('css')
</head>

<body>
@include('main.body.header')
@yield('content')
@include('main.body.footer')


<script src="{{asset('frontend/assets/js/app.min.js')}}"></script>
@yield('js')
</body>

</html>

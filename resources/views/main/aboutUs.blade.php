@extends('main.home_master')

@section('title')
    {{trans('front.about_us')}}
@endsection

@section('css')
@endsection

@section('content')
    <!-- Menu Section Start -->
@if(isset($aboutUs))
    {!!     $aboutUs->about_us_translation !!}
@endif
@endsection

@section('js')
@endsection

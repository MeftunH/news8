@extends('main.home_master')

@section('title')
    Ana Sehife
@endsection

@section('css')
@endsection

@section('content')
    <!-- Intro -->
    <section class="intro">
        <div class="container">
            <div class="row">
                <div class="col-12 p-0 p-lg-3">
                    <div class="swiper-container slider__main" data-slides-lg="1" data-slides-md="1" data-slides-sm="1"
                         data-slides-xs="1" data-loop="true" data-autoplay="true" data-margin="0">
                        <div class="swiper-wrapper">
                            @foreach($slider as $row)
                                <div class="swiper-slide">

                                    <div class="swiper-slide__wrapper" style="background-image: url({{$row->image}});">

                                        <div class="swiper-slide__info">

                                            <a href="{{( route('front.catPost',['id'=>$row->category_id] ) )}}" class="swiper-slide__category">{{$row->category_name}}</a>
                                            <a href="{{route('front.post_view', [$row->id])}}" class="swiper-slide__title">{{$row->title}}</a>
                                            <span
                                                class="swiper-slide__date">{{\App\Models\Post::DateTranslate($row->post_date)}}</span>
                                        </div>

                                        <div class="swiper-slide__latest-wrapper d-lg-flex d-none">

                                            @foreach($slider_little_posts as $little)
                                                @if($row->category_id == $little->category_id)
                                                    <div class="swiper-slide__latest">
                                                        <a href="{{route('front.post_view', [$little->id])}}"
                                                           class="swiper-slide__latest-title">{{$little->title}} </a>
                                                        <span
                                                            class="swiper-slide__latest-date">{{\App\Models\Post::DateTranslate($little->post_date)}}</span>
                                                    </div>
                                                    @break($loop->index == 4)
                                                @endif
                                            @endforeach

                                        </div>

                                    </div>
                                </div>@endforeach
                        </div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.Intro -->
    <!-- Latest News -->
    <section class="latest-news">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    @if(isset($heading))
                        <div class="latest-news__big" style="background-image: url({{$heading->image}})">
                            <div class="latest-news__big-info">

                                <a href="{{( route('front.catPost',['id'=>$heading->category_id] ) )}}" class="latest-news__big-category">{{$heading->category_name}}</a>

                                <a href="{{route('front.post_view', [$heading->id])}}"
                                   class="latest-news__big-title">{{$heading->title}}</a>
                                <p class="latest-news__big-description d-none d-sm-block">{{ strip_tags(str_limit($heading->description, 20))}}</p>
                                <span
                                    class="latest-news__big-date">{{\App\Models\Post::DateTranslate($heading->post_date)}}</span>
                            </div>
                        </div>
                    @elseif(isset($if_today_heading_not_found))
                        <div class="latest-news__big"
                             style="background-image: url({{$if_today_heading_not_found->image}})">
                            <div class="latest-news__big-info">

                                <a href="{{( route('front.catPost',['id'=>$if_today_heading_not_found->category_id] ) )}}"
                                   class="latest-news__big-category">{{$if_today_heading_not_found->category_name}}</a>
                                <a href="{{route('front.post_view', [$if_today_heading_not_found->id])}}" class="latest-news__big-title">{{$if_today_heading_not_found->title}}</a>
                                <p class="latest-news__big-description d-none d-sm-block">{{ strip_tags(str_limit($if_today_heading_not_found->description, 20))}}</p>
                                <span
                                    class="latest-news__big-date">{{\App\Models\Post::DateTranslate($if_today_heading_not_found->post_date)}}</span>
                            </div>
                        </div>
                    @else
                        <div class="latest-news__big"
                             style="background-image: url({{$if_any_heading_content_not_found->image}})">
                            <div class="latest-news__big-info">

                                <a href="{{( route('front.catPost',['id'=>$if_any_heading_content_not_found->category_id] ) )}}"
                                   class="latest-news__big-category">{{$if_any_heading_content_not_found->category_name}}</a>
                                <a href="{{route('front.post_view', [$if_any_heading_content_not_found->id])}}"
                                   class="latest-news__big-title">{{$if_any_heading_content_not_found->title}}</a>
                                <p class="latest-news__big-description d-none d-sm-block">{{ strip_tags(str_limit($if_any_heading_content_not_found->description, 20))}}</p>
                                <span
                                    class="latest-news__big-date">{{\App\Models\Post::DateTranslate($if_any_heading_content_not_found->post_date)}}</span>
                            </div>
                        </div>
                    @endif
                    <div class="row latets-news__small-list">
                        @if(isset($is_sub_heading))
                            @foreach($is_sub_heading as $row)

                                <div class="col-sm-3 col-6">
                                    <article class="latest-news__small">
                                        <a href="{{route('front.post_view', [$row->id])}}"><img
                                                src="{{asset($row->image)}}" alt=""
                                                class="latest-news__small-image img-fluid"></a>
                                        <span class="latest-news__small-category">{{$row->category_name}}</span>
                                        <a href="{{route('front.post_view', [$row->id])}}"
                                           class="latest-news__small-title">{{$row->title}}</a>
                                        <span
                                            class="latest-news__small-date">{{\App\Models\Post::DateTranslate($row->post_date)}}</span>
                                    </article>
                                </div>

                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="latest-news__header">
                        <h3>{{trans('front.latest_news')}}</h3>
                    </div>
                    <div class="latest-news__right">
                        <ul class="latest-news__list">

                            @foreach($latest as $row)
                                <li class="latest-news__item">
                                    <span
                                        class="latest-news__item-date">{{ Carbon\Carbon::parse($row->post_date)->format('H:i') }}</span>
                                    <div class="latest-news__item-info">
                                        <a href="{{route('front.post_view', [$row->id])}}" class="latest-news__item-link">{{$row->title}}</a>
                                        <a href="{{( route('front.catPost',['id'=>$row->category_id] ) )}}"
                                           class="latest-news__item-category_link">

                                            @if($row->is_video_content==1)
                                                <span>{{trans('front.video')}}</span>
                                            @elseif($row->is_photo_content==1)
                                                <span>{{trans('front.photo')}}</span>
                                            @elseif($row->is_audio_content==1)
                                                <span>{{trans('front.audio')}}</span>
                                            @endif
                                            {{$row->category_name}}
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <a href="{{route('front.lastNews')}}"
                           class="latest-news__button">{{trans('front.all_news')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.Latest News -->
    <!-- Important News -->
    <section class="important-news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="latest-news__header latest-news__header_pr d-flex justify-content-between">
                        <h3>{{trans('front.most_important_news')}}</h3>
                        <div class="latest-news__header-navigation">
                            <div class="important-news__prev">
                                <i class="so-arrow-left"></i>
                            </div>
                            <div class="important-news__next">
                                <i class="so-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="swiper-container slider__important-news" data-slides-lg="3" data-slides-md="2"
                         data-slides-sm="2"
                         data-slides-xs="1" data-loop="true" data-autoplay="true" data-margin="20"
                         data-navigation-next=".important-news__next" data-navigation-prev=".important-news__prev"
                         data-slide-index=".swiper-slide__num">
                        <div class="swiper-wrapper">
                            @foreach($most_important_news as $imp)
                                <div class="swiper-slide">
                                    <div class="swiper-slide__wrapper">
                                        <img src="{{asset($imp->image)}}" alt="{{$imp->image}}"
                                             class="swiper-slide__image img-fluid">
                                        <div class="swiper-slide__info-wrapper">
                                            <span class="swiper-slide__num"></span>
                                            <div class="swiper-slide__info">
                                                <a href="{{( route('front.catPost',['id'=>$imp->category_id] ) )}}"
                                                   class="swiper-slide__info-category">{{$imp->category_name}}</a>
                                                <a href="{{route('front.post_view', [$imp->id])}}" class="swiper-slide__info-link">{{$imp->title}}</a>
                                                <span
                                                    class="swiper-slide__date">{{\App\Models\Post::DateTranslate($imp->post_date)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.Important News -->
    <!-- Audio News -->

    <section class="audio-news important-news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="latest-news__header">
                        <h3><i class="so-volume"></i>{{trans('front.audio_news')}}</h3>
                        <div class="latest-news__header-navigation">
                            <div class="audio-news__prev">
                                <i class="so-arrow-left"></i>
                            </div>
                            <div class="audio-news__next">
                                <i class="so-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="swiper-container slider__important-news" data-slides-lg="3" data-slides-md="2"
                         data-slides-sm="2"
                         data-slides-xs="1" data-loop="true" data-autoplay="true" data-margin="20"
                         data-navigation-next=".audio-news__next" data-navigation-prev=".audio-news__prev">
                        <div class="swiper-wrapper">
                            @foreach($audio_news as $row)
                                <div class="swiper-slide">
                                    <div class="swiper-slide__wrapper">
                                        <img src="{{$row->image}}" alt=""
                                             class="swiper-slide__image img-fluid">
                                        <div class="swiper-slide__info-wrapper">

                                            <div class="swiper-slide__info m-0">
                                                <a href="{{( route('front.catPost',['id'=>$row->category_id] ) )}}"
                                                   class="swiper-slide__info-category">
                                                        @if(isset($row->audio_link))
                                                            <audio controls>
                                                                <source src="{{$row->audio_link}}" />
                                                            </audio>
                                                        @else
                                                        <audio controls>
                                                            <source src=" {{$row->url}}" />
                                                        </audio>
                                                        @endif
                                                    {{$row->category_name}}</a>
                                                <a href="{{route('front.post_view', [$row->id])}}" class="swiper-slide__info-link">{{$row->title}}</a>
                                                <span
                                                    class="swiper-slide__date">{{\App\Models\Post::DateTranslate($row->post_date)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.Audio News -->
    {{$cat_val = ''}}
    @foreach($all as $key=> $value)
        <section class="news-block">
            <div class="container">
                <div class="row">


                    @foreach($value as $k=>$v)
                        @if($v->is_main_category ==0)

                            <div class="col-lg-3">
                                <div class="latest-news__header">
                                    <h3>{{$v->category_name}}</h3>
                                </div>
                                @foreach($if_posts_big as $nmp_big)
                                    @foreach($nmp_big as $key=>$value)
                                        @if($value->category_id == $v->category_id)
                                            <div class="news-block__medium">
                                                <img src="{{$value->image}}" alt="" class="news-block__image img-fluid">
                                                <div class="news-block__info-wrapper">
                                                    <div class="news-block__info">
                                                        <a href="{{( route('front.catPost',['id'=>$value->category_id] ) )}}"
                                                           class="latest-news__item-category latest-news__item-category_link">{{$value->category_name}}</a>
                                                        <a href="{{route('front.post_view', [$value->id])}}" class="news-block__info-link">{{$value->title}}</a>
                                                        <span
                                                            class="news-block__date">{{\App\Models\Post::DateTranslate($value->post_date)}}</span>
                                                        <div hidden>
                                                            {{$cat_val = $value->category_name}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        @endif
                                    @endforeach
                                @endforeach
                                @foreach($if_not_main_posts as $fmp=>$fmp_values)

                                    @foreach($fmp_values as $fey=>$fValue)
                                        @if($fValue->category_id == $v->category_id)

                                            <ul class="news-block__list">
                                                <li class="latest-news__item latest-news__item_block">
                                                    <img src="{{$fValue->image}}" alt=""
                                                         class="latest-news__item-image img-fluid">
                                                    <div class="latest-news__item-info">
                                                        <a href="{{route('front.post_view', [$fValue->id])}}" class="latest-news__item-link">{{$fValue->title}}</a>
                                                        <a href="{{( route('front.catPost',['id'=>$fValue->category_id] ) )}}"
                                                           class="latest-news__item-category latest-news__item-category_link">@if($fValue->is_photo_content == 1)
                                                                <span>{{trans('front.photo')}}</span>@endif
                                                            {{$fValue->category_name}}</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        @endif
                                    @endforeach
                                @endforeach

                                <a href="" class="latest-news__button mb-5 mb-lg-0">{{$cat_val}}</a>

                            </div>
                            {{$cat_name = ""}}
                        @elseif($v->is_main_category ==1)
                            <div class="col-lg-6">
                                <div class="latest-news__header">
                                    <h3>{{$v->category_name}}</h3>
                                </div>
                                @foreach($if_posts_big as $nmp_big)
                                    @foreach($nmp_big as $key=>$value)
                                        @if($value->category_id == $v->category_id)
                                            <div class="news-block__medium-wrapper">
                                                <div class="news-block__medium news-block__medium_big">
                                                    <img src="{{ $value->image}}" alt=""
                                                         class="news-block__image img-fluid">
                                                    <div class="news-block__info-wrapper">
                                                        <div class="news-block__info">
                                                            <a href="{{( route('front.catPost',['id'=>$value->category_id] ) )}}" class="news-block__info-category">{{$value->category_name}}</a>
                                                            <a href={{route('front.post_view', [$value->id])}}"" class="news-block__info-link">{{$value->title}}</a>
                                                            <span
                                                                class="news-block__date">{{\App\Models\Post::DateTranslate($value->post_date)}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @foreach($if_main_posts as $fmp=>$fmp_values)

                                                    @foreach($fmp_values as $fey=>$fValue)
                                                        @if($fValue->category_id == $v->category_id)
                                                <ul class="news-block__list news-block__list_big">

                                                    <li class="latest-news__item latest-news__item_block">
                                                        <img src="{{$fValue->image}}" alt=""
                                                             class="latest-news__item-image img-fluid">
                                                        <div class="latest-news__item-info">
                                                            <a href="{{route('front.post_view', [$fValue->id])}}" class="latest-news__item-link">{{$fValue->title}}</a>
                                                            <a href="{{( route('front.catPost',['id'=>$fValue->category_id] ) )}}"
                                                               class="latest-news__item-category latest-news__item-category_link">@if($fValue->is_photo_content == 1)
                                                                    <span>{{trans('front.photo')}}</span>@endif {{$fValue->category_name}}</a>
                                                            <div hidden>
                                                                {{$cat_name = $fValue->category_name}}
                                                            </div>
                                                        </div>
                                                    </li>
                                                        @endif
                                                    @endforeach
                                                </ul>

                                                @endforeach
                                            </div>
                                        @endif
                                    @endforeach
                                @endforeach
                                <a href="{{( route('front.catPost',['id'=>$v->category_id] ) )}}" class="latest-news__button mb-5 mb-lg-0">{{$cat_name}}</a>
                            </div>

                        @endif
                    @endforeach  </div>
            </div>
        </section>
    @endforeach


    <section class="video-news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="latest-news__header">
                        <h3>{{trans('front.video_news')}}</h3>
                        <a href="{{route('front.videoNews')}}"
                           class="video-news__header-link">{{trans('front.show_all')}}<i
                                class="so-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 video-news__border-right">
                    <div class="news-block__medium news-block__medium_big news-block__medium_media">
                        <img src="{{asset('frontend/assets/img/icons/play.svg')}}" class="video-news__play" alt="">
                        <img src="{{$video_big->image}}" alt="" class="news-block__image img-fluid">
                        <div class="news-block__info-wrapper">
                            <div class="news-block__info">
                                <a href="{{route('front.post_view', [$video_big->id])}}"
                                   class="news-block__info-category latest-news__item-category"><span>{{trans('front.video')}}</span> {{$video_big->category_name}}
                                </a>
                                <a href="{{route('front.post_view', [$video_big->id])}}" class="news-block__info-link">{{$video_big->title}}</a>
                                <span
                                    class="news-block__date">{{\App\Models\Post::DateTranslate($video_big->post_date)}}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 d-flex flex-column justify-content-between">

                    <div class="row">
                        @foreach($video_small as $vs)
                            <div class="col-6">
                                <div class="news-block__medium news-block__medium_media">
                                    <img src="{{asset('frontend/assets/img/icons/play.svg')}}"
                                         class="video-news__play video-news__play_small" alt="">
                                    <img src="{{$vs->image}}" alt="" class="news-block__image img-fluid">
                                    <div class="news-block__info-wrapper">
                                        <div class="news-block__info">
                                            <a href=""
                                               class="news-block__info-category latest-news__item-category"><span>{{trans('front.video')}}</span> {{$vs->category_name}}
                                            </a>
                                            <a href="{{route('front.post_view', [$vs->id])}}" class="news-block__info-link">{{$vs->title}}</a>
                                            <span
                                                class="news-block__date">{{\App\Models\Post::DateTranslate($vs->post_date)}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                        <div class="row video-news__border-top">
                            @foreach($video_footer as $vf)
                            <div class="col-sm-6 col-12">
                                <div
                                    class="latest-news__item latest-news__item_block latest-news__item_media mb-5 mb-lg-0">
                                    <div class="video-news__play-wrapper">
                                        <img src="{{asset('frontend/assets/img/icons/play.svg')}}"
                                             class="video-news__play video-news__play_small" alt="">
                                        <img src="{{$vf->image}}" alt="" class="latest-news__item-image">
                                    </div>
                                    <div class="latest-news__item-info">
                                        <a href="{{route('front.post_view', [$vf->id])}}" class="latest-news__item-link">{{$vf->title}}</a>
                                        <a href="" class="latest-news__item-category"><span>
                                            @if($vs->is_video_content==1)
                                                    {{trans('front.video')}}
                                                @endif
                                        </span> {{$vs->category_name}}</a>
                                    </div>
                                </div>
                            </div> @endforeach

                        </div>

                </div>

            </div>
        </div>
    </section>
    <!-- Important News -->
    <section class="important-news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="latest-news__header latest-news__header_pr d-flex justify-content-between">
                        <h3>{{trans('front.most_viewed_news')}}</h3>
                        <div class="latest-news__header-navigation">
                            <div class="important-news__prev">
                                <i class="so-arrow-left"></i>
                            </div>
                            <div class="important-news__next">
                                <i class="so-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">

                    <div class="swiper-container slider__important-news" data-slides-lg="3" data-slides-md="2"
                         data-slides-sm="2"
                         data-slides-xs="1" data-loop="true" data-autoplay="true" data-margin="20"
                         data-navigation-next=".important-news__next" data-navigation-prev=".important-news__prev"
                         data-slide-index=".swiper-slide__num_popular">

                        <div class="swiper-wrapper">
                            @foreach($most_viewed_news as $mvn)
                                <div class="swiper-slide">
                                    <div class="swiper-slide__wrapper">
                                        <img src="{{$mvn->image}}" alt="" class="swiper-slide__image img-fluid">
                                        <i class="mdi-eye-settings">{{$mvn->view_count}} view</i>
                                        <div class="swiper-slide__info-wrapper">
                                            <span class="swiper-slide__num swiper-slide__num_popular"></span>
                                            <div class="swiper-slide__info">
                                                <a href="{{( route('front.catPost',['id'=>$mvn->category_id] ) )}}"
                                                   class="swiper-slide__info-category">{{$mvn->category_name}}</a>
                                                <a href="{{( route('front.post_view',['id'=>$mvn->id] ) )}}" class="swiper-slide__info-link">{{$mvn->title}}</a>


                                                <span
                                                    class="swiper-slide__date">{{\App\Models\Post::DateTranslate($mvn->post_date)}}</span>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- /.Important News -->
    <!-- Media News -->

    <section class="media-news video-news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="latest-news__header">
                        <h3>{{trans('front.multimedia')}}</h3>
                        <a href="{{route('front.multiMedia')}}"
                           class="video-news__header-link">{{trans('front.show_all')}}<i
                                class="so-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-md-8 video-news__border-right">
                    <div
                        class="news-block__medium news-block__medium_big news-block__medium_media news-block__medium_video"
                        style="background-image: url('{{$multimedia_big->image}}');">
                        <div class="news-block__info-wrapper">
                            <div class="news-block__info">
                                <a href=""
                                   class="news-block__info-category latest-news__item-category">
                                    @if ($multimedia_big->is_photo_content==1)
                                        <span>{{trans('front.photo')}}</span>
                                    @elseif($multimedia_big->is_video_content==1)
                                        <span>{{trans('front.video')}}</span></a>
                                @endif
                                {{$multimedia_big->category_name}}
                                <a href="{{route('front.post_view', [$multimedia_big->id])}}" class="news-block__info-link">{{$multimedia_big->title}}</a>
                                <span
                                    class="news-block__date">{{\App\Models\Post::DateTranslate($multimedia_big->post_date)}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        @foreach($multimedia_small as $ms)
                            <div class="col-md-12 col-sm-6 col-12">
                                <div
                                    class="news-block__medium news-block__medium_media news-block__medium_video news-block__medium_video_mb-2"
                                    style="background-image: url('{{$ms->image}}');">
                                    <img src="{{asset('frontend/assets/img/icons/play.svg')}}" class="video-news__play"
                                         alt="">
                                    <div class="news-block__info-wrapper">
                                        <div class="news-block__info">
                                            <a href=""
                                               class="news-block__info-category latest-news__item-category"> @if ($ms->is_photo_content==1)
                                                    <span>{{trans('front.photo')}}</span>
                                                @elseif($ms->is_video_content==1)
                                                @endif
                                                {{$ms->category_name}}</a>
                                            <a href="{{route('front.post_view', [$ms->id])}}" class="news-block__info-link">{{$ms->title}}</a>
                                            <span
                                                class="news-block__date">{{\App\Models\Post::DateTranslate($ms->post_date)}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.Media News -->
    <!-- Region News -->
    <section class="region-news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="latest-news__header d-flex align-items-center justify-content-between">
                        <h3>{{trans('front.area_news')}}</h3>
                        <a href="{{route('front.areaNews')}}"
                           class="video-news__header-link">{{trans('front.all_area_news')}}<i
                                class="so-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <ul class="region-news__button-list">
                        @foreach($areas as $area)
                            <li><a href="{{( route('front.areaPosts',['id'=>$area->id] ) )}}"
                                   class="region-news__button">{{$area->area_name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="row">
                @foreach($area_news as $an)
                    <div class="col-sm-4 col-6">
                        <div class="news-block__medium news-block__medium_media news-block__medium_region">
                            <a href="{{route('front.post_view', [$an->id])}}"><img src="{{$an->image}}" alt="" class="news-block__image img-fluid"></a>
                            <div class="news-block__info-wrapper">
                                <div class="news-block__info">
                                    <a href="" class="news-block__info-category">{{$an->category_name}}</a>
                                    <a href="{{route('front.post_view', [$an->id])}}" class="news-block__info-link">{{$an->title}}</a>
                                    <span class="news-block__info-region">{{$an->area_name}}</span>
                                    <span
                                        class="news-block__date">{{\App\Models\Post::DateTranslate($an->post_date)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- /.Region News -->
@endsection

@section('js')
@endsection

@extends('main.home_master')
@section('content')

    <!-- Post Header Section Start -->
    <div class="post-header-section section mt-30 mb-30">
        <div class="container">
            <div class="row row-1">

                <!-- Page Banner Start -->
                <div class="col-12">
                    <div class="post-header" style="background-color: #0a0a0a !important;">

                        <!-- Title -->
                        <h3 class="title">
                            {{$post->title}}

                        </h3>

                        <!-- Meta -->
                        <div class="meta fix">
                            @if($post->subcategory_name == null)
                                <a href="#" class="meta-item category fashion">
                                    {{$post->category_name}}
                                </a>
                            @else
                                <a href="#" class="meta-item category fashion">

                                    {{$post->category_name}} | {{$post->subcategory_name}}

                                </a>
                            @endif
                            <a href="#" class="meta-item author"><img src="img/post/post-author-1.jpg"
                                                                      alt="post author">{{$post->username}}</a>
                            <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$post->post_date}}  <i
                                    class="fa fa-eye">{{$post->view_count}}</i></span>

                            {{--                            <a href="#" class="meta-item comments"><i class="fa fa-comments"></i>(38)</a>--}}
                            {{--                            <span class="meta-item view"><i class="fa fa-eye"></i>(3483)</span>--}}
                        </div>

                    </div>
                </div><!-- Post Header Section End -->

            </div>
        </div>
    </div><!-- Page Banner Section End -->

    <!-- Post Section Start -->
    <div class="post-section section">
        <div class="container">

            <!-- Feature Post Row Start -->
            <div class="row">

                <div class="col-lg-8 col-12 mb-50">

                    <!-- Post Block Wrapper Start -->
                    <div class="post-block-wrapper mb-50">

                        <!-- Post Block Body Start -->
                        <div class="body">
                            <div class="row">

                                <div class="col-12">

                                    <!-- Single Post Start -->
                                    <div class="single-post">
                                        <div class="post-wrap">

                                            <!-- Content -->
                                            <div class="content">
                                         POSTUN TITLEI :  {{$post->title}}
                                                <!-- Description -->
                                                <p class="dropcap">
                                            DESCRIPTIONU :        {{strip_tags($post->description)}}
                                                </p>


                                            </div>

                                            <div class="tags-social float-left">

                                                <div class="tags float-left">

                                                    <i class="fa fa-tags"></i>
                                                    TAGLERI
                                                    @foreach($tags as $tag)
                                                        {{  $tag->name}}
                                                    @endforeach

                                                </div>

                                                <div class="post-social float-right">
                                                    <a href="{{$socials->facebook}}" class="facebook"><i
                                                            class="fa fa-facebook"></i></a>
                                                    <a href="{{$socials->twitter}}" class="twitter"><i
                                                            class="fa fa-twitter"></i></a>
                                                    <a href="{{$socials->instagram}}"><i
                                                            class="fa fa-instagram"></i></a>
                                                    <a href="{{$socials->linkedin}}" class="google-plus"><i
                                                            class="fa fa-linkedin"></i></a>
                                                </div>

                                            </div>

                                        </div>
                                    </div><!-- Single Post End -->

                                </div>

                            </div>
                        </div><!-- Post Block Body End -->

                    </div><!-- Post Block Wrapper End -->

                    <!-- Previous & Next Post Start -->
                    <div class="post-nav mb-50">
                        <a
                            @if($prev !=null)
                            href="{{URL::to('/view/post/'.$prev->id)}}"

                            @endif
                            class="prev-post"><span>previous post</span>
                            @if($prev !=null)
                                {{$prev->title}}
                            @else
                                {{$post->title}}
                            @endif
                        </a>
                        <a
                            @if($next !=null)
                            href="{{URL::to('/view/post/'.$next->id)}}"

                            @endif

                            class="next-post"><span>next post</span>
                            @if($next !=null)
                                {{$next->title}}
                            @else
                                {{$post->title}}

                            @endif

                        </a>
                    </div><!-- Previous & Next Post End -->

                    <!-- Post Author Start -->

                    <!-- Post Block Wrapper Start -->
                    <!-- Post Block Wrapper Start -->
                    {{--                    <div class="post-block-wrapper mb-50">--}}

                    {{--                        <!-- Post Block Head Start -->--}}
                    {{--                        <div class="head">--}}

                    {{--                            <!-- Title -->--}}
                    {{--                            <h4 class="title">--}}

                    {{--                                @if(session()->get('lang')=='eng')--}}
                    {{--                                    You might also like!--}}
                    {{--                                @else--}}
                    {{--                                    Seve Bilecekleriniz!--}}
                    {{--                                @endif--}}

                    {{--                            </h4>--}}

                    {{--                        </div><!-- Post Block Head End -->--}}

                    {{--                        <!-- Post Block Body Start -->--}}
                    {{--                        <div class="body">--}}

                    {{--                            <div class="two-column-post-carousel column-post-carousel post-block-carousel row">--}}
                    {{--                                @foreach($more as $row)--}}
                    {{--                                    @if($row->id != $post->id)--}}
                    {{--                                        <div class="col-md-6 col-12">--}}

                    {{--                                            <!-- Overlay Post Start -->--}}
                    {{--                                            <div class="post post-overlay hero-post">--}}

                    {{--                                                <div class="post-wrap">--}}

                    {{--                                                    <!-- Image -->--}}
                    {{--                                                    <a class="image" href="{{URL::to('/view/post/'.$row->id)}}"><img--}}
                    {{--                                                            src="{{asset($row->image)}}"--}}
                    {{--                                                            alt="post"></a>--}}

                    {{--                                                    <!-- Category -->--}}
                    {{--                                                    <a href="#" class="category education">--}}
                    {{--                                                        {{$row->category_name}}--}}

                    {{--                                                    </a>--}}

                    {{--                                                    <!-- Content -->--}}
                    {{--                                                    <div class="content">--}}

                    {{--                                                        <!-- Title -->--}}
                    {{--                                                        <h4 class="title"><a href="{{URL::to('/view/post/'.$row->id)}}">--}}
                    {{--                                                            {{$row->title}}--}}
                    {{--                                                        </h4>--}}

                    {{--                                                        <!-- Meta -->--}}
                    {{--                                                        <div class="meta fix">--}}
                    {{--                                                            <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$row->post_date}}</span>--}}
                    {{--                                                        </div>--}}

                    {{--                                                    </div>--}}

                    {{--                                                </div>--}}
                    {{--                                            </div><!-- Overlay Post End -->--}}

                    {{--                                        </div>--}}

                    {{--                                @endif--}}
                    {{--                                @endforeach--}}
                    {{--                            </div>--}}

                    {{--                        </div><!-- Post Block Body End -->--}}


                    {{--                    </div><!-- Post Block Wrapper End -->--}}

                </div>

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
                <link rel="stylesheet"
                      href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
                <link rel="stylesheet"
                      href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css"/>
                <script>
                    jQuery(document).ready(function ($) {
                        $('.owl-carousel').owlCarousel({
                            loop: true,
                            margin: 10,
                            nav: true,
                            responsive: {
                                0: {
                                    items: 1
                                },
                                600: {
                                    items: 3
                                },
                                1000: {
                                    items: 5
                                }
                            }
                        })
                    })
                </script>
            </div>
            <div class="owl-carousel owl-theme mt-5">

                @foreach($more as $item)
                    <div class="item">
                        <h4>
                            <a href="#" class="category-fashion">
                                {{$item->title}}
                            </a>
                        </h4>
                        <h4>
                            <a class="image" href="{{URL::to('/view/post/'.$item->id)}}"><img
                                    src="{{asset($item->image)}}"
                                    alt="post"></a>
                        </h4>
                        <div class="meta fix">
                            <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$item->post_date}}</span>
                        </div>
                    </div>
                @endforeach

            </div>

            <br><br>
            <!-- Post Block Wrapper Start -->
            <div class="post-block-wrapper">

                <!-- Post Block Head Start -->
                <div class="head">

                    <!-- Title -->
                    <h4 class="title">Leave a Comment</h4>

                </div><!-- Post Block Head End -->

                <!-- Post Block Body Start -->
                <div class="body">

                    <div class="post-comment-form">
                        <form action="#" class="row">

                            <div class="col-md-6 col-12 mb-20">
                                <label for="name">Name <sup>*</sup></label>
                                <input type="text" id="name">
                            </div>

                            <div class="col-md-6 col-12 mb-20">
                                <label for="email">Email <sup>*</sup></label>
                                <input type="text" id="email">
                            </div>

                            <div class="col-12 mb-20">
                                <label for="website">Website <sup>*</sup></label>
                                <input type="text" id="website">
                            </div>

                            <div class="col-12 mb-20">
                                <label for="message">Message <sup>*</sup></label>
                                <textarea id="message"></textarea>
                            </div>

                            <div class="col-12">
                                <input type="submit" value="Submit Comment">
                            </div>

                        </form>
                    </div>

                </div><!-- Post Block Body End -->

            </div><!-- Post Block Wrapper End -->

        </div>


    </div><!-- Feature Post Row End -->




@endsection

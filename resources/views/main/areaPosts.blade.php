@extends('main.home_master')
@section('content')
    <!-- Menu Section Start -->


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="{{asset('frontend/assets//js/jquery.jscroll.min.js')}}"></script>

    <!-- Post Section Start -->
    <section class="categories-news">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="categories-news__title">{{$area_name->area_name}}</h2>
                </div>

                @foreach($areaPosts as $cp)

                    <div class="col-sm-3 col-6">
                        <div class="news-block__medium news-block__medium_media news-block__medium_region">
                            <a href=""><img src="{{url($cp->image)}}" alt="" class="news-block__image img-fluid"></a>
                            <div class="news-block__info-wrapper">
                                <div class="news-block__info">
                                    <a href="" class="news-block__info-link">{{$cp->title}}</a>
                                    <span
                                        class="news-block__date">{{\App\Models\Post::DateTranslate($cp->post_date)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <script type="text/javascript">
            $('ul.pagination').hide();
            $(function() {
                $('.infinite-scroll').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<img class="center-block" src="/images/loading.gif" alt="Loading..." />',
                    padding: 0,
                    nextSelector: '.pagination li.active + li a',
                    contentSelector: 'div.infinite-scroll',
                    callback: function() {
                        $('ul.pagination').remove();
                    }
                });
            });
        </script>
    </section>
@endsection

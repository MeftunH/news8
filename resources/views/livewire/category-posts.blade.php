@extends('main.home_master')
@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>

    <section class="categories-news">
        <div  class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="categories-news__title">{{$cat_name->category_name}}</h2>
                </div>

                <div class="col-12">
                    <div class="infinite-scroll">
                        <ul class="region-news__button-list">

                            @foreach($sub_cat as $sub)
                                <li><a href="{{route('front.subCatPost', [$sub->id])}}"
                                       class="region-news__button">{{$sub->subcategory_name}}</a></li>
                            @endforeach

                        </ul>
                    </div>
                </div>

                @foreach($catPosts as $cp)

                    <tr @if ($loop->last) id="last_record" @endif>
                    <div class="col-sm-3 col-6">
                        <div class="news-block__medium news-block__medium_media news-block__medium_region">
                            <a href=""><img src="{{url($cp->image)}}" alt="" class="news-block__image img-fluid"></a>
                            <div class="news-block__info-wrapper">
                                <div class="news-block__info">
                                    <a href="" class="news-block__info-link">{{$cp->title}}</a>
                                    <span
                                        class="news-block__date">{{\App\Models\Post::DateTranslate($cp->post_date)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @if((new App\Http\Livewire\CategoryPosts)->loadAmountFunc() >= $totalRecords)
            <p class="text-center">No More News</p>
            @endif
    </section>

    <script>
        const lastRecord = document.getElementById('last_record');
        const options = {
            root: null,
            threshold: 1,
            rootMargin: '0px'
        }
        const observer = new IntersectionObserver((entries, observer) => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                @this.loadMore()
                }
            });
        });
        observer.observe(lastRecord);
    </script>

@endsection

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSegmentCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('segment_categories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('segment_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();
            $table->boolean('is_main_category');
            $table->timestamps();
            $table->foreign('segment_id')->references('id')->on('segments')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('segment_categories');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id');
            $table->integer('area_id')->nullable();
            $table->integer('subcategory_id')->nullable()->default(0);
            $table->integer('writer_id');
            $table->string('image');
            $table->integer('is_active')->nullable();
            $table->boolean('is_video_content');
            $table->integer('is_photo_content')->nullable();
            $table->integer('is_sub_heading')->nullable();
            $table->integer('slider_little_posts')->nullable();
            $table->integer('view_count')->default(0);
            $table->integer('is_big_thumbnail')->nullable();
            $table->integer('is_most_important')->nullable();
            $table->integer('heading')->default(0);
            $table->integer('unique_big_post')->nullable();
            $table->boolean('is_audio_content')->default(0);
            $table->string('post_date');
            $table->string('month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

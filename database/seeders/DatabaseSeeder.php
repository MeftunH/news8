<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('socials')->insert([
//            'id'=>1,
//            'facebook' => "",
//            'instagram' => "",
//            'youtube' => "",
//            'linkedin' => "",
//            'pinterest' => "",
//            ]);
//        DB::table('seo')->insert([
//            'id'=>1,
//            'meta_author' => "",
//            'meta_title' => "",
//            'meta_keyword' => "",
//            'meta_description' => "",
//            'google_analytics' => "",
//            'google_verification' => "",
//            'alexa_analytics' => "",
//
//        ]);
//        DB::table('livetv')->insert([
//            'id'=>1,
//            'embed_code' => "",
//            'status' => 0,
//
//        ]);
//        DB::table('notices')->insert([
//            'id'=>1,
//            'notice' => "",
//            'status' => 0,
//
//        ]);
//        DB::table('language')->insert([
//            'code'=>'en',
//            'name' => 'English',
//            'local' => 'en',
//            'flag' => '',
//            'status' => 1,
//            'is_default'=>1,
//        ]);
//        $admin = User::factory()->create(
//            [
//          'name'=>'admin',
//          'email'=>'admin@gmail.com',
//            ]
//        );
//        $editor = User::factory()->create(
//            [
//                'name'=>'editor',
//                'email'=>'editor@gmail.com',
//            ]
//        );
//        $writer = User::factory()->create(
//            [
//                'name'=>'writer',
//                'email'=>'writer@gmail.com',
//            ]
//        );
//        Role::create(['name'=>'admin']);
//        Role::create(['name'=>'editor']);
//        Role::create(['name'=>'writer']);
//
//        $permissions =[
//            'create',
//            'read',
//            'update',
//            'delete',
//        ];
//
//        foreach (Role::all() as $item) {
//            foreach ($permissions as $permission) {
//                Permission::create(['name'=>"{$item->name} $permission"]);
//            }
//        }
//        $admin->syncPermission(Permission::all());
//        $editor->syncPermission(Permission::where('name','like',"%editor%"));
//        $writer->syncPermission(Permission::where('name','like',"%writer%"));
//
//        $admin->assignRole('admin');
//        $editor->assignRole('editor');
//        $writer->assignRole('writer');
    }
}

<?php

namespace App\Providers;

use AmrShawky\Currency\Currency;
use App\View\Composers\SinglePostComposer;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(LocalizationServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

//        setlocale(LC_TIME, 'az_AZ', 'az', 'AZ', 'Azerbaijani', 'az_AZ.UTF-8');
//        Carbon::setLocale('en'); // This is only needed to use ->diffForHumans()
        Paginator::useBootstrap();
        $languages = \App\Models\Language::orderBy('name', 'asc')->get();
        //dd(App::getLocale());
        $settings= DB::table('settings')->first();
        View::share('languages', $languages);
        $cur_lang = App::getLocale();
        View::share('cur_lang', $cur_lang);
        $popular = DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())
            ->whereDate('posts.created_at', '>', Carbon::now()->subDay())->orderBy('view_count', 'desc')->orderBy('view_count', 'desc')->limit(3)->get();
        $top = DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())
            ->whereDate('posts.created_at', '>', Carbon::now()->subMonth())->limit(3)->get();
        $websites = \Illuminate\Support\Facades\DB::table('websites')->get();

        $notices = \Illuminate\Support\Facades\DB::table('notices')->first();
//        View::share('category',$category);
//        View::share('catmore',$catmore);
//        View::share('socials',$socials);
        $socials_head = DB::table('socials')->first();

        View::share('top', $top);
        View::share('settings', $settings);
        View::share('notices', $notices);
        View::share('popular', $popular);
        View::share('websites', $websites);
        View::share('socials_head', $socials_head);

        //dd(App::getLocale());
        View::composer('main.single_post', SinglePostComposer::class);
    }
}

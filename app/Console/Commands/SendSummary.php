<?php

namespace App\Console\Commands;

use App\Mail\WeeklySummary;
use App\Models\Language;
use App\Models\Post;
use App\Models\Subscriber;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:summary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send weekly email summary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subscribers = Subscriber::active()->get();
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $post = DB::table('posts')
            ->leftJoin('post_translations', 'posts.id', 'post_translations.post_id')
            ->where('post_translations.language_id', $defaultLanguageId)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->whereDate('posts.created_at', '>', Carbon::now()->subMonth())
            ->select('posts.*', 'category_translations.category_name', 'post_translations.title')->get();

        foreach ($subscribers as $sub) {
            Mail::to($sub)->send(new WeeklySummary($sub,$post));
        }
    }
}

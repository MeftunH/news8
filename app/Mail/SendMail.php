<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $config  = config('mail.from');


        $config=config('mail.from');
        return $this->from($this->data['email'])
            ->to($config['address'], $config['name'] )
            ->subject($this->data['subject'])->view('emails.contactform')->with('data', $this->data);
    }
}

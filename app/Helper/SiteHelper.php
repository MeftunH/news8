<?php
namespace App\Helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class SiteHelper
{

    private $lan_ids;

    public function __construct($lan_ids)
    {
    $this->lan_ids = $lan_ids;
    }

    public function lan_ids()
    {
        return 'lan_ids';

    }
    public function if_not_main_posts_function($category_id)
    {
        $lan = DB::table('language')->where('local', App::getLocale())->first();
        $if_not_main_posts= DB::table('posts')
            ->where('posts.category_id',$category_id)
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'categories.id as cat_id','post_translations.title', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('posts.id', 'desc')->limit(3)->get();
        return($if_not_main_posts);
    }
    public static function LanguageOption()
    {
        $path = base_path().'/resources/lang/';
        $langs =scandir($path);
        $t=array();
        foreach ($langs as $value)
        {
            if ($value === '.' || $value === '..')
            {
                continue;
            }
            if (is_dir($path.$value))
            {
                $fp=file_get_contents($path.$value. '/info.json');
            $fp = json_decode($fp,true);
            $t[] =$fp;

            }
        }
        return $t;
    }

}

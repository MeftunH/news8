<?php


namespace App\View\Composers;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SinglePostComposer
{
    public function compose(View $view)
    {
        $view->with('socials', DB::table('socials')->first());
    }
}

<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NullableField implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $otherField;
   private $message;
    public function __construct( $otherField,$message = '')
    {
        $this->message = $message;
        $this->otherField = $otherField;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($this->otherField === null)
        {
            return $value !== null;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}

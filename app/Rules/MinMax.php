<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MinMax implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $message;
    public function __construct($message = '')
    {
        $this->message = $message;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
    }
    public function message()
    {
        return $this->message;
    }
    /**
     * Get the validation error message.
     *
     * @return string
     */
}

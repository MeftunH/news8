<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    use HasFactory;
    protected $table='subscribers';
    protected $guarded=[];
    public function scopeActive($query)
    {
        return $query->where('is_still_subscriber', 1);
    }
}

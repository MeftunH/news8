<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    protected $table='category_translations';
    use HasFactory;
//    public function rules()
//    {
//        return [
//            'category_name' => 'required|unique:category_translations|max:255',
//        ];
//    }
}

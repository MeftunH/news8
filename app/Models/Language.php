<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    protected $table = 'language';
    protected $guarded = [];


    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public static function getAllLanguageCodes()
    {
        $languages = static::all();
        $codes = [];
        foreach ($languages as $language) {
            $codes[] = $language->code;

        }
        return $codes;

    }
    public static function getDefaultLanguage()
    {
        return static::where('is_default',1)->first();
    }

    public function tag_translations()
    {
        return $this->belongsToMany(TagTranslation::class, 'tag_translations', 'language_id', 'tag_id');
    }
}

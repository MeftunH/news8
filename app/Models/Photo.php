<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;
    protected $guarded=[];
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function setFilenamesAttribute($value)
    {
        $this->attributes['photo_link'] = json_encode($value);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $table = 'tags';
    protected $guarded = [];

//    public function posts()
//    {
//        return $this->belongsToMany(Post::class);
//    }
    public function languages()
    {
        return $this->belongsToMany(Language::class);
    }

//    public function InsertOrUpdate($tag_names = [],$language_id,$post)
//    {
//        $tag_ids = [];
//        foreach ($tag_names as $key=>$item) {
//            if ($item !=null) {
//                $count=TagTranslation::where('language_id',$language_id)->where('post_id',$post->id)->count();
//                if ($count!= 0)
//                {
//                    $tag = Tag::where()delete();
//                    TagTranslation::where('language_id',$language_id)->where('post_id',$post->id)->delete()
//                }
//                $tag = Tag::create(['post_id' => $post->id]);
//                $tag_ids[] = $tag->id;
//                TagTranslation::create(['name' => $item, 'language_id' => $language_id, 'tag_id' => $tag->id]);
//            }
//        }
//    }

}

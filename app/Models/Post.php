<?php

namespace App\Models;

use App\Models\Models\PostTag;
use Carbon\Carbon;
use Conner\Tagging\Model\Tag;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    use HasFactory;
    protected $table='posts';
    protected $guarded = [];



    public function tag()
    {
        return $this->belongsToMany(Tag::class);
    }



    public function scopeBigThumbnailActive($query)
    {
        return $query->where('is_big_thumbnail',1);
    }
    public function scopeSameCat($query,$cat_id)
    {
        return $query->where('posts.category_id',$cat_id);
    }
    public function scopeBigThumbnailPassive($query)
    {
        return $query->where('is_big_thumbnail',null);
    }


    public static function DateTranslate($date)
    {
        $translatedDate=Carbon::parse($date)->translatedFormat('d F Y - H:i');
        return $translatedDate;
    }
    public function postTranslations()
    {
        return $this->hasMany(PostTranslation::class);
    }
    public function post_tags(){
        return $this->belongsToMany(self::class,'posts_tags','post_id','tag_id');
    }
}

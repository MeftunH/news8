<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Search extends Component
{
    use WithPagination;
    public $searchTerm;
    public function render()
    {
        $searchTerm = '%'.$this->searchTerm.'%';
        $posts = DB::table('posts')
        ->join('categories', 'posts.category_id', 'categories.id')
        ->leftjoin('subcategories', 'posts.subcategory_id', 'subcategories.id')
        ->join('users', 'posts.writer_id', 'users.id')
        ->select('posts.*', 'categories.category_en', 'categories.category_az', 'subcategories.subcategory_en', 'subcategories.subcategory_az', 'users.name')
        ->where('posts.title_en','like', $searchTerm)->paginate(10);
        return view('livewire.search',compact('searchTerm','posts'));
    }
}

<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class CategoryPosts extends Component
{
    protected int $loadAmount = 8;

    public function loadMore()
    {
        $this->loadAmount += 8;
    }
    public function loadAmountFunc() {
        return $this->loadAmount;
    }

    protected $listeners = [
        'load-more' => 'loadMore',
    ];

    public function render($id)
    {
        $lan = DB::table('language')->where('local', App::getLocale())->first();
        $sub_cat = DB::table('subcategories')->where('subcategories.category_id', $id)
            ->join('subcategory_translations', 'subcategories.id', 'subcategory_translations.subcategory_id')
            ->where('subcategory_translations.language_id', $lan->id)->select('subcategories.id as id', 'subcategory_translations.subcategory_name as subcategory_name')->get();

        $cat_name = DB::table('categories')
            ->where('categories.id', $id)
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->where('category_translations.language_id', $lan->id)
            ->first();
        $catPosts = DB::table('categories')
            ->where('categories.id', $id)
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->join('posts', 'posts.category_id', 'categories.id')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('users', 'posts.writer_id', 'users.id')
            ->where('post_translations.language_id', $lan->id)
            ->where('category_translations.language_id', $lan->id)
            ->orderBy('posts.id', 'desc')->limit($this->loadAmount)->get();
        $totalRecords = count($catPosts);
        return view('livewire.category-posts',compact('sub_cat','cat_name','catPosts','totalRecords'));
    }

}

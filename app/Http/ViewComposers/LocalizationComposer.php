<?php

namespace App\Http\ViewComposers;

use App\Models\Language;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use LaravelLocalization;

class LocalizationComposer {



    public function compose(View $view)
    {

        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();

        $category_head = DB::table('category_translations')
            ->leftjoin('language','language.id','category_translations.language_id')
            ->join('categories','categories.id','category_translations.category_id')
            ->where('language.local',App::currentLocale())->orderBy('category_id','ASC')
            ->select('categories.id as id','category_translations.category_name')->where('categories.status',1)->get();
        $catmore_head= DB::table('category_translations'
        )->leftjoin('language','language.id','category_translations.language_id')
            ->join('categories','categories.id','category_translations.category_id')
            ->where('language.local',App::currentLocale())->orderBy('category_id','ASC')
            ->select('categories.id as id','category_translations.category_name')->skip(6)->take(PHP_INT_MAX)->get();
        $headline = \Illuminate\Support\Facades\DB::table('posts')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->select('posts.*','post_translations.title','post_translations.description')
            ->leftjoin('language','language.id','post_translations.language_id')
            ->where('language.local',\Illuminate\Support\Facades\App::currentLocale())
            ->where('posts.is_photo_content',1)->get();
        $view->with('currentLocale', LaravelLocalization::getCurrentLocale());
        $view->with('category_head',$category_head);
        $view->with('catmore_head',$catmore_head);
        $view->with('lan',$lan);

        $view->with('headline',$headline);
        $view->with('altLocale', config('app.fallback_locale'));

    }

}

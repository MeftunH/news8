<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Segment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class SegmentController extends Controller
{

    public function AddSegment()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $category = DB::table('categories')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->select('categories.id', 'category_translations.category_name')
            ->where('language_id', $defaultLanguageId)
            ->get();


        return view('backend.segment.category.create', compact('category'));
    }

    public function StoreSegment(Request $request)
    {
        $data = array();
        $data['name'] = $request->name;
        $data['is_active'] = $request->is_active;


        $validateData = $request->validate([
            'name' => 'required|unique:segments',
            'category_id' => 'required',
        ]);
        $cat_counter=0;
        $main_counter=0;
        foreach ($request->input('category_id') as $key=>$value) {
            if (($value) !=0)
                $cat_counter++;
            }
        foreach ($request->input('is_main_category') as $key=>$value) {
            if (($value) !=0)
                $main_counter++;
        }
        if ($main_counter!=1)
        {
            throw ValidationException::withMessages(['Each section has only 1 main content']);

        }
        if ($cat_counter!=3)
        {
            throw ValidationException::withMessages(['Each section has only 3 category content']);

        }


        if (isset($request->category_id) && isset($request->is_main_category)) {

            $segment_category_data=[];
            $segment=Segment::create($data);
            foreach ($request->input('category_id') as $key=>$value) {

                $segment_category_data['category_id'] = $request->input("category_id.$key");
                $segment_category_data['segment_id'] = $segment->id;
                $segment_category_data['is_main_category'] = $request->input("is_main_category.$key");
                if ($segment_category_data['category_id']!=0)

                $segment_categories=DB::table('segment_categories')->insert($segment_category_data);
            }

        }
    return redirect('/category-segments');
    }

    public function EditSegment($id)
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $categories = DB::table('categories')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->select('categories.id', 'category_translations.category_name')
            ->where('language_id', $defaultLanguageId)
            ->get();
        $seg_cat=DB::table('segment_categories')->where('segment_id',$id)->get();
        $cat_array=[];
        $main=0;
        foreach ($seg_cat as $item) {
            foreach ($categories as $category) {
                if ($category->id == $item->category_id)
                    {
                        $cat_array[]=$item->category_id;
                    }
            }
        }

        $seg=DB::table('segments')
            ->where('segments.is_active',1)
            ->where('id',$id)
            ->orderBy('segments.id','desc')->distinct()->first();


        foreach ($seg_cat as $itm) {
            if ($itm->segment_id==$seg->id)
            {
                if ($itm->is_main_category == 1)
                {
                    $main=$itm->category_id;
                }

            }
        }

        return view('backend.segment.category.edit', compact('categories','seg_cat','seg','cat_array','main'));

    }

    public function DeleteSegment($id)
    {
        DB::table('segments')->where('id', $id)->delete();
        return redirect('backend.segment.category.index');
    }

    public function UpdateSegment(Request $request,$id)
    {

    }

    public function Index()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $categories = DB::table('categories')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->select('categories.id', 'category_translations.category_name')
            ->where('language_id', $defaultLanguageId)
            ->get();
        $seg_cat=DB::table('segment_categories')->get();
        $seg=DB::table('segments')
            ->orderBy('segments.id','desc')->distinct()->get();
         return view('backend.segment.category.index',compact('seg','categories','seg_cat'));
    }
}

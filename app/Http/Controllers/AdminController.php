<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Language;
use App\Models\Post;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function DashBoard()
    {
        $post_count=Post::all()->count();
        $category_count=Category::all()->count();
        $subcategory_count=SubCategory::all()->count();
        $language_count=Language::all()->count();

        return view('admin.index')->with(compact('post_count','category_count','subcategory_count','language_count'));
    }
    public function index()
    {
         $languages= Language::orderBy('name','asc')->get();
        return view('admin.body.sidebar',compact('languages'));
    }
public function GetLang()
{
    $languages= Language::orderBy('name','asc')->get();
    return view('admin.body.header',compact('languages'));
}
    public function Logout()
    {
    Auth::logout();
    return Redirect()->route('login')->with('success','Logouted');
    }
}

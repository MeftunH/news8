<?php

namespace App\Http\Controllers;

use App\Models\AboutUsTranslation;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class AboutUsController extends Controller
{
    public function Index()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;

        $aboutUs=DB::table('about_us')->join('about_us_translations','about_us.id','about_us_translations.about_us_id')
            ->where('about_us_translations.language_id',$defaultLanguageId)
            ->get();
        return view('backend.aboutUs.index',compact('aboutUs'));
    }
    public function EditAboutUs($id)
    {
        $about_us = DB::table('about_us')->where('id', $id)->first();
        $translations = DB::table('about_us')
            ->join('about_us_translations', 'about_us.id', 'about_us_translations.about_us_id')
            ->where('about_us.id', $id)
            ->get();
//        dd($translations);

        return view('backend.aboutUs.edit', compact('translations','about_us'));
    }

    public function UpdateAboutUs(Request $request,$id)
    {
        $data = array();
        $notification = array();
        $data['is_active'] = $request->is_active ? 1 : 0;
        foreach ($request->about_us_translation as $language => $about_us_translation) {

            $translation = AboutUsTranslation::where('about_us_id',$id)->where('language_id',$language)->first();
            $translation->about_us_translation = $about_us_translation;
            if ($translation->isDirty()) {
                $validate_array = ['about_us_translation' => 'required|min:1|max:255'.$id];
                $this->validate($request, $validate_array );
                $translation->save();
                $notification = array(
                    'message' => 'About us Edited successfully',
                    'alert-type' => 'success'
                );
            }

        }
        DB::table('about_us')->where('id',$id)->update($data);

        return Redirect()->route('AboutUs')->with($notification);
    }
}

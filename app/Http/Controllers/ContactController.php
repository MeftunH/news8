<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
class ContactController extends Controller
{
    public function contactForm()
    {
        return view('contact');
    }

    public function storeContactForm(Request $request)
    {

            $this->validate($request, [
                'email'  =>  'required|email',
                'subject'  =>  'required',
                'message' =>  'required'
            ]);


        Mail::send(new SendMail($request->all()));

        return redirect()->back()->with(['success' => 'Contact Form Submit Successfully']);
    }
}

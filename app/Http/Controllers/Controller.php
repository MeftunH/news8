<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;

class
Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        if (defined('CORE_MULTI_LANG') && CORE_MULTI_LANG==1)
        {
            $lang = (session()->get('lang') != "" ? session()->get('lang') : CORE_LANG);
            App::setLocale($lang);
        }
    }


}

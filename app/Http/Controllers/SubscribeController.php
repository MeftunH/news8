<?php

namespace App\Http\Controllers;

use App\Actions\Subscribers\GetAllSubscribersAction;
use App\Actions\Subscribers\StoreSubscriberAction;
use App\Http\Requests\SubscribeRequest;
use App\Models\Subscriber;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    public function index(GetAllSubscribersAction $getAllSubscribersAction)
    {
        return view('backend.subscriber.index')->with([
            "subscribers"=>$getAllSubscribersAction->run(),
            ]);
    }
    public function subscribe(SubscribeRequest $request)
    {
        $subscriber = new Subscriber([
            'email' => $request->email
        ]);
        $subscriber->save();
    }
    public function store(SubscribeRequest $request, StoreSubscriberAction $storeSubscriberAction)
    {
        $storeSubscriberAction->run($request->only(['email']));

        return redirect()->back();
    }

}

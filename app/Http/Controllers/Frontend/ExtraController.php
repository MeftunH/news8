<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Language;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ExtraController extends Controller
{

    public function SinglePost($id)
    {

        $lan = DB::table('language')->where('local', App::getLocale())->first();

        $post = DB::table('posts')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->leftjoin('subcategories', 'posts.subcategory_id', 'subcategories.id')
            ->leftjoin('subcategory_translations', 'posts.subcategory_id', 'subcategory_translations.subcategory_id')
            ->join('users', 'posts.writer_id', 'users.id')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('language', 'post_translations.language_id', '=', 'language.id')
            ->where('posts.id', $id)
            ->where('post_translations.language_id', $lan->id)
            ->select('posts.*', 'post_translations.title', 'post_translations.description', 'category_translations.category_name', 'subcategory_translations.subcategory_name', 'users.name as username', 'language.name')
            ->where('language.id', $lan->id)
            ->first();

        $tags = DB::table('tags')
            ->join('tag_translations', 'tag_translations.tag_id', '=', 'tags.id')
            ->leftjoin('language', 'tag_translations.language_id', '=', 'language.id')
            ->where('post_id', $post->id)
            ->where('language.id', $lan->id)
            ->select('tag_translations.name')->get();
        $more = DB::table('posts')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->select('posts.*', 'category_translations.category_name', 'post_translations.title')
            ->where('category_translations.category_id', $post->category_id)
            ->where('post_translations.language_id', $lan->id)
            ->where('category_translations.language_id', $lan->id)
            ->orderBy('id', 'desc')->get();

        $count = DB::table('posts')->where('id', $post->id)->increment('view_count', 1);

        $prev = DB::table('posts')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('posts.id', '<', $post->id)
            ->where('posts.category_id', $post->category_id)
            ->where('post_translations.language_id', $lan->id)->select('posts.id as id', 'post_translations.title')->orderBy('post_date', 'desc')->first();

        $next = DB::table('posts')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('posts.id', '>', $post->id)
            ->where('post_translations.language_id', $lan->id)
            ->where('posts.category_id', $post->category_id)->select('posts.id as id', 'post_translations.title')->first();
        return view('main.single_post', compact('prev', 'next', 'post', 'tags', 'more', 'count'));
    }

    public function search(Request $request)
    {
        // Get the search value from the request
        $search = $request->input('search');
        $lan = DB::table('language')->where('local', App::getLocale())->first();

        // Search in the title and body columns from the posts table
        $posts = DB::table('posts')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('users', 'posts.writer_id', 'users.id')
            ->select('posts.*', 'post_translations.title', 'post_translations.description', 'users.name')
            ->where('post_translations.language_id', $lan->id)
            ->where('title', 'LIKE', "%{$search}%")
            ->get();

        // Return the search view with the resluts compacted
        return view('main.search', compact('posts'));
    }

    public function contact(Request $request)
    {
        return view('main.contact');
    }


    public function CatPost($id, Request $request)
    {

        $lan = DB::table('language')->where('local', App::getLocale())->first();

        $catposts = DB::table('categories')
            ->where('categories.id', $id)
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->join('posts', 'posts.category_id', 'categories.id')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('users', 'posts.writer_id', 'users.id')
            ->where('post_translations.language_id', $lan->id)
            ->where('category_translations.language_id', $lan->id)
            ->orderBy('posts.id', 'desc')
            ->get();

        $sub_cat = DB::table('subcategories')->where('subcategories.category_id', $id)
            ->join('subcategory_translations', 'subcategories.id', 'subcategory_translations.subcategory_id')
            ->where('subcategory_translations.language_id', $lan->id)->select('subcategories.id as id', 'subcategory_translations.subcategory_name as subcategory_name')->get();

        $cat_name = DB::table('categories')
            ->where('categories.id', $id)
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->where('category_translations.language_id', $lan->id)
            ->first();

        $top = DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')->where('posts.category_id', $id)
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())
            ->whereDate('posts.created_at', '>', Carbon::now()->subMonth())->limit(3)->get();
        if ($request->ajax()) {
            $view = view('main.allpost',compact('catposts'))->render();
            return response()->json(['html'=>$view]);
        }
        return view('main.allpost', compact('sub_cat', 'catposts', 'cat_name', 'top'));
    }
    public function AreaPost($id, Request $request)
    {

        $lan = DB::table('language')->where('local', App::getLocale())->first();

        $areaPosts = DB::table('areas')
            ->where('areas.id', $id)
            ->join('area_translations', 'area_translations.area_id', 'areas.id')
            ->join('posts', 'posts.area_id', 'areas.id')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('users', 'posts.writer_id', 'users.id')
            ->where('post_translations.language_id', $lan->id)
            ->where('area_translations.language_id', $lan->id)
            ->orderBy('posts.id', 'desc')
            ->get();


        $area_name = DB::table('areas')
            ->where('areas.id', $id)
            ->join('area_translations', 'area_translations.area_id', 'areas.id')
            ->where('area_translations.language_id', $lan->id)
            ->first();


        return view('main.areaPosts', compact('areaPosts', 'area_name'));
    }

    public function videoNews()
    {
        $lan = DB::table('language')->where('local', App::getLocale())->first();

        $multimedia = DB::table('posts')->where('posts.is_video_content', 1)
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('post_translations.language_id', $lan->id)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'post_translations.title', 'post_translations.description', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('post_date', 'desc')->get();
        return  view('main.multimedia', compact('multimedia'));
    }

    public function AboutUs()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;

        $aboutUs=DB::table('about_us')->join('about_us_translations','about_us.id','about_us_translations.about_us_id')
            ->where('about_us_translations.language_id',$defaultLanguageId)
            ->where('is_active',1)
            ->first();
//        dd($aboutUs);
        return view('main.aboutUs',compact('aboutUs'));

    }

    public function PhotoNews()
    {
        $lan = DB::table('language')->where('local', App::getLocale())->first();

        $multimedia = DB::table('posts')->where('posts.is_photo_content', 1)
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('post_translations.language_id', $lan->id)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'post_translations.title', 'post_translations.description', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('post_date', 'desc')->get();
        return  view('main.multimedia', compact('multimedia'));
    }
    public function AreaNews()
    {
        $lan = DB::table('language')->where('local', App::getLocale())->first();

        $areaPosts = DB::table('posts')->where('posts.area_id', "!=",null)
            ->join('areas','posts.area_id','areas.id')
            ->join('area_translations', 'area_translations.area_id', 'areas.id')
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('post_translations.language_id', $lan->id)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('area_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'area_translations.area_name','areas.id as area_id','post_translations.title', 'post_translations.description', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('post_date', 'desc')->get();
        return  view('main.allAreaPosts', compact('areaPosts'));
    }
    public function SubCatPost($id)
    {
        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();
//        $subcatposts = DB::table('posts')
//            ->join('categories', 'posts.category_id', 'categories.id')
//            ->leftjoin('subcategories', 'posts.subcategory_id', 'subcategories.id')
//            ->join('users', 'posts.writer_id', 'users.id')
//            ->where('posts.subcategory_id', $id)
//            ->orderBy('posts.id', 'desc')
//            ->paginate(6);
        $subcatposts = DB::table('subcategories')
            ->where('subcategories.id', $id)
            ->join('subcategory_translations', 'subcategory_translations.subcategory_id', 'subcategories.id')
            ->join('posts', 'posts.subcategory_id', 'subcategories.id')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('users', 'posts.writer_id', 'users.id')
            ->where('post_translations.language_id', $lan->id)
            ->where('subcategory_translations.language_id', $lan->id)
            ->orderBy('posts.id', 'desc')
            ->get();

        $subcat_name = DB::table('subcategories')
            ->where('subcategories.id', $id)
            ->join('subcategory_translations', 'subcategory_translations.subcategory_id', 'subcategories.id')
            ->join('categories', 'categories.id', 'subcategories.category_id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->where('subcategory_translations.language_id', $lan->id)
            ->where('category_translations.language_id', $lan->id)
            ->first();

        $top_sub = DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')->where('posts.subcategory_id', $id)
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())
            ->whereDate('posts.created_at', '>', Carbon::now()->subMonth())->limit(3)->get();
        return view('main.subposts', compact('subcatposts', 'subcat_name', 'top_sub'));
    }

    public function LastNews()
    {
        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();
        $lastNews=DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('post_translations.language_id', $lan->id)->orderBy('posts.post_date','desc') ->where('posts.is_active', 1)->select('posts.id as id','post_translations.title','posts.image','posts.post_date','posts.is_video_content','posts.is_photo_content','posts.is_audio_content')->get();

        return  view('main.lastNews', compact('lastNews'));
    }

    public function LastNewsToday()
    {
        $id=DB::table('language')->where('local', App::getLocale())->select('id')->first();
//        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();

        $lastNews=DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('post_translations.language_id', $id->id)->orderBy('posts.post_date','desc')
            ->whereDate('posts.created_at', '=', Carbon::today())
            ->where('posts.is_active', 1)
            ->select('posts.id as id','post_translations.title','posts.image','posts.post_date','posts.is_video_content','posts.is_photo_content','posts.is_audio_content')->get();
        return  view('main.lastNews', compact('lastNews'));
    }

    public function LastNewsYesterday()
    {
        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();
        $lastNews=DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('post_translations.language_id', $lan->id)->orderBy('posts.post_date','desc')
            ->whereDate('posts.created_at', '=', Carbon::yesterday())
            ->where('posts.is_active', 1)
            ->select('posts.id as id','post_translations.title','posts.image','posts.post_date','posts.is_video_content','posts.is_photo_content','posts.is_audio_content')->get();
        return  view('main.lastNews', compact('lastNews'));
    }

    public function LastNewsLastMonth()
    {
        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();
        $lastNews=DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('post_translations.language_id', $lan->id)->orderBy('posts.post_date','desc')
            ->whereDate('posts.created_at', '>', Carbon::now()->subMonth())
            ->where('posts.is_active', 1)
            ->select('posts.id as id','post_translations.title','posts.image','posts.post_date','posts.is_video_content','posts.is_photo_content','posts.is_audio_content')->get();
        return  view('main.lastNews', compact('lastNews'));
    }
    public function LastNewsPrevWeek()
    {
        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();
        $lastNews=DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('post_translations.language_id', $lan->id)->orderBy('posts.post_date','desc')
            ->whereBetween('posts.created_at', [Carbon::now()->subWeek()->subWeek(),Carbon::now()->subWeek()])
            ->where('posts.is_active', 1)
            ->select('posts.id as id','post_translations.title','posts.image','posts.post_date','posts.is_video_content','posts.is_photo_content','posts.is_audio_content')->get();
        return  view('main.lastNews', compact('lastNews'));
    }
    public function LastNewsThisWeek()
    {
        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();
        $lastNews=DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('post_translations.language_id', $lan->id)->orderBy('posts.post_date','desc')
            ->whereBetween('posts.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->where('posts.is_active', 1)
            ->select('posts.id as id','post_translations.title','posts.image','posts.post_date','posts.is_video_content','posts.is_photo_content','posts.is_audio_content')->get();
        return  view('main.lastNews', compact('lastNews'));
    }

    public function LastNewsThisMonth()
    {
        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();

        $lastNews=DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('post_translations.language_id', $lan->id)->orderBy('posts.post_date','desc')
            ->whereMonth('posts.created_at', Carbon::today()->month)
            ->where('posts.is_active', 1)
            ->select('posts.id as id','post_translations.title','posts.image','posts.post_date','posts.is_video_content','posts.is_photo_content','posts.is_audio_content')->get();
        return  view('main.lastNews', compact('lastNews'));
    }

    public function MultiMedia()
    {
        $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();

       $multimedia= DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('post_translations.language_id', $lan->id)
           ->orderBy('posts.post_date','desc')
            ->where('posts.is_active', 1)
           ->where('posts.is_audio_content',1)
           ->orWhere(function ($query) {
               $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();
               $query->where('post_translations.language_id', $lan->id)->where('is_video_content', 1);
           })
           ->orWhere(function ($query) {
               $lan = \Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();
               $query->where('post_translations.language_id', $lan->id)->where('is_audio_content', 1);
           })
            ->select('posts.id as id','post_translations.title','posts.image','posts.post_date','posts.is_video_content','posts.is_photo_content','posts.is_audio_content')->get();
        return  view('main.multimedia', compact('multimedia'));
    }
    public function post($post)
    {
        $Key = 'post' . $post->id;
        if (Session::has($Key)) {

            DB::table('posts')
                ->where('id', $post->id)
                ->increment('view_count', 1);
            Session::put($Key, 1);
        }

    }
}

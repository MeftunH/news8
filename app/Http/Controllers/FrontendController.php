<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Segment;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{

    public function Home()
    {
        $lan = DB::table('language')->where('local', App::getLocale())->first();

        $most_important_news = DB::table('posts')->where('is_most_important', 1)
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('post_translations.language_id', $lan->id)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'post_translations.title', 'post_translations.description', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('post_date', 'desc')->get();

        $heading = DB::table('heading')
            ->where('heading.is_active', 1)
            ->join('posts', 'heading.post_id', 'posts.id')
            ->join('heading_translations', 'heading_translations.heading_id', 'heading.id')
            ->join('categories', 'heading.category_id', 'categories.id')
            ->join('category_translations', 'heading.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('heading_translations.language_id', $lan->id)
            ->select('heading.*', 'heading_translations.title', 'heading_translations.description', 'posts.view_count', 'category_translations.category_name')
            ->whereDate('heading.post_date', Carbon::today())
            ->first();


        $if_today_heading_not_found = DB::table('heading')
            ->where('heading.is_active', 1)
            ->join('posts', 'heading.post_id', 'posts.id')
            ->join('heading_translations', 'heading_translations.heading_id', 'heading.id')
            ->join('categories', 'heading.category_id', 'categories.id')
            ->join('category_translations', 'heading.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('heading_translations.language_id', $lan->id)
            ->select('heading.*', 'heading_translations.title', 'heading_translations.description', 'posts.view_count', 'category_translations.category_name')
            ->whereDate('heading.post_date', '>', Carbon::now()->subDay())
            ->first();

        $if_any_heading_content_not_found = DB::table('posts')
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('post_translations.language_id', $lan->id)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'post_translations.title', 'post_translations.description', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->whereDate('posts.post_date', '<', Carbon::now())
            ->orderBy('posts.post_date', 'desc')->first();

        $is_sub_heading = DB::table('posts')->where('is_sub_heading', 1)->where('heading', 0)
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'post_translations.title', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('posts.post_date', 'desc')->limit(8)->get();

        $audio_news = DB::table('posts')->where('is_audio_content', 1)
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->join('audio', 'audio.post_id', 'posts.id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'audio.url', 'audio.link', 'language.id as lang_id', 'post_translations.title', 'category_translations.category_name', 'category_translations.language_id as cat_lang')->orderBy('posts.id', 'desc')->get();


        $slider_little_posts = DB::table('posts')->where('slider_little_posts', 0)
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('post_translations.language_id', $lan->id)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'post_translations.title', 'post_translations.description', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('post_date', 'desc')->get();

        $slider = DB::table('posts')->where('slider_little_posts', 1)
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'post_translations.title', 'category_translations.category_name', 'category_translations.language_id as cat_lang')->orderBy('posts.id', 'desc')->get();


        $cate = DB::table('category_translations')
            ->join('language', 'category_translations.language_id', 'language.id')
            ->where('language.id', $lan->id)->get();

        $firstcat = DB::table('category_translations')->join('language', 'category_translations.language_id', 'language.id')
            ->where('language.id', $lan->id)->first();

        $firstcatpostbig = DB::table('posts')->where('category_id', $firstcat->id)->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.id', $lan->id)->where('is_big_thumbnail', 1)->first();

        $firstcatpostsmall = DB::table('posts')->where('is_big_thumbnail', NULL)->join('post_translations', 'post_translations.post_id', 'posts.id')->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.id', $lan->id)->inRandomOrder()->limit(3)->get();


        //Problem error
        $cat = DB::table('category_translations')->join('language', 'category_translations.language_id', 'language.id')
            ->join('posts', 'posts.category_id', 'category_translations.category_id')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->where('language.id', $lan->id)->limit(5)->get();
        //$art->video_links->skip(10)->take(10)->get();

        $catmore = DB::table('category_translations')->skip(5)->take(PHP_INT_MAX)->get();

        $latest = DB::table('posts')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->select('posts.*', 'post_translations.title', 'category_translations.category_name', 'post_translations.description')->where('posts.is_active', 1)
            ->orderBy('post_date', 'desc')->limit(5)->get();


        $latest_second_section = DB::table('posts')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())->select('posts.*', 'post_translations.title', 'post_translations.description')->orderBy('post_date', 'desc')->skip(5)->limit(5)->get();

        $popular = DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())
            ->whereDate('posts.created_at', Carbon::today())->orderBy('view_count', 'desc')->limit(5)->get();

        $most_viewed_news = DB::table('posts')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('post_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->where('posts.view_count', '!=', 0)
            ->select('posts.id as id', 'posts.image', 'posts.view_count', 'post_translations.title', 'category_translations.category_name','category_translations.category_id', 'posts.post_date')->orderBy('view_count', 'desc')->get();

        $multimedia_big = DB::table('posts')->where('is_photo_content', 1)->orWhere('is_video_content', 1)
            ->join('videos', 'videos.post_id', 'posts.id')
            ->join('audio', 'audio.post_id', 'posts.id')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->orderBy('posts.post_date', 'desc')->limit(1)->first();
        $multimedia_small = DB::table('posts')->where('is_photo_content', 1)->orWhere('is_video_content', 1)
            ->join('videos', 'videos.post_id', 'posts.id')
            ->join('audio', 'audio.post_id', 'posts.id')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->orderBy('posts.post_date', 'desc')->orderBy('posts.post_date', 'desc')->skip(1)->limit(2)->get();


        $weekly_trend = DB::table('posts')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('users', 'posts.writer_id', 'users.id')
            ->where('posts.created_at', '>', Carbon::now()->subWeek())->select('posts.*', 'post_translations.title', 'users.name')->where('post_translations.language_id', $lan->id)->orderBy('view_count', 'desc')->get();
        $photo_big = DB::table('photos')->orderBy('id', 'desc')->first();
        $photo_small = DB::table('photos')->orderBy('id', 'desc')->limit(5)->get();
        $video_big = DB::table('videos')->join('posts', 'posts.id', 'videos.post_id')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->select('posts.id as id', 'posts.image', 'posts.is_video_content', 'posts.post_date', 'post_translations.title', 'post_translations.title', 'post_translations.description', 'category_translations.category_name')
            ->where('post_translations.language_id', $lan->id)->where('posts.is_active', 1)->orderBy('post_date', 'desc')->first();

        $video_small = DB::table('videos')->join('posts', 'posts.id', 'videos.post_id')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->select()
            ->where('post_translations.language_id', $lan->id)->where('posts.is_active', 1)->orderBy('posts.post_date', 'desc')
            ->select('posts.id as id', 'posts.image', 'posts.is_video_content', 'posts.post_date', 'post_translations.title', 'post_translations.title', 'post_translations.description', 'category_translations.category_name')
            ->skip(1)->take(2)->get();
        $video_footer = DB::table('videos')->join('posts', 'posts.id', 'videos.post_id')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('post_translations.language_id', $lan->id)->where('posts.is_active', 1)->orderBy('posts.post_date', 'desc')
            ->select('posts.id as id', 'posts.is_video_content', 'posts.image', 'posts.post_date', 'post_translations.title', 'post_translations.title', 'post_translations.description', 'category_translations.category_name')
            ->skip(3)->take(2)->get();

        $headline = \Illuminate\Support\Facades\DB::table('posts')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->select('posts.*', 'post_translations.title', 'post_translations.description')
            ->leftjoin('language', 'language.id', 'post_translations.language_id')
            ->where('language.local', \Illuminate\Support\Facades\App::getLocale())
            ->where('posts.is_photo_content', 1)->get();
        $notices = \Illuminate\Support\Facades\DB::table('notices')->first();


        $query_post_translations = DB::table('posts')
            ->rightjoin('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('post_translations.language_id', $lan->id)->latest('post_date')->get();

        $bigPostListToCategory = $query_post_translations->where('is_big_thumbnail', 1);


        $smallPostListToCategory = $query_post_translations->where('is_big_thumbnail', null);
        $banner_ad = DB::table('ads')->where('type', 0)->first();

        $all = DB::table('segments')->join('segment_categories', 'segment_categories.segment_id', 'segments.id')
            ->join('categories', 'categories.id', 'segment_categories.category_id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->select('categories.id as category_id', 'category_translations.category_name','segment_categories.segment_id','segment_categories.is_main_category')
            ->where('segments.is_active',1)
            ->where('language_id', $lan->id)->get()->groupBy('segment_id');
        $if_not_main_posts= DB::table('posts')
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'categories.id as cat_id','post_translations.title', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('posts.id', 'desc')->get()->groupBy('cat_id') ->map(function ($query) {
                return $query->skip(1)->take(3);
            });
        $if_posts_big= DB::table('posts')
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'categories.id as cat_id','post_translations.title', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('posts.id', 'desc')->get()->groupBy('cat_id') ->map(function ($query) {
                return $query->take(1);
            });
        $if_main_posts= DB::table('posts')
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*', 'language.id as lang_id', 'categories.id as cat_id','post_translations.title', 'category_translations.category_name', 'category_translations.language_id as cat_lang')
            ->orderBy('posts.id', 'desc')->get()->groupBy('cat_id') ->map(function ($query) {
                return $query->skip(1)->take(2);
            });

        $cat_ids=[];
//        dd($if_not_main_posts);
        $counter=0;
        foreach ($all as $key=> $value)
        {
            foreach($value as $k=>$v)
            {

               if ($value[0]->is_main_category == 1)
               {

                   list($value[0],$value[1]) = array($value[1],$value[0]);
               }
                elseif ($value[2]->is_main_category == 1)
                {
                    list($value[2],$value[1]) = array($value[1],$value[2]);

                }
            }

        }
//dd($all);
//        dd($is_not_main_posts);

        $seg = DB::table('segments')->get();
        $categories = DB::table('categories')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->select('categories.id', 'category_translations.category_name')
            ->where('language_id', $lan->id)
            ->get();
        $seg_cat = DB::table('segment_categories')->get();

        $cat_names = [];
        $array = [];

        $segment = Segment::all();
        $cat_has_seg = DB::table('segment_categories')->get();
        $cat_id = [];
        foreach ($segment as $item) {
            foreach ($item->categories as $c)
                $array[$item->id][] = $c->id;
        }

        $areas=DB::table('areas')->join('area_translations','areas.id','area_translations.area_id')->where('language_id', $lan->id)->select('areas.id as id','area_translations.area_name')->get();
        $area_news= DB::table('posts')
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->join('language', 'post_translations.language_id', 'language.id')
            ->where('language.local', App::getLocale())
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $lan->id)
            ->join('areas','areas.id','posts.area_id')->join('area_translations','areas.id','area_translations.area_id')->where('area_translations.language_id', $lan->id)
            ->where('posts.is_active', 1)
            ->select('posts.*','area_translations.area_name', 'language.id as lang_id', 'post_translations.title', 'category_translations.category_name', 'category_translations.language_id as cat_lang')->orderBy('posts.post_date', 'desc')->limit(6)->get();



        return view('main.home')->with(compact('if_posts_big','if_main_posts','if_not_main_posts','area_news','areas','array','multimedia_small','multimedia_big','all','segment','cat_has_seg','array','most_viewed_news','video_footer','cat_names','categories', 'seg', 'seg_cat', 'audio_news', 'most_important_news', 'banner_ad', 'smallPostListToCategory', 'bigPostListToCategory', 'headline', 'notices', 'heading', 'if_today_heading_not_found', 'if_any_heading_content_not_found', 'is_sub_heading', 'slider_little_posts', 'slider', 'cate', 'firstcat', 'firstcatpostbig', 'firstcatpostsmall', 'cat', 'catmore', 'latest', 'latest_second_section', 'popular', 'weekly_trend', 'photo_big', 'photo_small', 'video_big', 'video_small'));
    }

}

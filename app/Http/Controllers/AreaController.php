<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\AreaTranslation;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AreaController extends Controller
{
    public function AddArea()
    {
        return view('backend.area.create');
    }

    public function StoreArea(Request $request)
    {
        $validator =  $request->validate([
            "area_name"    => "required|array|min:3",
            "area_name.*"  => "required|string|min:3",
        ]);
        $is_active = $request->is_active ? 1 : 0;
        $notification = array(
            'message' => 'Area Inserted successfully',
            'alert-type' => 'success'
        );
        $area= Area::create(['is_active'=>$is_active]);
        foreach (Language::all() as $key => $value) {
            $translated[$key] = [
                'area_name' => $request->input("area_name.$value->id"),
                'language_id' => $value->id,
                'area_id' => $area->id
            ];
        }
        AreaTranslation::insert($translated);
        return Redirect()->route('index.area')->with($notification);

    }
    public function Index()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $area = DB::table('areas')->orderBy('areas.id', 'desc')
            ->Join('area_translations', 'areas.id', 'area_translations.area_id')
            ->where('area_translations.language_id', $defaultLanguageId)
            ->select('area_translations.area_name','areas.id')
            ->paginate(3);
        return view('backend.area.index', compact('area'));
    }

    public function EditArea($id)
    {
        $area = DB::table('areas')->where('id', $id)->first();
        $translations = DB::table('areas')
            ->join('area_translations', 'areas.id', 'area_translations.area_id')
//            ->join('language', 'category_translations.language_id', 'language.id')
            ->where('areas.id', $id)
            ->get();
//        dd($translations);

        return view('backend.area.edit', compact('translations','area'));
    }

    public function UpdateArea(Request $request,$id)
    {
        $data = array();
        $notification = array();
        $data['is_active'] = $request->is_active ? 1 : 0;
        foreach ($request->area_name as $language => $area_name) {

            $translation = AreaTranslation::where('area_id',$id)->where('language_id',$language)->first();
            $translation->area_name = $area_name;
            if ($translation->isDirty()) {
                $validate_array = ['area_name' => 'required|min:1|max:255'.$id];
                $validate_array["$area_name"] = 'required'.$id;
                $this->validate($request, $validate_array );
                $translation->save();
                $notification = array(
                    'message' => 'Area Edited successfully',
                    'alert-type' => 'success'
                );
            }

        }
        DB::table('areas')->where('id',$id)->update($data);

        return Redirect()->route('index.area')->with($notification);
    }

    public function DeleteArea($id)
    {
        $area=DB::table('areas')->where('id',$id)->delete();
        $notification = array(
            'message' => 'Area Deleted successfully',
            'alert-type' => 'error'
        );
        return redirect()->route('index.area')->with($notification);
    }
}

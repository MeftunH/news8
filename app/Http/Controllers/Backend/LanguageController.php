<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\AreaTranslation;
use App\Models\CategoryTranslation;
use App\Models\Language;
use App\Models\PostTranslation;
use App\Models\SubCategoryTranslation;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;




class LanguageController extends Controller
{
    function __construct()

    {

        $this->middleware('permission:language-read|language-create|language-edit|language-delete', ['only' => ['index']]);

        $this->middleware('permission:language-create', ['only' => ['create','store']]);

        $this->middleware('permission:language-edit', ['only' => ['edit','update']]);

        $this->middleware('permission:language-delete', ['only' => ['delete']]);

    }


    public function GetChange($local)
    {
//        dd($local);
        $c=Language::where('local',$local)->count();
        if ($c!=0) {
            session(['locale' => $local]);
            Session::get('locale');
            Session()->forget('locale');
            Session()->put('locale', $local);
            App::setLocale($local);
            Carbon::setLocale($local);
            setLocale(LC_TIME, $local);
//            dd(App::currentLocale());
        }
        else{
            return abort(404);
        }
        $segments = str_replace(url('/'), '', url()->previous());
        $segments = array_filter(explode('/', $segments));
        array_shift($segments);
        array_unshift($segments, $local);

        return redirect()->to(implode('/', $segments));
    }

    public function Index()
    {
        $languages = Language::paginate(10);

        return view('backend.language.index', compact('languages'));
    }

    public function Create()
    {
        return view('backend.language.create');
    }

    public function Store(Request $request)
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        //I think about locale word has problem with config cause it has error which look like protected field.That is why i rename locale to local
        $validator = Validator::make($request->all(), [
            'code' => 'required|max:255',
            'name' => 'required|max:255',
            'local' => 'required|max:255',
            'status' => 'boolean',
        ]);
        if ($validator->fails()) {
            return redirect(route('language.create'))
                ->withErrors($validator)
                ->withInput();
        }
        $language = new Language();
        $language->code = $request->input('code');
        $language->name = $request->input('name');
        $language->local = $request->input('local');

        if ($request->get('status') == null) {
            $status = 0;
        } else {
            $status = request('status');
        }

        $language->status = $status;
        $image = $request->flag;
        if ($image) {
            $image_uni = uniqid() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(50, 50)->save('storage/flag_img/' . $image_uni);
            $language->flag = 'storage/flag_img/' . $image_uni;

        }
        $temp = base_path();
        $uploads_dir = $temp . '/resources/lang/' . $language->local;
        $chmod = 0777;
        if (!file_exists($uploads_dir)) {
            File::makeDirectory($uploads_dir, $chmod, true, true);
        } else {
            return Redirect::back()->withErrors(['This language already exist']);
        }
        $info = json_encode(['name' => $language->name, 'local' => $language->local]);
        $fopen = fopen($temp . '/resources/lang/' . $language->local . '/info.json', 'w+');
        fwrite($fopen, $info);
        fclose($fopen);
        $files = scandir($temp . '/resources/lang/en');
        foreach ($files as $file) {
            if ($file != '.' && $file !== '..' && $file != 'info.json') {
                $copy = copy($temp . '/resources/lang/en/' . $file, $temp . '/resources/lang/' . $language->local . '/' . $file);
            }
        }

        if ($copy) {
            $language->save();
            $post = PostTranslation::where('language_id', $defaultLanguage->id)->get();
            $category = CategoryTranslation::where('language_id', $defaultLanguage->id)->get();
            $subcategory = SubCategoryTranslation::where('language_id', $defaultLanguage->id)->get();
            $area = AreaTranslation::where('language_id', $defaultLanguage->id)->get();

            foreach ($category as $item) {
                $rep = $item->replicate();
                $rep->language_id = $language->id;
                $rep->save();
            }
            foreach ($subcategory as $item) {
                $rep = $item->replicate();
                $rep->language_id = $language->id;
                $rep->save();
            }
            foreach ($post as $item) {
                $rep = $item->replicate();
                $rep->language_id = $language->id;
                $rep->save();
            }
            foreach ($area as $item) {
                $rep = $item->replicate();
                $rep->language_id = $language->id;
                $rep->save();
            }
        } else {
            return Redirect::back()->withErrors(['This language already exist']);
        }

        return redirect(route('language.all'));

    }

    public function EditTranslation(Request $request)
    {

        if (!is_null($request->input('edit'))) {

            $locale_req = $request->input('edit');
            $language = DB::table('language')->where('local', $locale_req)->first();
            $folder = $language->local;
            $file = (!is_null($request->input('file')) ? $request->input('file') : 'auth.php');
            $files = scandir(base_path() . '/resources/lang/' . $folder . '/');
//        dd($file);
            $str = File::getRequire(base_path() . '/resources/lang/' . $folder . '/' . $file);
            $data = [
                'file' => $file,
                'files' => $files,
                'lang' => $language->local,
                'stringLangs' => $str,
            ];
            return view('backend.translations.edit', compact('language', 'data'));
        } else
            return redirect(route('language.edit'))
                ->withErrors('Error')
                ->withInput();
    }

    public function SaveTranslation(Request $request)
    {
        $input = $request->all();

        $form = "<?php \n";
        $form .= "return [ \n";
        foreach ($_POST as $key => $val) {
            if ($key != '_token' && $key != 'flag' && $key != 'lang' && $key != 'file' && $key != 'site_title' && $key != 'site_description' && $key != 'site_keywords') {
                if (!is_array($val)) {
                    $form .= '"' . $key . '" => "' . strip_tags($val) . '", ' . " \n";
                }
            }
        }
        $form .= "'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
     'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
     'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
       'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],

    'custom' => [
        'attribute-name' => [
                'rule-name' => 'custom-message',
        ],
    ],
     'attributes' => [],";
        $form .= "]; \n";

        $lang = $request->input('lang');
        $file = $request->input('file');
        $fileName = base_path() . '/resources/lang/' . $lang . '/' . $file;
        $fp = fopen($fileName, "w+");
        fwrite($fp, $form);
        fclose($fp);
        return redirect()->back();

    }


    public function Edit($id, Request $request)
    {

        $language = DB::table('language')->where('id', $id)->first();
        return view('backend.language.edit', compact('language'));
    }

    public function Update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|max:255',
            'name' => 'required|max:255',
            'local' => 'required|max:255',
            'status' => 'boolean',
        ]);
        if ($validator->fails()) {
            return redirect(route('language.edit'))
                ->withErrors($validator)
                ->withInput();
        }
        $language = Language::find($id);
        $language->code = $request->input('code');
        $language->name = $request->input('name');
        $language->local = $request->input('local');
        if ($request->get('status') == null) {
            $status = 0;
        } else {
            $status = request('status');
        }
        $language->status = $status;
        $oldflag = $request->oldflag;
        $image = $request->flag;
        if ($image) {
            $image_uni = uniqid() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(50, 50)->save('storage/flag_img/' . $image_uni);
            $language->flag = 'storage/flag_img/' . $image_uni;
            $language->save();
            unlink($oldflag);
            $notification = array(
                'message' => 'Language Edited successfully',
                'alert-type' => 'success'
            );

            return Redirect()->route('language.all')->with($notification);
        } else {
            $data['image'] = $oldflag;
            $language->save();
            $notification = array(
                'message' => 'Language Edited successfully',
                'alert-type' => 'success'
            );

        }
        return Redirect()->route('language.all')->with($notification);
    }


    public function Delete($id)
    {
        $language = DB::table('language')->where('id', $id)->first();

//        if ($language->flag && $language->flag != null)
//            unlink($language->flag);
        $languages = Language::orderBy('local', 'asc')->get();
        $pathUpload = 'backend/assets/uploads/';


        $path = base_path() . '/resources/lang/' . $language->local;
        if (is_dir($path)) {
//                    dd($path.$value);

            if ($language->local != 'en') {

                //delete directories
                File::deleteDirectory($path);


            }
        }


        DB::table('language')->where('id', $id)->delete();
        $notification = array(
            'message' => 'Language Deleted successfully',
            'alert-type' => 'error'
        );


        return Redirect()->route('language.all')->with($notification);
    }


}

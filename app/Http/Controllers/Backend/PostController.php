<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Audio;
use App\Models\Heading;
use App\Models\HeadingTranslation;
use App\Models\Language;
use App\Models\Photo;
use App\Models\Post;
use App\Models\PostTranslation;
use App\Models\Tag;

use App\Models\TagTranslation;
use App\Rules\NullableField;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\Validator;


class PostController extends Controller
{
    protected $audio_check;
    function __construct()

    {

        $this->middleware('permission:post-list|post-create|post-edit|post-delete', ['only' => ['index','show']]);

        $this->middleware('permission:post-create', ['only' => ['create','store']]);

        $this->middleware('permission:post-edit', ['only' => ['edit','update']]);

        $this->middleware('permission:post-delete', ['only' => ['destroy']]);

    }
    public function Index()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;

        $post = DB::table('posts')
            ->leftJoin('post_translations', 'posts.id', 'post_translations.post_id')
            ->where('post_translations.language_id', $defaultLanguageId)
            ->join('categories', 'posts.category_id', 'categories.id')
//            ->join('areas', 'posts.area_id', 'areas.id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
//            ->join('area_translations', 'area_translations.area_id', 'areas.id')
            ->where('category_translations.language_id', $defaultLanguageId)
//            ->where('area_translations.language_id', $defaultLanguageId)
//            ->join('subcategories', 'posts.subcategory_id', 'subcategories.id')
//            ->join('subcategory_translations', 'subcategory_translations.subcategory_id', 'subcategories.id')
//            ->where('subcategory_translations.language_id', $defaultLanguageId)
            ->select('posts.*', 'category_translations.category_name', 'post_translations.title')
            ->orderBy('id', 'desc')->paginate(5);
//        dd($post);
        return view('backend.post.index', compact('post'));
    }

    public function Create()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $category = DB::table('categories')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->select('categories.id', 'category_translations.category_name')
            ->where('language_id', $defaultLanguageId)
            ->get();
        $area= DB::table('areas')
            ->join('area_translations', 'area_translations.area_id', 'areas.id')
            ->select('areas.id', 'area_translations.area_name')
            ->where('language_id', $defaultLanguageId)
            ->get();

//        $tags= DB::table('tag_translations')->pluck('name','id');
        return view('backend.post.create', compact('category','area'));
    }

    public function StorePost(Request $request)
    {

        $translate = array();
        $translate['title'] = $request->title;
        $translate['description'] = $request->details;
        $translate['tags'] = $request->tags;

        $data = array();
        $data['writer_id'] = Auth::id();
        $data['category_id'] = $request->category_id;
        $data['area_id'] = $request->area_id;
        $data['subcategory_id'] = $request->subcategory_id;
        $data['is_photo_content'] = $request->is_photo_content;
        $data['is_sub_heading'] = $request->is_sub_heading;
        $data['slider_little_posts'] = $request->slider_little_posts;
        $data['is_big_thumbnail'] = $request->big_thumbnail;
        $data['unique_big_post'] = $request->unique_big_post;
        $data['is_most_important'] = $request->is_most_important;
        $data['is_video_content'] = $request->is_video_content;
        $data['heading'] = $request->heading;
        $data['is_active'] = $request->is_active;
        $post_date = Carbon::parse($request->post_date)->format('Y-m-d H:i:s');
        $data['post_date'] = $post_date;
        $data['month'] = date("F");
        $data['created_at'] = new DateTime();
        $data['is_audio_content'] = $request->is_audio_content;

        $audio_data['link'] = $request->link;
        $audio_data['video_link'] = $request->video_link;
        $audio_data['url'] = $request->url;
//heading fetch
        $heading_data = array();
        $heading_data['writer_id'] = Auth::id();
        $heading_data['category_id'] = $data['category_id'];
        $heading_data['subcategory_id'] = $data['subcategory_id'];
        $heading_data['is_active'] = $data['heading'];
        $heading_data['post_date'] = $post_date;
        $heading_data['month'] = date("F");
        $heading_data['created_at'] = new DateTime();
        $video_data['video_link'] = $request->video_link;
//
        $validateData = $request->validate([
            'category_id' => 'required',
            'details' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png,gif,svg|max:8192',

        ]);


        if ($request->is_photo_content == 1) {
            $validateData = $request->validate([
                'photo_link' => 'required',
                'photo_link.*' => 'mimes:jpeg,jpg,png,gif,csv|max:2048'
            ]);
        }
        if ($request->is_audio_content == 1) {
            $validateData = $request->validate([
                'url' => 'required_without:link|mimes:application/octet-stream,audio/mpeg,mpga,mp3,wav',
                'link' => [new NullableField(request('url'))],
            ]);
        }

        if ($request->category_id) {
            $request->unique_big_post = null;
        }

        if ($request->is_big_thumbnail == 1) {
            $request->unique_big_post = $request->category_id;
        } else {
            $request->unique_big_post = 0;
        }

        $image = $request->image;
        if ($image) {
            $image_uni = uniqid('', true) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(500, 300)->save('storage/post_img/' . $image_uni);
            $data['image'] = 'storage/post_img/' . $image_uni;
            $heading_data['image'] = 'storage/post_img/' . $image_uni;
            $post = Post::create($data);
            $heading_data['post_id'] = $post->id;
            $video_data['post_id'] = $post->id;

            if ($request->is_video_content == 1) {
                $validateData = $request->validate([
                    'video_link' => 'required'
                ]);
                DB::table('videos')->insert($video_data);

            }
            if ($request->heading == 1) {
                $head = Heading::create($heading_data);
            }
            $notification = array(
                'message' => 'Post Added successfully',
                'alert-type' => 'success'
            );
        }
        $audio_url = $request->url;
        if ($audio_url) {
            $audio_uni = uniqid('', true) . '.' . $audio_url->getClientOriginalExtension();
            $upload =  $upload = $request->file('url')->store('/storage/audio_files','public');

            if ($upload) {
                $audio_data['url'] = $upload;
            }
        }


        if ($post) {

            $validator =  $request->validate([
                "title.1"  => "required|string|min:3",
                "details.1"  => "required|string|min:3",
            ]);
            foreach (Language::all() as $key => $value) {
                $title = $request->input("title.$value->id");
                $description = $request->input("details.$value->id");
                if($title == null)
                {
                    $title = $request->input("title.1");
                }
                if($description == null)
                {
                    $description = $request->input("details.1");
                }
                $translated[$key] = [
                    'title' => $title,
                    'description' => $description,
                    'language_id' => $value->id,
                    'post_id' => $post->id
                ];
                if ($request->heading == 1) {

                    $heading_translates[$key] = [
                        'title' => $title,
                        'description' => $description,
                        'language_id' => $value->id,
                        'heading_id' => $head->id,
                    ];
                }
                $tag_names = explode(',', $request->input("tags.$value->id"));
                $tag_ids = [];

                foreach ($tag_names as $item) {
                    $language_id = $value->id;
                    $tag = Tag::create(['post_id' => $post->id]);
                    $tag_ids[] = $tag->id;
                    TagTranslation::create(['name' => $item, 'language_id' => $language_id, 'tag_id' => $tag->id]);
                }
            }

            PostTranslation::insert($translated);
            if ($request->heading == 1) {
                HeadingTranslation::insert($heading_translates);
            }
            if ($request->is_audio_content == 1) {
                $audio_data['post_id'] = $post->id;
                Audio::insert($audio_data);
            }
        } else {
            return Redirect()->route('all.post')->with($notification);
        }

        $photo_links = [];
        if ($request->hasfile('photo_link')) {
            $photo = $request->file('photo_link');

            foreach ($photo as $file) {

                $name = $file->getClientOriginalName();
                $path = $file->storeAs('storage/uploads', $name, 'public');
                $photo_links[] = $path;

            }
            $photoModal = new Photo();
            $photoModal->post_id = $post->id;
            $photoModal->photo_link = json_encode($photo_links);
            $photoModal->save();
        }

        return Redirect()->route('all.post')->with($notification);
    }

    public function EditPost($id)
    {

        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;

        $translations = DB::table('posts')->where('posts.id', $id)
            ->join('post_translations', 'posts.id', 'post_translations.post_id')
            ->get();


        $post = DB::table('posts')->where('posts.id', $id)
            ->join('categories', 'categories.id', 'posts.category_id')
            ->leftjoin('areas', 'areas.id', 'posts.area_id')
            ->select('posts.id as id', 'categories.id as cat_id', 'areas.id as ar_id','posts.area_id as area_id','posts.is_video_content', 'posts.is_audio_content', 'posts.category_id as category_id', 'posts.subcategory_id as subcategory_id', 'posts.post_date', 'posts.image as image', 'posts.is_photo_content as is_photo_content', 'posts.is_photo_content as is_video_content', 'posts.is_most_important as is_most_important', 'posts.is_big_thumbnail as is_big_thumbnail', 'posts.is_sub_heading as is_sub_heading', 'posts.slider_little_posts as slider_little_posts', 'posts.heading as heading', 'posts.is_active as is_active')
            ->first();
        $audio = DB::table('audio')->where('audio.post_id', $id)->first();
        if (isset($audio)) {
            $this->audio_check = true;
        } else {
            $this->audio_check = false;
        }
        $video = DB::table('videos')->where('videos.post_id', $id)->first();
//        $tags=array();
        $photo = DB::table('photos')->where('photos.post_id', $id)->first();
//        dd(json_decode($photo->photo_link,true));
        $tags = DB::table('tags')
            ->join('posts', 'posts.id', 'tags.post_id')
            ->leftjoin('tag_translations', 'tag_translations.tag_id', 'tags.id')
            ->join('language', 'tag_translations.language_id', 'language.id')
            ->where('tags.post_id', $id)
            ->select('tag_translations.name', 'tag_translations.language_id')
            ->get();

        $data = [];

        foreach ($tags as $tag) {
            $data[$tag->language_id][] = $tag->name;
        }

        $category = DB::table('categories')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->select('categories.id', 'category_translations.category_name')
            ->where('language_id', $defaultLanguageId)
            ->get();

        $area= DB::table('areas')
            ->join('area_translations', 'area_translations.area_id', 'areas.id')
            ->select('areas.id', 'area_translations.area_name')
            ->where('language_id', $defaultLanguageId)
            ->get();

        $sub = DB::table('subcategories')
            ->Join('subcategory_translations', 'subcategories.id', 'subcategory_translations.subcategory_id')
            ->where('subcategory_translations.language_id', $defaultLanguageId)
            ->join('posts', 'posts.category_id', 'subcategories.category_id')
            ->where('posts.id', $id)
            ->select('subcategories.id as id', 'subcategory_translations.subcategory_name as subcategory_name')->get();

        $is_sub_heading_count = DB::table('posts')->where('posts.id', $id)
            ->join('categories', 'categories.id', 'posts.category_id')->where('is_sub_heading', 1)->count();

        return view('backend.post.edit', compact('area','photo', 'video', 'audio', 'translations', 'category', 'post', 'sub'))->withTags($data);
    }

// show post method
    public function show(Post $post)
    {
        Redis::incr('post:' . $post->id . ':count');

        return view('posts.index', compact('post'));
    }

    public function UpdatePost(Request $request, $id)
    {

        //dd($request->all());
        $data = array();
        $data['writer_id'] = Auth::id();
        $data['category_id'] = $request->category_id;
        $data['area_id'] = $request->area_id;
        $data['subcategory_id'] = $request->subcategory_id;
        $data['is_photo_content'] = $request->is_photo_content;
        $data['is_sub_heading'] = $request->is_sub_heading;
        $data['slider_little_posts'] = $request->slider_little_posts;
        $data['heading'] = $request->heading;
        $data['is_audio_content'] = $request->is_audio_content;
        $audio_data['link'] = $request->link;
        $audio_data['url'] = $request->url;
        $video_data['video_link'] = $request->video_link;

        $post_date = Carbon::parse($request->post_date)->format('Y-m-d H:i:s');
        if (isset($post_date))
            $data['post_date'] = $post_date;
        else
            $data['post_date'] = Carbon::now()->format('Y-m-d H:i:s');

        if ($request->category_id) {
            $request->unique_big_post = null;
        }
        if ($request->is_big_thumbnail == 1) {
            $request->unique_big_post = $request->category_id;
        } else {
            $request->unique_big_post = null;

        }
//        if ($request->is_photo_content == 1)
//        {
//            $validateData = $request->validate([
//                'photo_link' => 'required',
//                'photo_link.*' => 'mimes:jpeg,jpg,png,gif,csv|max:2048'
//            ]);
//        }
        $data['is_big_thumbnail'] = $request->is_big_thumbnail;
        $data['unique_big_post'] = $request->unique_big_post;
        $data['is_most_important'] = $request->is_most_important;
        $data['is_video_content'] = $request->is_video_content;
        $data['is_active'] = $request->is_active;

        $heading_data = array();
        $heading_data['writer_id'] = Auth::id();
        $heading_data['post_id'] = $id;
        $heading_data['category_id'] = $request->category_id;
        $heading_data['subcategory_id'] = $request->subcategory_id;


        if (isset($post_date))
            $heading_data['post_date'] = $post_date;
        else
            $data['post_date'] = Carbon::now()->format('Y-m-d H:i:s');

        $heading_data['month'] = date("F");
        $heading_data['is_active'] = $request->heading;
        $oldimage = $request->oldimage;
        $image = $request->image;
        $check_unique = DB::table('heading')->where('post_id', $id)->first();
        $validateData = $request->validate([
            'category_id' => 'required',
            'title' => 'required',
            'details' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif,svg|max:8192',

        ]);
        if ($request->is_audio_content != 1) {
            $validateData = $request->validate([
                'url' => ['mimes:mpga,mp3,wav', new NullableField(request('url'), 'If url null then must be not null in this field')],
                'link' => [new NullableField(request('url'), 'If url null then must be not null in this field')],
            ]);
        }

        if ($image) {
            $image_uni = uniqid() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(500, 300)->save('storage/post_img/' . $image_uni);
            $data['image'] = 'storage/post_img/' . $image_uni;
            $heading_data['image'] = 'storage/post_img/' . $image_uni;
            if ($request->heading == 1 && $check_unique == null) {
                $_head = Heading::create($heading_data);
                foreach (Language::all() as $key => $value) {
                    $heading_translates[$key] = [
                        'title' => $request->input("title.$value->id"),
                        'description' => $request->input("details.$value->id"),
                        'language_id' => $value->id,
                        'heading_id' => $_head->id,
                    ];
                }
                HeadingTranslation::insert($heading_translates);

            }

            DB::table('posts')->where('id', $id)->update($data);

            DB::table('heading')->where('post_id', $id)->update($heading_data);
            unlink($oldimage);
            $notification = array(
                'message' => 'Post Edited successfully',
                'alert-type' => 'success'
            );

            return Redirect()->route('all.post')->with($notification);
        } else {
            $data['image'] = $oldimage;
            $heading_data['image'] = $oldimage;
            if ($request->heading == 1 && $check_unique == null) {
                $head = Heading::create($heading_data);
                foreach (Language::all() as $key => $value) {
                    $heading_translates[$key] = [
                        'title' => $request->input("title.$value->id"),
                        'description' => $request->input("details.$value->id"),
                        'language_id' => $value->id,
                        'heading_id' => $head->id,
                    ];
                }
                HeadingTranslation::insert($heading_translates);
//                dd($heading_data);
            }


            DB::table('posts')->where('id', $id)->update($data);


            DB::table('heading')->where('post_id', $id)->update($heading_data);
            $notification = array(
                'message' => 'Post Edited successfully',
                'alert-type' => 'success'
            );

        }
        $audio_data['post_id'] = $id;
        $video_data['post_id'] = $id;
        $audio_url = $request->url;

        if ($request->video_link) {

            $up_data = DB::table('videos')->where('post_id', $id)->update($video_data);

            if ($up_data != 0)
                DB::table('videos')->insert($video_data);
        }
        if ($request->url) {
            $audio_uni = uniqid() . '.' . $audio_url->getClientOriginalExtension();
            $upload = $request->file('url')->store('/storage/audio_files','public');

            $audio_data['url'] = $upload;

            $up = DB::table('audio')->where('post_id', $id)->update($audio_data);

            if ($up == 0)
                DB::table('audio')->insert($audio_data);
        }
        elseif ($request->link)
        {
            $audio_data['link'] = $request->link;

            $up = DB::table('audio')->where('post_id', $id)->update($audio_data);

            if ($up == 0)
                DB::table('audio')->insert($audio_data);
        }

        $tags = Tag::where('post_id', $id)->delete();
        foreach ($request->title as $language => $title) {
            $translation = PostTranslation::where('post_id', $id)->where('language_id', $language)->first();
            $translation->title = $title;
            $translation->description = $request->details[$language];
            if ($translation->isDirty()) {
                $translation->save();
                $notification = array(
                    'message' => 'Post Edited successfully',
                    'alert-type' => 'success'
                );
            }
            $tag_names = explode(',', $request->tags[$language]);
            foreach ($tag_names as $item) {
                $tag = Tag::create(['post_id' => $id]);
                $tag_ids[] = $tag->id;
                TagTranslation::create(['name' => $item, 'language_id' => $language, 'tag_id' => $tag->id]);

            }

        }
        $photo_links = [];
        if ($request->hasfile('photo_link')) {

            Photo::where('post_id', $id)->delete();
            $photo = $request->file('photo_link');

            foreach ($photo as $file) {

                $name = $file->getClientOriginalName();
                $path = $file->storeAs('storage/uploads', $name, 'public');
                $photo_links[] = $path;

            }
            $photoModal = new Photo();
            $photoModal->post_id = $id;
            $photoModal->photo_link = json_encode($photo_links);
            $photoModal->save();
        }


        return Redirect()->route('all.post')->with($notification);
    }


    public function DeletePost($id)
    {
        $post = DB::table('posts')->where('id', $id)->first();
        if ($post->heading == 1) {
            $heading = DB::table('heading')->where('post_id', $id)->first();
            unlink($heading->image);
        }
        unlink($post->image);

        DB::table('posts')->where('id', $id)->delete();
        DB::table('heading')->where('post_id', $id)->delete();
        Tag::where('post_id', $id)->delete();
        $notification = array(
            'message' => 'Post Deleted successfully',
            'alert-type' => 'error'
        );
        return Redirect()->route('all.post')->with($notification);
    }

    public function GetSubCategory($category_id)
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;

        $sub = DB::table('subcategories')
            ->leftjoin('categories', 'categories.id', 'subcategories.category_id')
            ->Join('subcategory_translations', 'subcategories.id', 'subcategory_translations.subcategory_id')
            ->where('subcategory_translations.language_id', $defaultLanguageId)
            ->where('category_id', $category_id)->get();
        return response()->json($sub);

    }


}

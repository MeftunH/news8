<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class GalleryController extends Controller
{
    public function PhotoGallery()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;

        $photos = DB::table('photos')->join('posts','posts.id','photos.post_id')
            ->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->where('post_translations.language_id', $defaultLanguageId)
            ->where('posts.is_active',1)
            ->orderBy('posts.post_date', 'desc')
            ->select('posts.id as id','posts.is_photo_content','posts.image','posts.post_date','post_translations.title','post_translations.title','photos.photo_link','post_translations.description','category_translations.category_name')
            ->orderBy('id', 'desc')->paginate(5);
        return view('backend.gallery.photos', compact('photos'));
    }

    public function AddPhoto()
    {
        return view('backend.gallery.create_photo');
    }

    public function StorePhoto(Request $request)
    {

        $data = array();
        $data['title'] = $request->title;
        $data['type'] = $request->type;


        $image = $request->photo;
        if ($image) {
            $image_one = uniqid() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(500, 300)->save('image/photo_gallery/' . $image_one);
            $data['photo'] = 'image/photo_gallery/' . $image_one;
            DB::table('photos')->insert($data);

            $notification = array(
                'message' => 'Photo Inserted Successfully',
                'alert-type' => 'success'
            );

            return Redirect()->route('photo.gallery')->with($notification);

        } else {
            return Redirect()->back();
        }

    }

    public function EditPhoto($id)
    {
        $photo = DB::table('photos')->where('id', $id)->first();
        return view('backend.gallery.edit_photo', compact('photo'));
    }

    public function UpdatePhoto(Request $request, $id)
    {
        $data = array();
        $data['title'] = $request->title;
        $data['type'] = $request->type;

        $oldimage = $request->oldimage;
        $image = $request->photo;
        if ($image) {
            $image_uni = uniqid() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(500, 300)->save('image/photo_gallery/' . $image_uni);
            $data['photo'] = 'image/photo_gallery/' . $image_uni;
            DB::table('photos')->where('id', $id)->update($data);
            unlink($oldimage);
            $notification = array(
                'message' => 'Post Edited successfully',
                'alert-type' => 'success'
            );

            return Redirect()->route('photo.gallery')->with($notification);
        } else {
            $data['photo'] = $oldimage;
            DB::table('photos')->where('id', $id)->update($data);

            $notification = array(
                'message' => 'Photo Edited successfully',
                'alert-type' => 'success'
            );

        }
        return Redirect()->route('photo.gallery')->with($notification);

    }

    public function DeletePhoto($id)
    {
        $photos = DB::table('photos')->where('id', $id)->first();
        unlink($photos->photo);
        DB::table('photos')->where('id', $id)->delete();
        $notification = array(
            'message' => 'Photo Deleted successfully',
            'alert-type' => 'error'
        );
        return Redirect()->route('photo.gallery')->with($notification);
    }

    public function VideoGallery()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;

        $video = DB::table('videos')->join('posts','posts.id','videos.post_id')->join('post_translations', 'post_translations.post_id', 'posts.id')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'posts.category_id', 'category_translations.category_id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->where('post_translations.language_id', $defaultLanguageId)
            ->where('posts.is_active',1)->orderBy('posts.post_date', 'desc')
            ->select('posts.id as id','posts.is_video_content','posts.image','posts.post_date','post_translations.title','post_translations.title','videos.video_link','post_translations.description','category_translations.category_name')
            ->orderBy('id', 'desc')->paginate(5);
        return view('backend.gallery.video', compact('video'));
    }

    public function AddVideo()
    {
        return view('backend.gallery.create_video');
    }

    public function StoreVideo(Request $request)
    {
        $data = array();
        $data['title'] = $request->title;
        $data['type'] = $request->type;
        $data['link'] = $request->link;

        DB::table('videos')->insert($data);

        $notification = array(
            'message' => 'Video Inserted Successfully',
            'alert-type' => 'info'
        );

        return Redirect()->route('video.gallery')->with($notification);
    }

    public function EditVideo($id)
    {
        $video = DB::table('videos')->where('id', $id)->first();
        return view('backend.gallery.edit_video', compact('video'));
    }

    public function UpdateVideo(Request $request, $id)
    {
        $data = array();
        $data['title'] = $request->title;
        $data['type'] = $request->type;
        $data['link'] = $request->link;
        DB::table('videos')->where('id', $id)->update($data);
            $notification = array(
                'message' => 'Video Edited successfully',
                'alert-type' => 'success'
            );

            return Redirect()->route('video.gallery')->with($notification);

    }

    public function DeleteVideo($id)
    {
        $videos = DB::table('videos')->where('id', $id)->first();
        DB::table('videos')->where('id', $id)->delete();
        $notification = array(
            'message' => 'Video Deleted successfully',
            'alert-type' => 'error'
        );
        return Redirect()->route('video.gallery')->with($notification);
    }

}

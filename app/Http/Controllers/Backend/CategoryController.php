<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Models\Category;
use App\Models\CategoryTranslation;
use App\Models\Language;
use Brian2694\Toastr\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function Index()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $category = DB::table('categories')->orderBy('categories.id', 'desc')
        ->Join('category_translations', 'categories.id', 'category_translations.category_id')
        ->where('category_translations.language_id', $defaultLanguageId)
            ->select('category_translations.category_name','categories.id')
        ->paginate(3);
        return view('backend.category.index', compact('category'));
    }

    public function AddCategory()
    {
        return view('backend.category.create');
    }

    public function StoreCategory(Request $request)
    {
        $validate_array = ['category_name' => 'required|unique:category_translations|max:255'];
        $status = $request->status ? 1 : 0;
        $notification = array(
            'message' => 'Category Inserted successfully',
            'alert-type' => 'success'
        );
        $category = Category::create(['status'=>$status]);
        foreach (Language::all() as $key => $value) {
            $validate_array["category_name.$value->id"] = 'required';
            $translated[$key] = [
                'category_name' => $request->input("category_name.$value->id"),
                'language_id' => $value->id,
                'category_id' => $category->id
            ];
        }
        $this->validate($request, $validate_array );
        CategoryTranslation::insert($translated);
        return Redirect()->route('categories')->with($notification);


    }

    public function EditCategory($id)
    {
        $category = DB::table('categories')->where('id', $id)->first();
        $translations = DB::table('categories')
            ->join('category_translations', 'categories.id', 'category_translations.category_id')
//            ->join('language', 'category_translations.language_id', 'language.id')
            ->where('categories.id', $id)
            ->get();
//        dd($translations);

        return view('backend.category.edit', compact('translations','category'));
    }

    public function UpdateCategory(Request $request,$id)
    {

        $data = array();
        $notification = array();
        $data['status'] = $request->status ? 1 : 0;
        foreach ($request->category_name as $language => $category_name) {

            $translation = CategoryTranslation::where('category_id',$id)->where('language_id',$language)->first();
            $translation->category_name = $category_name;
            if ($translation->isDirty()) {
                $validate_array = ['category_name' => 'required|min:1|max:255'.$id];
                $validate_array["$category_name"] = 'required'.$id;
                $this->validate($request, $validate_array );
                $translation->save();
                $notification = array(
                    'message' => 'Category Edited successfully',
                    'alert-type' => 'success'
                );
            }

        }
        DB::table('categories')->where('id',$id)->update($data);

        return Redirect()->route('categories')->with($notification);
    }
    public function DeleteCategory($id)
    {
        $category=DB::table('categories')->where('id',$id)->delete();
        $notification = array(
            'message' => 'Category Deleted successfully',
            'alert-type' => 'error'
        );
        return redirect()->route('categories')->with($notification);
    }
}

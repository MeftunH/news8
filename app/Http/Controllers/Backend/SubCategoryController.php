<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryTranslation;
use App\Models\Language;
use App\Models\SubCategory;
use App\Models\SubCategoryTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubCategoryController extends Controller
{
    public function Index()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;

        $subcategory = DB::table('subcategories')
            ->leftjoin('categories', 'categories.id', 'subcategories.category_id')
            ->Join('subcategory_translations', 'subcategories.id', 'subcategory_translations.subcategory_id')
            ->where('subcategory_translations.language_id', $defaultLanguageId)
            ->join('category_translations', 'category_translations.category_id', 'subcategories.category_id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->select('subcategories.id as id', 'subcategory_translations.subcategory_name as subcategory_name', 'categories.id as cat_id', 'category_translations.category_name')
            ->orderBy('subcategories.id', 'desc')->paginate(3);
//        $category=CategoryTranslation::all()
//            ->join('subcategories','subcategories.category_id','categories.id')
//            ->first();
//dd($subcategory);
        return view('backend.subcategory.index', compact('subcategory'));
    }

    public function AddSubCategory()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $category = DB::table('categories')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->select('categories.id', 'category_translations.category_name')
            ->where('language_id', $defaultLanguageId)
            ->get();
        return view('backend.subcategory.create', compact('category'));
    }

    public function StoreSubCategory(Request $request)
    {
        $validate_array = ['subcategory_name' => 'required|unique:subcategory_translations|max:255', 'category_id' => 'required'];
//        $validateData = $request->validate([
//            'subcategory_en' => 'required|unique:subcategories|max:255',
//            'subcategory_az' => 'required|unique:subcategories|max:255',
//           'category_id' =>'required',
//        ]);
//        $data['subcategory_name'] = $request->subcategory_name;
        $category_id = $request->category_id;
        if ($category_id != null)
            $subcategory = SubCategory::create(['category_id' => $category_id]);
        else {
            $this->validate($request, $validate_array);
        }
        $notification = array(
            'message' => 'SubCategory Inserted successfully',
            'alert-type' => 'success'
        );
//            $data = [
//                'category_en'=>$request->category_en,
//            ];
        foreach (Language::all() as $key => $value) {
            $validate_array["subcategory_name.$value->id"] = 'required';
            $this->validate($request, $validate_array);
            $translated[$key] = [
                'subcategory_name' => $request->input("subcategory_name.$value->id"),
                'language_id' => $value->id,
                'subcategory_id' => $subcategory->id
            ];
        }

        SubCategoryTranslation::insert($translated);


        return Redirect()->route('subcategories')->with($notification);
    }

    public function EditSubCategory($id)
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;

        $get_subcategory = DB::table('subcategories')->where('subcategories.id', $id)
            ->first();
//        dd($get_subcategory);
        $data = DB::table('subcategories')
            ->join('categories', 'categories.id', 'subcategories.category_id')
            ->join('subcategory_translations','subcategory_translations.subcategory_id','subcategories.id')
            ->join('category_translations', 'category_translations.category_id', 'subcategories.category_id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->where('subcategories.id',$id)
            ->select('subcategories.id as id', 'subcategory_translations.subcategory_name as subcategory_name', 'categories.id as cat_id', 'category_translations.category_name', 'subcategory_translations.language_id as language_id')
            ->get();
        $get_category = DB::table('categories')
            ->join('category_translations','category_id','categories.id')
            ->select('categories.id as cat_id','category_translations.category_name')
            ->where('language_id',$defaultLanguageId)
            ->get();
//        dd($get_category);
        return view('backend.subcategory.edit', compact('get_subcategory', 'data','get_category'));
    }

    public function UpdateSubCategory(Request $request, $id)
    {
        $data=array();
        $data['category_id'] = $request->category_id;
        foreach ($request->subcategory_name as $language => $subcategory_name) {

            $translation = SubCategoryTranslation::where('subcategory_id',$id)->where('language_id',$language)->first();
            $translation->subcategory_name = $subcategory_name;
            if ($translation->isDirty()) {
//                $validate_array = ['subcategory_name' => 'required|unique:subcategory_translations|max:255'.$id];
//                $validate_array["$subcategory_name"] = 'required'.$id;
//                $this->validate($request, $validate_array );
                $translation->save();
                $notification = array(
                    'message' => 'Category Edited successfully',
                    'alert-type' => 'success'
                );
            }

        }
        DB::table('subcategories')->where('id', $id)->update($data);
        $notification = array(
            'message' => 'SubCategory Updated successfully',
            'alert-type' => 'warning'
        );
        return Redirect()->route('subcategories')->with($notification);
    }

    public function DeleteSubCategory($id)
    {
        $subcategory = DB::table('subcategories')->where('id', $id)->delete();
        $notification = array(
            'message' => 'SubCategory Deleted successfully',
            'alert-type' => 'error'
        );
        return redirect()->route('subcategories')->with($notification);
    }

}

<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class AdsController extends Controller
{
    public function AdList()
    {
        $ads=DB::table('ads')->orderBy('id','desc')->get();
        return view('backend.ads.index',compact('ads'));
    }
    public function Create()
    {
        return view('backend.ads.create');
    }

    public function Store(Request $request)
    {
        $data = array();
        $data['link']=$request->link;
        $data['type']=$request->type;
        $image=$request->ads;
        $validated = $request->validate([
            'link' => 'required',
            'ads' => 'required',
        ]);

        if ($request->type == 0) {
            if ($image) {
                $image_uni = uniqid() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(1170, 282)->save('storage/ads/'.$image_uni);
                $data['ads'] = 'storage/ads/' . $image_uni;
                DB::table('ads')->insert($data);
            }
        }
        else
        {
            if ($image) {

                $image_uni = uniqid() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(500, 300)->save('storage/ads/'.$image_uni);
                $data['ads'] = 'storage/ads/' . $image_uni;
                DB::table('ads')->insert($data);
            }
        }
        $notification = array(
            'message' => 'Ad Inserted successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('ad.list');

    }
}

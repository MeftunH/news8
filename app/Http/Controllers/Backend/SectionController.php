<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SectionController extends Controller
{

    public function Slider()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $addedPosts = DB::table('posts')
            ->leftJoin('post_translations', 'posts.id', 'post_translations.post_id')
            ->where('post_translations.language_id', $defaultLanguageId)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->select('posts.*', 'category_translations.category_name', 'post_translations.title')
            ->orderBy('id', 'desc')->where('is_active', 1)
            ->where('slider_little_posts', 1)->get();
        return view('backend.section.slider.index')->with(compact('addedPosts', $addedPosts));
    }

    public function SliderCreate()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $addedPosts = DB::table('posts')
            ->leftJoin('post_translations', 'posts.id', 'post_translations.post_id')
            ->where('post_translations.language_id', $defaultLanguageId)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->select('posts.*', 'category_translations.category_name', 'post_translations.title')
            ->orderBy('id', 'desc')->where('is_active', 1)
            ->where('slider_little_posts', 0)->get();
        return view('backend.section.slider.create')->with(compact('addedPosts', $addedPosts));

    }


    public function SliderSave(Request $request)
    {


        $data=[];
        if (isset($request->slider_little_posts)) {
            foreach ($request->slider_little_posts as $key => $item) {
                $data[] = $key;

            }
            DB::table('posts')->wherenotIn('id', $data)->update(['slider_little_posts' => 0]);
        }
         if (isset($request->slider_little_posts_create)) {
             foreach ($request->slider_little_posts_create as $key => $item) {
                 $data[] = $key;
             }
             DB::table('posts')->whereIn('id', $data)->update(['slider_little_posts' => 1]);

         }
        return redirect()->route('section.slider');
    }

    public function MostImportantNews()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $addedPosts = DB::table('posts')
            ->leftJoin('post_translations', 'posts.id', 'post_translations.post_id')
            ->where('post_translations.language_id', $defaultLanguageId)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->select('posts.*', 'category_translations.category_name', 'post_translations.title')
            ->orderBy('id', 'desc')->where('is_active', 1)
            ->where('is_most_important', 1)->get();
        return view('backend.section.mostImportantNews.index')->with(compact('addedPosts', $addedPosts));
    }


    public function AudioNews()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $addedPosts = DB::table('posts')
            ->leftJoin('post_translations', 'posts.id', 'post_translations.post_id')
            ->where('post_translations.language_id', $defaultLanguageId)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->select('posts.*', 'category_translations.category_name', 'post_translations.title')
            ->orderBy('id', 'desc')->where('is_active', 1)
            ->where('is_audio_content', 1)->get();
        return view('backend.section.audioNews.index')->with(compact('addedPosts', $addedPosts));
    }
    public function AudioNewsSave(Request  $request)
    {

        $data=[];
        if (isset($request->is_audio_important)) {
            foreach ($request->is_audio_important as $key => $item) {
                $data[] = $key;

            }
            DB::table('posts')->wherenotIn('id', $data)->update(['is_audio_important' => 0]);
        }
        return redirect()->route('section.audioNews');
    }

    public function MostImportantNewsCreate()
    {
        $defaultLanguage = Language::getDefaultLanguage();
        $defaultLanguageId = $defaultLanguage->id;
        $addedPosts = DB::table('posts')
            ->leftJoin('post_translations', 'posts.id', 'post_translations.post_id')
            ->where('post_translations.language_id', $defaultLanguageId)
            ->join('categories', 'posts.category_id', 'categories.id')
            ->join('category_translations', 'category_translations.category_id', 'categories.id')
            ->where('category_translations.language_id', $defaultLanguageId)
            ->select('posts.*', 'category_translations.category_name', 'post_translations.title')
            ->orderBy('id', 'desc')->where('is_active', 1)
            ->where('is_most_important', 0)->get();
        return view('section.mostImportantNews.create')->with(compact('addedPosts', $addedPosts));
    }

    public function MostImportantNewsSave(Request  $request)
    {

        $data=[];
        if (isset($request->is_most_important)) {
            foreach ($request->is_most_important as $key => $item) {
                $data[] = $key;

            }
            DB::table('posts')->wherenotIn('id', $data)->update(['is_most_important' => 0]);
        }
        if (isset($request->is_most_important_create)) {
            foreach ($request->is_most_important_create as $key => $item) {
                $data[] = $key;
            }
            DB::table('posts')->whereIn('id', $data)->update(['is_most_important' => 1]);

        }
        return redirect()->route('section.mostImportantNews');
    }


}

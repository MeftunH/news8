<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }


    public function index(Request $request)
    {
        $roles = Role::orderBy('id','DESC')->paginate(5);
        return view('backend.roles.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    public function create()
    {
        $permission = Permission::get();
        return view('backend.roles.create',compact('permission'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));

        return redirect()->route('roles.index')
            ->with('success','Role created successfully');
    }
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();

        return view('backend.roles.show',compact('role','rolePermissions'));
    }
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();

        return view('backend.roles.edit',compact('role','permission','rolePermissions'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();

        $role->syncPermissions($request->input('permission'));

        return redirect()->route('roles.index')
            ->with('success','Role updated successfully');
    }
    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('roles.index')
            ->with('success','Role deleted successfully');
    }
    public function InsertWriter()
    {
        return view('backend.users.add');
   }

    public function StoreWriter(Request $request){

        $request->validate([
            'name' => 'required|unique:users|min:2|max:255',
            'email' => 'required|unique:users|email',
            'password' => 'required|password',
        ]);
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);
        $data['user_actions'] = $request->user_actions;
        $data['categories'] = $request->categories;
        $data['areas'] = $request->areas;
        $data['post'] = $request->post;
        $data['settings'] = $request->settings;
        $data['website'] = $request->website;
        $data['sections'] = $request->sections;
        $data['multimedia'] = $request->multimedia;
        $data['adverisement'] = $request->adverisement;
        $data['language'] = $request->language;
        $data['subscribers'] = $request->subscribers;
        $data['dashboard'] = $request->dashboard;
        $data['type'] = 0;
//        if (Hash::check('Meftun2001', $data['password']))
//        {
//            dd('geldi');
//        }
//        dd('gelmedi');
        DB::table('users')->insert($data);

        $notification = array(
            'message' => 'Writer Inserted Successfully',
            'alert-type' => 'success'
        );

        return view('backend.users.add')->with($notification);

    }
    public function AllWriter(){
        $writer = DB::table('users')->where('type',0)->get();
        return view('backend.users.index',compact('writer'));

    }
    public function EditWriter($id){

        $writer = DB::table('users')->where('id',$id)->first();
        return view('backend.users.edit',compact('writer'));

    }

    public function UpdateWriter(Request $request,$id)
    {
        $request->validate([
            'name' => 'required|min:2|max:255',
            'email' => 'required|email',
        ]);
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['user_actions'] = $request->user_actions;
        $data['categories'] = $request->categories;
        $data['areas'] = $request->areas;
        $data['post'] = $request->post;
        $data['settings'] = $request->settings;
        $data['website'] = $request->website;
        $data['sections'] = $request->sections;
        $data['multimedia'] = $request->multimedia;
        $data['adverisement'] = $request->adverisement;
        $data['language'] = $request->language;
        $data['subscribers'] = $request->subscribers;
        $data['dashboard'] = $request->dashboard;
        $data['type'] = 0;
        DB::table('users')->where('id',$id)->update($data);

        $notification = array(
            'message' => 'Writer Updated Successfully',
            'alert-type' => 'info'
        );

        return Redirect()->route('all.writer')->with($notification);

    }

}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{
   public function SocialSetting()
   {
       $social = DB::table('socials')->first();
       return view('backend.setting.social',compact('social'));
   }

    public function Logo()
    {
       $settings= DB::table('settings')->first();
        return view('backend.website.logo_and_icon',compact('settings'));
   }

    public function LogoEdit($id)
    {
        $settings= DB::table('settings')->where('id', $id)->first();

        return view('backend.website.logo_and_icon_edit', compact('settings'));
   }

    public function LogoUpdate(Request $request,$id)
    {
        $data = array();
        $validator =  $request->validate([
            "logo"    => "mimes:jpeg,jpg,svg,png,gif,csv|max:2048",
            "icon"  => "mimes:jpeg,jpg,png,gif,svg,csv|max:2048",

        ]);
        $old_logo = $request->oldlogo;
        $old_icon = $request->oldicon;
        $data['logo'] = $request->logo;
        $data['icon'] = $request->icon;
        $icon = $request->icon;
        $logo = $request->logo;
          if ($icon || $logo) {
              if($icon) {
                  $icon_uni = uniqid() . '.' . $icon->getClientOriginalExtension();
                  Image::make($icon)->resize(500, 300)->save('storage/website_img/' . $icon_uni);
                  $data['icon'] = 'storage/website_img/' . $icon_uni;

              }

              if ($logo) {
                  $logo_uni = uniqid() . '.' . $logo->getClientOriginalExtension();
                  Image::make($logo)->resize(500, 300)->save('storage/website_img/' . $logo_uni);
                  $data['logo'] = 'storage/website_img/' . $logo_uni;

              }
          }
          else
          {
              $data['icon'] = $old_icon;
              $data['logo'] = $old_logo;

          }
//          dd($data['logo']);
        DB::table('settings')->where('id', $id)->update($data);
        return Redirect()->route('settings');

    }
    public function SocialUpdate(Request $request, $id){

        $data = array();
        $data['facebook'] = $request->facebook;
        $data['twitter'] = $request->twitter;
        $data['pinterest'] = $request->pinterest;
        $data['youtube'] = $request->youtube;
        $data['linkedin'] = $request->linkedin;
        $data['instagram'] = $request->instagram;
        DB::table('socials')->where('id',$id)->update($data);

        $notification = array(
            'message' => 'Social Setting Updated Successfully',
            'alert-type' => 'success'
        );

        return Redirect()->route('social.setting')->with($notification);
    }
    public function SeoSetting()
    {
        $seo = DB::table('seo')->first();
        return view('backend.setting.seo',compact('seo'));
    }

    public function SeoUpdate(Request $request, $id){

        $data = array();
        $data['meta_author'] = $request->meta_author;
        $data['meta_title'] = $request->meta_title;
        $data['meta_keyword'] = $request->meta_keyword;
        $data['meta_description'] = $request->meta_description;
        $data['google_analytics'] = $request->google_analytics;
        $data['google_verification'] = $request->google_verification;
        $data['alexa_analytics'] = $request->alexa_analytics;
        DB::table('seo')->where('id',$id)->update($data);

        $notification = array(
            'message' => 'Seo Setting Updated Successfully',
            'alert-type' => 'success'
        );

        return Redirect()->route('seo.setting')->with($notification);
    }

    public function LiveTvSetting()
    {
        $live_tv = DB::table('livetv')->first();
        return view('backend.setting.live_tv',compact('live_tv'));
    }
    public function LiveTvUpdate(Request $request, $id)
    {
        $data = array();
        $data['embed_code'] = $request->embed_code;
        $data['status'] = $request->status;
        DB::table('livetv')->where('id',$id)->update($data);

        $notification = array(
            'message' => 'Live_TV Setting Updated Successfully',
            'alert-type' => 'success'
        );

        return Redirect()->route('live_tv.setting')->with($notification);
    }
    public function Active_LiveTV(Request $request, $id)
    {
        DB::table('livetv')->where('id',$id)->update(['status'=>1]);
        $notification = array(
            'message' => 'Live_TV Active Successfully',
            'alert-type' => 'success'
        );

        return Redirect()->back()->with($notification);
    }
    public function Inactive_LiveTV(Request $request, $id)
    {
        DB::table('livetv')->where('id',$id)->update(['status'=>0]);
        $notification = array(
            'message' => 'Live_TV Inactive Successfully',
            'alert-type' => 'success'
        );

        return Redirect()->back()->with($notification);
    }

    public function NoticeSetting(){
        $notice = DB::table('notices')->first();
        return view('backend.setting.notice',compact('notice'));
    }


    public function NoticeUpdate(Request $request, $id){

        $data = array();
        $data['notice'] = $request->notice;

        DB::table('notices')->where('id',$id)->update($data);

        $notification = array(
            'message' => 'Notice Setting Updated Successfully',
            'alert-type' => 'success'
        );

        return Redirect()->route('notice.setting')->with($notification);
    }



    public function ActiveNoticeSetting(Request $request, $id){

        DB::table('notices')->where('id',$id)->update(['status'=>1]);
        $notification = array(
            'message' => 'Notice Active Successfully',
            'alert-type' => 'success'
        );

        return Redirect()->back()->with($notification);
    }


    public function InActiveNoticeSetting(Request $request, $id){

        DB::table('notices')->where('id',$id)->update(['status'=>0]);
        $notification = array(
            'message' => 'Notice InActive Successfully',
            'alert-type' => 'success'
        );

        return Redirect()->back()->with($notification);
    }
    public function WebsiteSetting(){
        $website =  DB::table('websites')->orderBy('id','desc')->paginate(5);
        return view('backend.website.index',compact('website'));
    }


    public function AddWebsiteSetting(){
        return view('backend.website.create');
    }



    public function StoreWebsite(Request $request){

        $data = array();
        $data['website_name'] = $request->website_name;
        $data['website_link'] = $request->website_link;

        DB::table('websites')->insert($data);

        $notification = array(
            'message' => 'Website Link Updated Successfully',
            'alert-type' => 'success'
        );

        return Redirect()->route('all.website')->with($notification);
    }
    public function EditWebsite($id)
    {
        $website=DB::table('websites')->where('id',$id)->first();
        return view('backend.website.edit',compact('website'));
    }
    public function UpdateWebsite(Request $request,$id)
    {
        $data = array();
        $data['website_name'] = $request->website_name;
        $data['website_link'] = $request->website_link;
        DB::table('websites')->where('id',$id)->update($data);
        $notification = array(
            'message' => 'Website Updated successfully',
            'alert-type' => 'warning'
        );
        return Redirect()->route('all.website')->with($notification);
    }
    public function DeleteWebsite($id)
    {
        $website=DB::table('websites')->where('id',$id)->delete();
        $notification = array(
            'message' => 'Website Deleted successfully',
            'alert-type' => 'danger'
        );

        return Redirect()->route('all.website')->with($notification);
    }




}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LocalizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lan_id=\Illuminate\Support\Facades\DB::table('language')->where('local', \Illuminate\Support\Facades\App::getLocale())->first();
        return $next($request);
    }
}
